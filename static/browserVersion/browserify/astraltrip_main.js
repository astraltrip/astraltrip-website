(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["Meyda"] = factory();
	else
		root["Meyda"] = factory();
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/assert/assert.js":
/*!***************************************!*\
  !*** ./node_modules/assert/assert.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

var objectAssign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

// compare and isBuffer taken from https://github.com/feross/buffer/blob/680e9e5e488f22aac27599a57dc844a6315928dd/index.js
// original notice:

/*!
 * The buffer module from node.js, for the browser.
 *
 * @author   Feross Aboukhadijeh <feross@feross.org> <http://feross.org>
 * @license  MIT
 */
function compare(a, b) {
  if (a === b) {
    return 0;
  }

  var x = a.length;
  var y = b.length;

  for (var i = 0, len = Math.min(x, y); i < len; ++i) {
    if (a[i] !== b[i]) {
      x = a[i];
      y = b[i];
      break;
    }
  }

  if (x < y) {
    return -1;
  }
  if (y < x) {
    return 1;
  }
  return 0;
}
function isBuffer(b) {
  if (global.Buffer && typeof global.Buffer.isBuffer === 'function') {
    return global.Buffer.isBuffer(b);
  }
  return !!(b != null && b._isBuffer);
}

// based on node assert, original notice:
// NB: The URL to the CommonJS spec is kept just for tradition.
//     node-assert has evolved a lot since then, both in API and behavior.

// http://wiki.commonjs.org/wiki/Unit_Testing/1.0
//
// THIS IS NOT TESTED NOR LIKELY TO WORK OUTSIDE V8!
//
// Originally from narwhal.js (http://narwhaljs.org)
// Copyright (c) 2009 Thomas Robinson <280north.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the 'Software'), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

var util = __webpack_require__(/*! util/ */ "./node_modules/node-libs-browser/node_modules/util/util.js");
var hasOwn = Object.prototype.hasOwnProperty;
var pSlice = Array.prototype.slice;
var functionsHaveNames = (function () {
  return function foo() {}.name === 'foo';
}());
function pToString (obj) {
  return Object.prototype.toString.call(obj);
}
function isView(arrbuf) {
  if (isBuffer(arrbuf)) {
    return false;
  }
  if (typeof global.ArrayBuffer !== 'function') {
    return false;
  }
  if (typeof ArrayBuffer.isView === 'function') {
    return ArrayBuffer.isView(arrbuf);
  }
  if (!arrbuf) {
    return false;
  }
  if (arrbuf instanceof DataView) {
    return true;
  }
  if (arrbuf.buffer && arrbuf.buffer instanceof ArrayBuffer) {
    return true;
  }
  return false;
}
// 1. The assert module provides functions that throw
// AssertionError's when particular conditions are not met. The
// assert module must conform to the following interface.

var assert = module.exports = ok;

// 2. The AssertionError is defined in assert.
// new assert.AssertionError({ message: message,
//                             actual: actual,
//                             expected: expected })

var regex = /\s*function\s+([^\(\s]*)\s*/;
// based on https://github.com/ljharb/function.prototype.name/blob/adeeeec8bfcc6068b187d7d9fb3d5bb1d3a30899/implementation.js
function getName(func) {
  if (!util.isFunction(func)) {
    return;
  }
  if (functionsHaveNames) {
    return func.name;
  }
  var str = func.toString();
  var match = str.match(regex);
  return match && match[1];
}
assert.AssertionError = function AssertionError(options) {
  this.name = 'AssertionError';
  this.actual = options.actual;
  this.expected = options.expected;
  this.operator = options.operator;
  if (options.message) {
    this.message = options.message;
    this.generatedMessage = false;
  } else {
    this.message = getMessage(this);
    this.generatedMessage = true;
  }
  var stackStartFunction = options.stackStartFunction || fail;
  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, stackStartFunction);
  } else {
    // non v8 browsers so we can have a stacktrace
    var err = new Error();
    if (err.stack) {
      var out = err.stack;

      // try to strip useless frames
      var fn_name = getName(stackStartFunction);
      var idx = out.indexOf('\n' + fn_name);
      if (idx >= 0) {
        // once we have located the function frame
        // we need to strip out everything before it (and its line)
        var next_line = out.indexOf('\n', idx + 1);
        out = out.substring(next_line + 1);
      }

      this.stack = out;
    }
  }
};

// assert.AssertionError instanceof Error
util.inherits(assert.AssertionError, Error);

function truncate(s, n) {
  if (typeof s === 'string') {
    return s.length < n ? s : s.slice(0, n);
  } else {
    return s;
  }
}
function inspect(something) {
  if (functionsHaveNames || !util.isFunction(something)) {
    return util.inspect(something);
  }
  var rawname = getName(something);
  var name = rawname ? ': ' + rawname : '';
  return '[Function' +  name + ']';
}
function getMessage(self) {
  return truncate(inspect(self.actual), 128) + ' ' +
         self.operator + ' ' +
         truncate(inspect(self.expected), 128);
}

// At present only the three keys mentioned above are used and
// understood by the spec. Implementations or sub modules can pass
// other keys to the AssertionError's constructor - they will be
// ignored.

// 3. All of the following functions must throw an AssertionError
// when a corresponding condition is not met, with a message that
// may be undefined if not provided.  All assertion methods provide
// both the actual and expected values to the assertion error for
// display purposes.

function fail(actual, expected, message, operator, stackStartFunction) {
  throw new assert.AssertionError({
    message: message,
    actual: actual,
    expected: expected,
    operator: operator,
    stackStartFunction: stackStartFunction
  });
}

// EXTENSION! allows for well behaved errors defined elsewhere.
assert.fail = fail;

// 4. Pure assertion tests whether a value is truthy, as determined
// by !!guard.
// assert.ok(guard, message_opt);
// This statement is equivalent to assert.equal(true, !!guard,
// message_opt);. To test strictly for the value true, use
// assert.strictEqual(true, guard, message_opt);.

function ok(value, message) {
  if (!value) fail(value, true, message, '==', assert.ok);
}
assert.ok = ok;

// 5. The equality assertion tests shallow, coercive equality with
// ==.
// assert.equal(actual, expected, message_opt);

assert.equal = function equal(actual, expected, message) {
  if (actual != expected) fail(actual, expected, message, '==', assert.equal);
};

// 6. The non-equality assertion tests for whether two objects are not equal
// with != assert.notEqual(actual, expected, message_opt);

assert.notEqual = function notEqual(actual, expected, message) {
  if (actual == expected) {
    fail(actual, expected, message, '!=', assert.notEqual);
  }
};

// 7. The equivalence assertion tests a deep equality relation.
// assert.deepEqual(actual, expected, message_opt);

assert.deepEqual = function deepEqual(actual, expected, message) {
  if (!_deepEqual(actual, expected, false)) {
    fail(actual, expected, message, 'deepEqual', assert.deepEqual);
  }
};

assert.deepStrictEqual = function deepStrictEqual(actual, expected, message) {
  if (!_deepEqual(actual, expected, true)) {
    fail(actual, expected, message, 'deepStrictEqual', assert.deepStrictEqual);
  }
};

function _deepEqual(actual, expected, strict, memos) {
  // 7.1. All identical values are equivalent, as determined by ===.
  if (actual === expected) {
    return true;
  } else if (isBuffer(actual) && isBuffer(expected)) {
    return compare(actual, expected) === 0;

  // 7.2. If the expected value is a Date object, the actual value is
  // equivalent if it is also a Date object that refers to the same time.
  } else if (util.isDate(actual) && util.isDate(expected)) {
    return actual.getTime() === expected.getTime();

  // 7.3 If the expected value is a RegExp object, the actual value is
  // equivalent if it is also a RegExp object with the same source and
  // properties (`global`, `multiline`, `lastIndex`, `ignoreCase`).
  } else if (util.isRegExp(actual) && util.isRegExp(expected)) {
    return actual.source === expected.source &&
           actual.global === expected.global &&
           actual.multiline === expected.multiline &&
           actual.lastIndex === expected.lastIndex &&
           actual.ignoreCase === expected.ignoreCase;

  // 7.4. Other pairs that do not both pass typeof value == 'object',
  // equivalence is determined by ==.
  } else if ((actual === null || typeof actual !== 'object') &&
             (expected === null || typeof expected !== 'object')) {
    return strict ? actual === expected : actual == expected;

  // If both values are instances of typed arrays, wrap their underlying
  // ArrayBuffers in a Buffer each to increase performance
  // This optimization requires the arrays to have the same type as checked by
  // Object.prototype.toString (aka pToString). Never perform binary
  // comparisons for Float*Arrays, though, since e.g. +0 === -0 but their
  // bit patterns are not identical.
  } else if (isView(actual) && isView(expected) &&
             pToString(actual) === pToString(expected) &&
             !(actual instanceof Float32Array ||
               actual instanceof Float64Array)) {
    return compare(new Uint8Array(actual.buffer),
                   new Uint8Array(expected.buffer)) === 0;

  // 7.5 For all other Object pairs, including Array objects, equivalence is
  // determined by having the same number of owned properties (as verified
  // with Object.prototype.hasOwnProperty.call), the same set of keys
  // (although not necessarily the same order), equivalent values for every
  // corresponding key, and an identical 'prototype' property. Note: this
  // accounts for both named and indexed properties on Arrays.
  } else if (isBuffer(actual) !== isBuffer(expected)) {
    return false;
  } else {
    memos = memos || {actual: [], expected: []};

    var actualIndex = memos.actual.indexOf(actual);
    if (actualIndex !== -1) {
      if (actualIndex === memos.expected.indexOf(expected)) {
        return true;
      }
    }

    memos.actual.push(actual);
    memos.expected.push(expected);

    return objEquiv(actual, expected, strict, memos);
  }
}

function isArguments(object) {
  return Object.prototype.toString.call(object) == '[object Arguments]';
}

function objEquiv(a, b, strict, actualVisitedObjects) {
  if (a === null || a === undefined || b === null || b === undefined)
    return false;
  // if one is a primitive, the other must be same
  if (util.isPrimitive(a) || util.isPrimitive(b))
    return a === b;
  if (strict && Object.getPrototypeOf(a) !== Object.getPrototypeOf(b))
    return false;
  var aIsArgs = isArguments(a);
  var bIsArgs = isArguments(b);
  if ((aIsArgs && !bIsArgs) || (!aIsArgs && bIsArgs))
    return false;
  if (aIsArgs) {
    a = pSlice.call(a);
    b = pSlice.call(b);
    return _deepEqual(a, b, strict);
  }
  var ka = objectKeys(a);
  var kb = objectKeys(b);
  var key, i;
  // having the same number of owned properties (keys incorporates
  // hasOwnProperty)
  if (ka.length !== kb.length)
    return false;
  //the same set of keys (although not necessarily the same order),
  ka.sort();
  kb.sort();
  //~~~cheap key test
  for (i = ka.length - 1; i >= 0; i--) {
    if (ka[i] !== kb[i])
      return false;
  }
  //equivalent values for every corresponding key, and
  //~~~possibly expensive deep test
  for (i = ka.length - 1; i >= 0; i--) {
    key = ka[i];
    if (!_deepEqual(a[key], b[key], strict, actualVisitedObjects))
      return false;
  }
  return true;
}

// 8. The non-equivalence assertion tests for any deep inequality.
// assert.notDeepEqual(actual, expected, message_opt);

assert.notDeepEqual = function notDeepEqual(actual, expected, message) {
  if (_deepEqual(actual, expected, false)) {
    fail(actual, expected, message, 'notDeepEqual', assert.notDeepEqual);
  }
};

assert.notDeepStrictEqual = notDeepStrictEqual;
function notDeepStrictEqual(actual, expected, message) {
  if (_deepEqual(actual, expected, true)) {
    fail(actual, expected, message, 'notDeepStrictEqual', notDeepStrictEqual);
  }
}


// 9. The strict equality assertion tests strict equality, as determined by ===.
// assert.strictEqual(actual, expected, message_opt);

assert.strictEqual = function strictEqual(actual, expected, message) {
  if (actual !== expected) {
    fail(actual, expected, message, '===', assert.strictEqual);
  }
};

// 10. The strict non-equality assertion tests for strict inequality, as
// determined by !==.  assert.notStrictEqual(actual, expected, message_opt);

assert.notStrictEqual = function notStrictEqual(actual, expected, message) {
  if (actual === expected) {
    fail(actual, expected, message, '!==', assert.notStrictEqual);
  }
};

function expectedException(actual, expected) {
  if (!actual || !expected) {
    return false;
  }

  if (Object.prototype.toString.call(expected) == '[object RegExp]') {
    return expected.test(actual);
  }

  try {
    if (actual instanceof expected) {
      return true;
    }
  } catch (e) {
    // Ignore.  The instanceof check doesn't work for arrow functions.
  }

  if (Error.isPrototypeOf(expected)) {
    return false;
  }

  return expected.call({}, actual) === true;
}

function _tryBlock(block) {
  var error;
  try {
    block();
  } catch (e) {
    error = e;
  }
  return error;
}

function _throws(shouldThrow, block, expected, message) {
  var actual;

  if (typeof block !== 'function') {
    throw new TypeError('"block" argument must be a function');
  }

  if (typeof expected === 'string') {
    message = expected;
    expected = null;
  }

  actual = _tryBlock(block);

  message = (expected && expected.name ? ' (' + expected.name + ').' : '.') +
            (message ? ' ' + message : '.');

  if (shouldThrow && !actual) {
    fail(actual, expected, 'Missing expected exception' + message);
  }

  var userProvidedMessage = typeof message === 'string';
  var isUnwantedException = !shouldThrow && util.isError(actual);
  var isUnexpectedException = !shouldThrow && actual && !expected;

  if ((isUnwantedException &&
      userProvidedMessage &&
      expectedException(actual, expected)) ||
      isUnexpectedException) {
    fail(actual, expected, 'Got unwanted exception' + message);
  }

  if ((shouldThrow && actual && expected &&
      !expectedException(actual, expected)) || (!shouldThrow && actual)) {
    throw actual;
  }
}

// 11. Expected to throw an error:
// assert.throws(block, Error_opt, message_opt);

assert.throws = function(block, /*optional*/error, /*optional*/message) {
  _throws(true, block, error, message);
};

// EXTENSION! This is annoying to write outside this module.
assert.doesNotThrow = function(block, /*optional*/error, /*optional*/message) {
  _throws(false, block, error, message);
};

assert.ifError = function(err) { if (err) throw err; };

// Expose a strict only variant of assert
function strict(value, message) {
  if (!value) fail(value, true, message, '==', strict);
}
assert.strict = objectAssign(strict, assert, {
  equal: assert.strictEqual,
  deepEqual: assert.deepStrictEqual,
  notEqual: assert.notStrictEqual,
  notDeepEqual: assert.notDeepStrictEqual
});
assert.strict.strict = assert.strict;

var objectKeys = Object.keys || function (obj) {
  var keys = [];
  for (var key in obj) {
    if (hasOwn.call(obj, key)) keys.push(key);
  }
  return keys;
};

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/dct/index.js":
/*!***********************************!*\
  !*** ./node_modules/dct/index.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./src/dct.js */ "./node_modules/dct/src/dct.js");


/***/ }),

/***/ "./node_modules/dct/src/dct.js":
/*!*************************************!*\
  !*** ./node_modules/dct/src/dct.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*===========================================================================*\
 * Discrete Cosine Transform
 *
 * (c) Vail Systems. Joshua Jung and Ben Bryan. 2015
 *
 * This code is not designed to be highly optimized but as an educational
 * tool to understand the Mel-scale and its related coefficients used in
 * human speech analysis.
\*===========================================================================*/
var cosMap = null;

// Builds a cosine map for the given input size. This allows multiple input sizes to be memoized automagically
// if you want to run the DCT over and over.
var memoizeCosines = function(N) {
  cosMap = cosMap || {};
  cosMap[N] = new Array(N*N);

  var PI_N = Math.PI / N;

  for (var k = 0; k < N; k++) {
    for (var n = 0; n < N; n++) {
      cosMap[N][n + (k * N)] = Math.cos(PI_N * (n + 0.5) * k);
    }
  }
};

function dct(signal, scale) {
  var L = signal.length;
  scale = scale || 2;

  if (!cosMap || !cosMap[L]) memoizeCosines(L);

  var coefficients = signal.map(function () {return 0;});

  return coefficients.map(function (__, ix) {
    return scale * signal.reduce(function (prev, cur, ix_, arr) {
      return prev + (cur * cosMap[L][ix_ + (ix * L)]);
    }, 0);
  });
};

module.exports = dct;


/***/ }),

/***/ "./node_modules/fftjs/dist/fft.js":
/*!****************************************!*\
  !*** ./node_modules/fftjs/dist/fft.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./utils */ "./node_modules/fftjs/dist/utils.js");

// real to complex fft
var fft = function fft(signal) {

  var complexSignal = {};

  if (signal.real === undefined || signal.imag === undefined) {
    complexSignal = utils.constructComplexArray(signal);
  } else {
    complexSignal.real = signal.real.slice();
    complexSignal.imag = signal.imag.slice();
  }

  var N = complexSignal.real.length;
  var logN = Math.log2(N);

  if (Math.round(logN) != logN) throw new Error('Input size must be a power of 2.');

  if (complexSignal.real.length != complexSignal.imag.length) {
    throw new Error('Real and imaginary components must have the same length.');
  }

  var bitReversedIndices = utils.bitReverseArray(N);

  // sort array
  var ordered = {
    'real': [],
    'imag': []
  };

  for (var i = 0; i < N; i++) {
    ordered.real[bitReversedIndices[i]] = complexSignal.real[i];
    ordered.imag[bitReversedIndices[i]] = complexSignal.imag[i];
  }

  for (var _i = 0; _i < N; _i++) {
    complexSignal.real[_i] = ordered.real[_i];
    complexSignal.imag[_i] = ordered.imag[_i];
  }
  // iterate over the number of stages
  for (var n = 1; n <= logN; n++) {
    var currN = Math.pow(2, n);

    // find twiddle factors
    for (var k = 0; k < currN / 2; k++) {
      var twiddle = utils.euler(k, currN);

      // on each block of FT, implement the butterfly diagram
      for (var m = 0; m < N / currN; m++) {
        var currEvenIndex = currN * m + k;
        var currOddIndex = currN * m + k + currN / 2;

        var currEvenIndexSample = {
          'real': complexSignal.real[currEvenIndex],
          'imag': complexSignal.imag[currEvenIndex]
        };
        var currOddIndexSample = {
          'real': complexSignal.real[currOddIndex],
          'imag': complexSignal.imag[currOddIndex]
        };

        var odd = utils.multiply(twiddle, currOddIndexSample);

        var subtractionResult = utils.subtract(currEvenIndexSample, odd);
        complexSignal.real[currOddIndex] = subtractionResult.real;
        complexSignal.imag[currOddIndex] = subtractionResult.imag;

        var additionResult = utils.add(odd, currEvenIndexSample);
        complexSignal.real[currEvenIndex] = additionResult.real;
        complexSignal.imag[currEvenIndex] = additionResult.imag;
      }
    }
  }

  return complexSignal;
};

// complex to real ifft
var ifft = function ifft(signal) {

  if (signal.real === undefined || signal.imag === undefined) {
    throw new Error("IFFT only accepts a complex input.");
  }

  var N = signal.real.length;

  var complexSignal = {
    'real': [],
    'imag': []
  };

  //take complex conjugate in order to be able to use the regular FFT for IFFT
  for (var i = 0; i < N; i++) {
    var currentSample = {
      'real': signal.real[i],
      'imag': signal.imag[i]
    };

    var conjugateSample = utils.conj(currentSample);
    complexSignal.real[i] = conjugateSample.real;
    complexSignal.imag[i] = conjugateSample.imag;
  }

  //compute
  var X = fft(complexSignal);

  //normalize
  complexSignal.real = X.real.map(function (val) {
    return val / N;
  });

  complexSignal.imag = X.imag.map(function (val) {
    return val / N;
  });

  return complexSignal;
};

module.exports = {
  fft: fft,
  ifft: ifft
};

/***/ }),

/***/ "./node_modules/fftjs/dist/utils.js":
/*!******************************************!*\
  !*** ./node_modules/fftjs/dist/utils.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// memoization of the reversal of different lengths.

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var memoizedReversal = {};
var memoizedZeroBuffers = {};

var constructComplexArray = function constructComplexArray(signal) {
  var complexSignal = {};

  complexSignal.real = signal.real === undefined ? signal.slice() : signal.real.slice();

  var bufferSize = complexSignal.real.length;

  if (memoizedZeroBuffers[bufferSize] === undefined) {
    memoizedZeroBuffers[bufferSize] = Array.apply(null, Array(bufferSize)).map(Number.prototype.valueOf, 0);
  }

  complexSignal.imag = memoizedZeroBuffers[bufferSize].slice();

  return complexSignal;
};

var bitReverseArray = function bitReverseArray(N) {
  if (memoizedReversal[N] === undefined) {
    var maxBinaryLength = (N - 1).toString(2).length; //get the binary length of the largest index.
    var templateBinary = '0'.repeat(maxBinaryLength); //create a template binary of that length.
    var reversed = {};
    for (var n = 0; n < N; n++) {
      var currBinary = n.toString(2); //get binary value of current index.

      //prepend zeros from template to current binary. This makes binary values of all indices have the same length.
      currBinary = templateBinary.substr(currBinary.length) + currBinary;

      currBinary = [].concat(_toConsumableArray(currBinary)).reverse().join(''); //reverse
      reversed[n] = parseInt(currBinary, 2); //convert to decimal
    }
    memoizedReversal[N] = reversed; //save
  }
  return memoizedReversal[N];
};

// complex multiplication
var multiply = function multiply(a, b) {
  return {
    'real': a.real * b.real - a.imag * b.imag,
    'imag': a.real * b.imag + a.imag * b.real
  };
};

// complex addition
var add = function add(a, b) {
  return {
    'real': a.real + b.real,
    'imag': a.imag + b.imag
  };
};

// complex subtraction
var subtract = function subtract(a, b) {
  return {
    'real': a.real - b.real,
    'imag': a.imag - b.imag
  };
};

// euler's identity e^x = cos(x) + sin(x)
var euler = function euler(kn, N) {
  var x = -2 * Math.PI * kn / N;
  return { 'real': Math.cos(x), 'imag': Math.sin(x) };
};

// complex conjugate
var conj = function conj(a) {
  a.imag *= -1;
  return a;
};

module.exports = {
  bitReverseArray: bitReverseArray,
  multiply: multiply,
  add: add,
  subtract: subtract,
  euler: euler,
  conj: conj,
  constructComplexArray: constructComplexArray
};

/***/ }),

/***/ "./node_modules/node-libs-browser/node_modules/util/node_modules/inherits/inherits_browser.js":
/*!****************************************************************************************************!*\
  !*** ./node_modules/node-libs-browser/node_modules/util/node_modules/inherits/inherits_browser.js ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

if (typeof Object.create === 'function') {
  // implementation from standard node.js 'util' module
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  };
} else {
  // old school shim for old browsers
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    var TempCtor = function () {}
    TempCtor.prototype = superCtor.prototype
    ctor.prototype = new TempCtor()
    ctor.prototype.constructor = ctor
  }
}


/***/ }),

/***/ "./node_modules/node-libs-browser/node_modules/util/support/isBufferBrowser.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/node-libs-browser/node_modules/util/support/isBufferBrowser.js ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function isBuffer(arg) {
  return arg && typeof arg === 'object'
    && typeof arg.copy === 'function'
    && typeof arg.fill === 'function'
    && typeof arg.readUInt8 === 'function';
}

/***/ }),

/***/ "./node_modules/node-libs-browser/node_modules/util/util.js":
/*!******************************************************************!*\
  !*** ./node_modules/node-libs-browser/node_modules/util/util.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process) {// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

var getOwnPropertyDescriptors = Object.getOwnPropertyDescriptors ||
  function getOwnPropertyDescriptors(obj) {
    var keys = Object.keys(obj);
    var descriptors = {};
    for (var i = 0; i < keys.length; i++) {
      descriptors[keys[i]] = Object.getOwnPropertyDescriptor(obj, keys[i]);
    }
    return descriptors;
  };

var formatRegExp = /%[sdj%]/g;
exports.format = function(f) {
  if (!isString(f)) {
    var objects = [];
    for (var i = 0; i < arguments.length; i++) {
      objects.push(inspect(arguments[i]));
    }
    return objects.join(' ');
  }

  var i = 1;
  var args = arguments;
  var len = args.length;
  var str = String(f).replace(formatRegExp, function(x) {
    if (x === '%%') return '%';
    if (i >= len) return x;
    switch (x) {
      case '%s': return String(args[i++]);
      case '%d': return Number(args[i++]);
      case '%j':
        try {
          return JSON.stringify(args[i++]);
        } catch (_) {
          return '[Circular]';
        }
      default:
        return x;
    }
  });
  for (var x = args[i]; i < len; x = args[++i]) {
    if (isNull(x) || !isObject(x)) {
      str += ' ' + x;
    } else {
      str += ' ' + inspect(x);
    }
  }
  return str;
};


// Mark that a method should not be used.
// Returns a modified function which warns once by default.
// If --no-deprecation is set, then it is a no-op.
exports.deprecate = function(fn, msg) {
  if (typeof process !== 'undefined' && process.noDeprecation === true) {
    return fn;
  }

  // Allow for deprecating things in the process of starting up.
  if (typeof process === 'undefined') {
    return function() {
      return exports.deprecate(fn, msg).apply(this, arguments);
    };
  }

  var warned = false;
  function deprecated() {
    if (!warned) {
      if (process.throwDeprecation) {
        throw new Error(msg);
      } else if (process.traceDeprecation) {
        console.trace(msg);
      } else {
        console.error(msg);
      }
      warned = true;
    }
    return fn.apply(this, arguments);
  }

  return deprecated;
};


var debugs = {};
var debugEnviron;
exports.debuglog = function(set) {
  if (isUndefined(debugEnviron))
    debugEnviron = process.env.NODE_DEBUG || '';
  set = set.toUpperCase();
  if (!debugs[set]) {
    if (new RegExp('\\b' + set + '\\b', 'i').test(debugEnviron)) {
      var pid = process.pid;
      debugs[set] = function() {
        var msg = exports.format.apply(exports, arguments);
        console.error('%s %d: %s', set, pid, msg);
      };
    } else {
      debugs[set] = function() {};
    }
  }
  return debugs[set];
};


/**
 * Echos the value of a value. Trys to print the value out
 * in the best way possible given the different types.
 *
 * @param {Object} obj The object to print out.
 * @param {Object} opts Optional options object that alters the output.
 */
/* legacy: obj, showHidden, depth, colors*/
function inspect(obj, opts) {
  // default options
  var ctx = {
    seen: [],
    stylize: stylizeNoColor
  };
  // legacy...
  if (arguments.length >= 3) ctx.depth = arguments[2];
  if (arguments.length >= 4) ctx.colors = arguments[3];
  if (isBoolean(opts)) {
    // legacy...
    ctx.showHidden = opts;
  } else if (opts) {
    // got an "options" object
    exports._extend(ctx, opts);
  }
  // set default options
  if (isUndefined(ctx.showHidden)) ctx.showHidden = false;
  if (isUndefined(ctx.depth)) ctx.depth = 2;
  if (isUndefined(ctx.colors)) ctx.colors = false;
  if (isUndefined(ctx.customInspect)) ctx.customInspect = true;
  if (ctx.colors) ctx.stylize = stylizeWithColor;
  return formatValue(ctx, obj, ctx.depth);
}
exports.inspect = inspect;


// http://en.wikipedia.org/wiki/ANSI_escape_code#graphics
inspect.colors = {
  'bold' : [1, 22],
  'italic' : [3, 23],
  'underline' : [4, 24],
  'inverse' : [7, 27],
  'white' : [37, 39],
  'grey' : [90, 39],
  'black' : [30, 39],
  'blue' : [34, 39],
  'cyan' : [36, 39],
  'green' : [32, 39],
  'magenta' : [35, 39],
  'red' : [31, 39],
  'yellow' : [33, 39]
};

// Don't use 'blue' not visible on cmd.exe
inspect.styles = {
  'special': 'cyan',
  'number': 'yellow',
  'boolean': 'yellow',
  'undefined': 'grey',
  'null': 'bold',
  'string': 'green',
  'date': 'magenta',
  // "name": intentionally not styling
  'regexp': 'red'
};


function stylizeWithColor(str, styleType) {
  var style = inspect.styles[styleType];

  if (style) {
    return '\u001b[' + inspect.colors[style][0] + 'm' + str +
           '\u001b[' + inspect.colors[style][1] + 'm';
  } else {
    return str;
  }
}


function stylizeNoColor(str, styleType) {
  return str;
}


function arrayToHash(array) {
  var hash = {};

  array.forEach(function(val, idx) {
    hash[val] = true;
  });

  return hash;
}


function formatValue(ctx, value, recurseTimes) {
  // Provide a hook for user-specified inspect functions.
  // Check that value is an object with an inspect function on it
  if (ctx.customInspect &&
      value &&
      isFunction(value.inspect) &&
      // Filter out the util module, it's inspect function is special
      value.inspect !== exports.inspect &&
      // Also filter out any prototype objects using the circular check.
      !(value.constructor && value.constructor.prototype === value)) {
    var ret = value.inspect(recurseTimes, ctx);
    if (!isString(ret)) {
      ret = formatValue(ctx, ret, recurseTimes);
    }
    return ret;
  }

  // Primitive types cannot have properties
  var primitive = formatPrimitive(ctx, value);
  if (primitive) {
    return primitive;
  }

  // Look up the keys of the object.
  var keys = Object.keys(value);
  var visibleKeys = arrayToHash(keys);

  if (ctx.showHidden) {
    keys = Object.getOwnPropertyNames(value);
  }

  // IE doesn't make error fields non-enumerable
  // http://msdn.microsoft.com/en-us/library/ie/dww52sbt(v=vs.94).aspx
  if (isError(value)
      && (keys.indexOf('message') >= 0 || keys.indexOf('description') >= 0)) {
    return formatError(value);
  }

  // Some type of object without properties can be shortcutted.
  if (keys.length === 0) {
    if (isFunction(value)) {
      var name = value.name ? ': ' + value.name : '';
      return ctx.stylize('[Function' + name + ']', 'special');
    }
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    }
    if (isDate(value)) {
      return ctx.stylize(Date.prototype.toString.call(value), 'date');
    }
    if (isError(value)) {
      return formatError(value);
    }
  }

  var base = '', array = false, braces = ['{', '}'];

  // Make Array say that they are Array
  if (isArray(value)) {
    array = true;
    braces = ['[', ']'];
  }

  // Make functions say that they are functions
  if (isFunction(value)) {
    var n = value.name ? ': ' + value.name : '';
    base = ' [Function' + n + ']';
  }

  // Make RegExps say that they are RegExps
  if (isRegExp(value)) {
    base = ' ' + RegExp.prototype.toString.call(value);
  }

  // Make dates with properties first say the date
  if (isDate(value)) {
    base = ' ' + Date.prototype.toUTCString.call(value);
  }

  // Make error with message first say the error
  if (isError(value)) {
    base = ' ' + formatError(value);
  }

  if (keys.length === 0 && (!array || value.length == 0)) {
    return braces[0] + base + braces[1];
  }

  if (recurseTimes < 0) {
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    } else {
      return ctx.stylize('[Object]', 'special');
    }
  }

  ctx.seen.push(value);

  var output;
  if (array) {
    output = formatArray(ctx, value, recurseTimes, visibleKeys, keys);
  } else {
    output = keys.map(function(key) {
      return formatProperty(ctx, value, recurseTimes, visibleKeys, key, array);
    });
  }

  ctx.seen.pop();

  return reduceToSingleString(output, base, braces);
}


function formatPrimitive(ctx, value) {
  if (isUndefined(value))
    return ctx.stylize('undefined', 'undefined');
  if (isString(value)) {
    var simple = '\'' + JSON.stringify(value).replace(/^"|"$/g, '')
                                             .replace(/'/g, "\\'")
                                             .replace(/\\"/g, '"') + '\'';
    return ctx.stylize(simple, 'string');
  }
  if (isNumber(value))
    return ctx.stylize('' + value, 'number');
  if (isBoolean(value))
    return ctx.stylize('' + value, 'boolean');
  // For some reason typeof null is "object", so special case here.
  if (isNull(value))
    return ctx.stylize('null', 'null');
}


function formatError(value) {
  return '[' + Error.prototype.toString.call(value) + ']';
}


function formatArray(ctx, value, recurseTimes, visibleKeys, keys) {
  var output = [];
  for (var i = 0, l = value.length; i < l; ++i) {
    if (hasOwnProperty(value, String(i))) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          String(i), true));
    } else {
      output.push('');
    }
  }
  keys.forEach(function(key) {
    if (!key.match(/^\d+$/)) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          key, true));
    }
  });
  return output;
}


function formatProperty(ctx, value, recurseTimes, visibleKeys, key, array) {
  var name, str, desc;
  desc = Object.getOwnPropertyDescriptor(value, key) || { value: value[key] };
  if (desc.get) {
    if (desc.set) {
      str = ctx.stylize('[Getter/Setter]', 'special');
    } else {
      str = ctx.stylize('[Getter]', 'special');
    }
  } else {
    if (desc.set) {
      str = ctx.stylize('[Setter]', 'special');
    }
  }
  if (!hasOwnProperty(visibleKeys, key)) {
    name = '[' + key + ']';
  }
  if (!str) {
    if (ctx.seen.indexOf(desc.value) < 0) {
      if (isNull(recurseTimes)) {
        str = formatValue(ctx, desc.value, null);
      } else {
        str = formatValue(ctx, desc.value, recurseTimes - 1);
      }
      if (str.indexOf('\n') > -1) {
        if (array) {
          str = str.split('\n').map(function(line) {
            return '  ' + line;
          }).join('\n').substr(2);
        } else {
          str = '\n' + str.split('\n').map(function(line) {
            return '   ' + line;
          }).join('\n');
        }
      }
    } else {
      str = ctx.stylize('[Circular]', 'special');
    }
  }
  if (isUndefined(name)) {
    if (array && key.match(/^\d+$/)) {
      return str;
    }
    name = JSON.stringify('' + key);
    if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
      name = name.substr(1, name.length - 2);
      name = ctx.stylize(name, 'name');
    } else {
      name = name.replace(/'/g, "\\'")
                 .replace(/\\"/g, '"')
                 .replace(/(^"|"$)/g, "'");
      name = ctx.stylize(name, 'string');
    }
  }

  return name + ': ' + str;
}


function reduceToSingleString(output, base, braces) {
  var numLinesEst = 0;
  var length = output.reduce(function(prev, cur) {
    numLinesEst++;
    if (cur.indexOf('\n') >= 0) numLinesEst++;
    return prev + cur.replace(/\u001b\[\d\d?m/g, '').length + 1;
  }, 0);

  if (length > 60) {
    return braces[0] +
           (base === '' ? '' : base + '\n ') +
           ' ' +
           output.join(',\n  ') +
           ' ' +
           braces[1];
  }

  return braces[0] + base + ' ' + output.join(', ') + ' ' + braces[1];
}


// NOTE: These type checking functions intentionally don't use `instanceof`
// because it is fragile and can be easily faked with `Object.create()`.
function isArray(ar) {
  return Array.isArray(ar);
}
exports.isArray = isArray;

function isBoolean(arg) {
  return typeof arg === 'boolean';
}
exports.isBoolean = isBoolean;

function isNull(arg) {
  return arg === null;
}
exports.isNull = isNull;

function isNullOrUndefined(arg) {
  return arg == null;
}
exports.isNullOrUndefined = isNullOrUndefined;

function isNumber(arg) {
  return typeof arg === 'number';
}
exports.isNumber = isNumber;

function isString(arg) {
  return typeof arg === 'string';
}
exports.isString = isString;

function isSymbol(arg) {
  return typeof arg === 'symbol';
}
exports.isSymbol = isSymbol;

function isUndefined(arg) {
  return arg === void 0;
}
exports.isUndefined = isUndefined;

function isRegExp(re) {
  return isObject(re) && objectToString(re) === '[object RegExp]';
}
exports.isRegExp = isRegExp;

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}
exports.isObject = isObject;

function isDate(d) {
  return isObject(d) && objectToString(d) === '[object Date]';
}
exports.isDate = isDate;

function isError(e) {
  return isObject(e) &&
      (objectToString(e) === '[object Error]' || e instanceof Error);
}
exports.isError = isError;

function isFunction(arg) {
  return typeof arg === 'function';
}
exports.isFunction = isFunction;

function isPrimitive(arg) {
  return arg === null ||
         typeof arg === 'boolean' ||
         typeof arg === 'number' ||
         typeof arg === 'string' ||
         typeof arg === 'symbol' ||  // ES6 symbol
         typeof arg === 'undefined';
}
exports.isPrimitive = isPrimitive;

exports.isBuffer = __webpack_require__(/*! ./support/isBuffer */ "./node_modules/node-libs-browser/node_modules/util/support/isBufferBrowser.js");

function objectToString(o) {
  return Object.prototype.toString.call(o);
}


function pad(n) {
  return n < 10 ? '0' + n.toString(10) : n.toString(10);
}


var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
              'Oct', 'Nov', 'Dec'];

// 26 Feb 16:19:34
function timestamp() {
  var d = new Date();
  var time = [pad(d.getHours()),
              pad(d.getMinutes()),
              pad(d.getSeconds())].join(':');
  return [d.getDate(), months[d.getMonth()], time].join(' ');
}


// log is just a thin wrapper to console.log that prepends a timestamp
exports.log = function() {
  console.log('%s - %s', timestamp(), exports.format.apply(exports, arguments));
};


/**
 * Inherit the prototype methods from one constructor into another.
 *
 * The Function.prototype.inherits from lang.js rewritten as a standalone
 * function (not on Function.prototype). NOTE: If this file is to be loaded
 * during bootstrapping this function needs to be rewritten using some native
 * functions as prototype setup using normal JavaScript does not work as
 * expected during bootstrapping (see mirror.js in r114903).
 *
 * @param {function} ctor Constructor function which needs to inherit the
 *     prototype.
 * @param {function} superCtor Constructor function to inherit prototype from.
 */
exports.inherits = __webpack_require__(/*! inherits */ "./node_modules/node-libs-browser/node_modules/util/node_modules/inherits/inherits_browser.js");

exports._extend = function(origin, add) {
  // Don't do anything if add isn't an object
  if (!add || !isObject(add)) return origin;

  var keys = Object.keys(add);
  var i = keys.length;
  while (i--) {
    origin[keys[i]] = add[keys[i]];
  }
  return origin;
};

function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

var kCustomPromisifiedSymbol = typeof Symbol !== 'undefined' ? Symbol('util.promisify.custom') : undefined;

exports.promisify = function promisify(original) {
  if (typeof original !== 'function')
    throw new TypeError('The "original" argument must be of type Function');

  if (kCustomPromisifiedSymbol && original[kCustomPromisifiedSymbol]) {
    var fn = original[kCustomPromisifiedSymbol];
    if (typeof fn !== 'function') {
      throw new TypeError('The "util.promisify.custom" argument must be of type Function');
    }
    Object.defineProperty(fn, kCustomPromisifiedSymbol, {
      value: fn, enumerable: false, writable: false, configurable: true
    });
    return fn;
  }

  function fn() {
    var promiseResolve, promiseReject;
    var promise = new Promise(function (resolve, reject) {
      promiseResolve = resolve;
      promiseReject = reject;
    });

    var args = [];
    for (var i = 0; i < arguments.length; i++) {
      args.push(arguments[i]);
    }
    args.push(function (err, value) {
      if (err) {
        promiseReject(err);
      } else {
        promiseResolve(value);
      }
    });

    try {
      original.apply(this, args);
    } catch (err) {
      promiseReject(err);
    }

    return promise;
  }

  Object.setPrototypeOf(fn, Object.getPrototypeOf(original));

  if (kCustomPromisifiedSymbol) Object.defineProperty(fn, kCustomPromisifiedSymbol, {
    value: fn, enumerable: false, writable: false, configurable: true
  });
  return Object.defineProperties(
    fn,
    getOwnPropertyDescriptors(original)
  );
}

exports.promisify.custom = kCustomPromisifiedSymbol

function callbackifyOnRejected(reason, cb) {
  // `!reason` guard inspired by bluebird (Ref: https://goo.gl/t5IS6M).
  // Because `null` is a special error value in callbacks which means "no error
  // occurred", we error-wrap so the callback consumer can distinguish between
  // "the promise rejected with null" or "the promise fulfilled with undefined".
  if (!reason) {
    var newReason = new Error('Promise was rejected with a falsy value');
    newReason.reason = reason;
    reason = newReason;
  }
  return cb(reason);
}

function callbackify(original) {
  if (typeof original !== 'function') {
    throw new TypeError('The "original" argument must be of type Function');
  }

  // We DO NOT return the promise as it gives the user a false sense that
  // the promise is actually somehow related to the callback's execution
  // and that the callback throwing will reject the promise.
  function callbackified() {
    var args = [];
    for (var i = 0; i < arguments.length; i++) {
      args.push(arguments[i]);
    }

    var maybeCb = args.pop();
    if (typeof maybeCb !== 'function') {
      throw new TypeError('The last argument must be of type Function');
    }
    var self = this;
    var cb = function() {
      return maybeCb.apply(self, arguments);
    };
    // In true node style we process the callback on `nextTick` with all the
    // implications (stack, `uncaughtException`, `async_hooks`)
    original.apply(this, args)
      .then(function(ret) { process.nextTick(cb, null, ret) },
            function(rej) { process.nextTick(callbackifyOnRejected, rej, cb) });
  }

  Object.setPrototypeOf(callbackified, Object.getPrototypeOf(original));
  Object.defineProperties(callbackified,
                          getOwnPropertyDescriptors(original));
  return callbackified;
}
exports.callbackify = callbackify;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/object-assign/index.js":
/*!*********************************************!*\
  !*** ./node_modules/object-assign/index.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*
object-assign
(c) Sindre Sorhus
@license MIT
*/


/* eslint-disable no-unused-vars */
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var propIsEnumerable = Object.prototype.propertyIsEnumerable;

function toObject(val) {
	if (val === null || val === undefined) {
		throw new TypeError('Object.assign cannot be called with null or undefined');
	}

	return Object(val);
}

function shouldUseNative() {
	try {
		if (!Object.assign) {
			return false;
		}

		// Detect buggy property enumeration order in older V8 versions.

		// https://bugs.chromium.org/p/v8/issues/detail?id=4118
		var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
		test1[5] = 'de';
		if (Object.getOwnPropertyNames(test1)[0] === '5') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test2 = {};
		for (var i = 0; i < 10; i++) {
			test2['_' + String.fromCharCode(i)] = i;
		}
		var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
			return test2[n];
		});
		if (order2.join('') !== '0123456789') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test3 = {};
		'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
			test3[letter] = letter;
		});
		if (Object.keys(Object.assign({}, test3)).join('') !==
				'abcdefghijklmnopqrst') {
			return false;
		}

		return true;
	} catch (err) {
		// We don't expect any of the above to throw, but better to be safe.
		return false;
	}
}

module.exports = shouldUseNative() ? Object.assign : function (target, source) {
	var from;
	var to = toObject(target);
	var symbols;

	for (var s = 1; s < arguments.length; s++) {
		from = Object(arguments[s]);

		for (var key in from) {
			if (hasOwnProperty.call(from, key)) {
				to[key] = from[key];
			}
		}

		if (getOwnPropertySymbols) {
			symbols = getOwnPropertySymbols(from);
			for (var i = 0; i < symbols.length; i++) {
				if (propIsEnumerable.call(from, symbols[i])) {
					to[symbols[i]] = from[symbols[i]];
				}
			}
		}
	}

	return to;
};


/***/ }),

/***/ "./node_modules/process/browser.js":
/*!*****************************************!*\
  !*** ./node_modules/process/browser.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "./src/extractors/chroma.js":
/*!**********************************!*\
  !*** ./src/extractors/chroma.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/* harmony default export */ __webpack_exports__["default"] = (function (args) {
  if (_typeof(args.ampSpectrum) !== 'object') {
    throw new TypeError('Valid ampSpectrum is required to generate chroma');
  }

  if (_typeof(args.chromaFilterBank) !== 'object') {
    throw new TypeError('Valid chromaFilterBank is required to generate chroma');
  }

  var chromagram = args.chromaFilterBank.map(function (row, i) {
    return args.ampSpectrum.reduce(function (acc, v, j) {
      return acc + v * row[j];
    }, 0);
  });
  var maxVal = Math.max.apply(Math, _toConsumableArray(chromagram));
  return maxVal ? chromagram.map(function (v) {
    return v / maxVal;
  }) : chromagram;
});

/***/ }),

/***/ "./src/extractors/energy.js":
/*!**********************************!*\
  !*** ./src/extractors/energy.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var assert__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! assert */ "./node_modules/assert/assert.js");
/* harmony import */ var assert__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(assert__WEBPACK_IMPORTED_MODULE_0__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }


/* harmony default export */ __webpack_exports__["default"] = (function () {
  if (_typeof(arguments[0].signal) !== 'object') {
    throw new TypeError();
  }

  var energy = 0;

  for (var i = 0; i < arguments[0].signal.length; i++) {
    energy += Math.pow(Math.abs(arguments[0].signal[i]), 2);
  }

  return energy;
});

/***/ }),

/***/ "./src/extractors/extractorUtilities.js":
/*!**********************************************!*\
  !*** ./src/extractors/extractorUtilities.js ***!
  \**********************************************/
/*! exports provided: mu */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mu", function() { return mu; });
function mu(i, amplitudeSpect) {
  var numerator = 0;
  var denominator = 0;

  for (var k = 0; k < amplitudeSpect.length; k++) {
    numerator += Math.pow(k, i) * Math.abs(amplitudeSpect[k]);
    denominator += amplitudeSpect[k];
  }

  return numerator / denominator;
}

/***/ }),

/***/ "./src/extractors/loudness.js":
/*!************************************!*\
  !*** ./src/extractors/loudness.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/* harmony default export */ __webpack_exports__["default"] = (function (args) {
  if (_typeof(args.ampSpectrum) !== 'object' || _typeof(args.barkScale) !== 'object') {
    throw new TypeError();
  }

  var NUM_BARK_BANDS = 24;
  var specific = new Float32Array(NUM_BARK_BANDS);
  var total = 0;
  var normalisedSpectrum = args.ampSpectrum;
  var bbLimits = new Int32Array(NUM_BARK_BANDS + 1);
  bbLimits[0] = 0;
  var currentBandEnd = args.barkScale[normalisedSpectrum.length - 1] / NUM_BARK_BANDS;
  var currentBand = 1;

  for (var i = 0; i < normalisedSpectrum.length; i++) {
    while (args.barkScale[i] > currentBandEnd) {
      bbLimits[currentBand++] = i;
      currentBandEnd = currentBand * args.barkScale[normalisedSpectrum.length - 1] / NUM_BARK_BANDS;
    }
  }

  bbLimits[NUM_BARK_BANDS] = normalisedSpectrum.length - 1; //process

  for (var _i = 0; _i < NUM_BARK_BANDS; _i++) {
    var sum = 0;

    for (var j = bbLimits[_i]; j < bbLimits[_i + 1]; j++) {
      sum += normalisedSpectrum[j];
    }

    specific[_i] = Math.pow(sum, 0.23);
  } //get total loudness


  for (var _i2 = 0; _i2 < specific.length; _i2++) {
    total += specific[_i2];
  }

  return {
    specific: specific,
    total: total
  };
});

/***/ }),

/***/ "./src/extractors/mfcc.js":
/*!********************************!*\
  !*** ./src/extractors/mfcc.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _powerSpectrum__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./powerSpectrum */ "./src/extractors/powerSpectrum.js");
/* harmony import */ var _utilities__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../utilities */ "./src/utilities.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }





var dct = __webpack_require__(/*! dct */ "./node_modules/dct/index.js");

/* harmony default export */ __webpack_exports__["default"] = (function (args) {
  if (_typeof(args.ampSpectrum) !== 'object') {
    throw new TypeError('Valid ampSpectrum is required to generate MFCC');
  }

  if (_typeof(args.melFilterBank) !== 'object') {
    throw new TypeError('Valid melFilterBank is required to generate MFCC');
  }

  var numberOfMFCCCoefficients = Math.min(40, Math.max(1, args.numberOfMFCCCoefficients || 13)); // Tutorial from:
  // http://practicalcryptography.com/miscellaneous/machine-learning
  // /guide-mel-frequency-cepstral-coefficients-mfccs/

  var powSpec = Object(_powerSpectrum__WEBPACK_IMPORTED_MODULE_0__["default"])(args);
  var numFilters = args.melFilterBank.length;
  var filtered = Array(numFilters);

  if (numFilters < numberOfMFCCCoefficients) {
    throw new Error("Insufficient filter bank for requested number of coefficients");
  }

  var loggedMelBands = new Float32Array(numFilters);

  for (var i = 0; i < loggedMelBands.length; i++) {
    filtered[i] = new Float32Array(args.bufferSize / 2);
    loggedMelBands[i] = 0;

    for (var j = 0; j < args.bufferSize / 2; j++) {
      //point-wise multiplication between power spectrum and filterbanks.
      filtered[i][j] = args.melFilterBank[i][j] * powSpec[j]; //summing up all of the coefficients into one array

      loggedMelBands[i] += filtered[i][j];
    } //log each coefficient.


    loggedMelBands[i] = Math.log(loggedMelBands[i] + 1);
  } //dct


  var loggedMelBandsArray = Array.prototype.slice.call(loggedMelBands);
  var mfccs = dct(loggedMelBandsArray).slice(0, numberOfMFCCCoefficients);
  return mfccs;
});

/***/ }),

/***/ "./src/extractors/perceptualSharpness.js":
/*!***********************************************!*\
  !*** ./src/extractors/perceptualSharpness.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _loudness__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./loudness */ "./src/extractors/loudness.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }


/* harmony default export */ __webpack_exports__["default"] = (function () {
  if (_typeof(arguments[0].signal) !== 'object') {
    throw new TypeError();
  }

  var loudnessValue = Object(_loudness__WEBPACK_IMPORTED_MODULE_0__["default"])(arguments[0]);
  var spec = loudnessValue.specific;
  var output = 0;

  for (var i = 0; i < spec.length; i++) {
    if (i < 15) {
      output += (i + 1) * spec[i + 1];
    } else {
      output += 0.066 * Math.exp(0.171 * (i + 1));
    }
  }

  output *= 0.11 / loudnessValue.total;
  return output;
});

/***/ }),

/***/ "./src/extractors/perceptualSpread.js":
/*!********************************************!*\
  !*** ./src/extractors/perceptualSpread.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _loudness__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./loudness */ "./src/extractors/loudness.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }


/* harmony default export */ __webpack_exports__["default"] = (function () {
  if (_typeof(arguments[0].signal) !== 'object') {
    throw new TypeError();
  }

  var loudnessValue = Object(_loudness__WEBPACK_IMPORTED_MODULE_0__["default"])(arguments[0]);
  var max = 0;

  for (var i = 0; i < loudnessValue.specific.length; i++) {
    if (loudnessValue.specific[i] > max) {
      max = loudnessValue.specific[i];
    }
  }

  var spread = Math.pow((loudnessValue.total - max) / loudnessValue.total, 2);
  return spread;
});

/***/ }),

/***/ "./src/extractors/powerSpectrum.js":
/*!*****************************************!*\
  !*** ./src/extractors/powerSpectrum.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/* harmony default export */ __webpack_exports__["default"] = (function () {
  if (_typeof(arguments[0].ampSpectrum) !== 'object') {
    throw new TypeError();
  }

  var powerSpectrum = new Float32Array(arguments[0].ampSpectrum.length);

  for (var i = 0; i < powerSpectrum.length; i++) {
    powerSpectrum[i] = Math.pow(arguments[0].ampSpectrum[i], 2);
  }

  return powerSpectrum;
});

/***/ }),

/***/ "./src/extractors/rms.js":
/*!*******************************!*\
  !*** ./src/extractors/rms.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/* harmony default export */ __webpack_exports__["default"] = (function (args) {
  if (_typeof(args.signal) !== 'object') {
    throw new TypeError();
  }

  var rms = 0;

  for (var i = 0; i < args.signal.length; i++) {
    rms += Math.pow(args.signal[i], 2);
  }

  rms = rms / args.signal.length;
  rms = Math.sqrt(rms);
  return rms;
});

/***/ }),

/***/ "./src/extractors/spectralCentroid.js":
/*!********************************************!*\
  !*** ./src/extractors/spectralCentroid.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _extractorUtilities__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./extractorUtilities */ "./src/extractors/extractorUtilities.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }


/* harmony default export */ __webpack_exports__["default"] = (function () {
  if (_typeof(arguments[0].ampSpectrum) !== 'object') {
    throw new TypeError();
  }

  return Object(_extractorUtilities__WEBPACK_IMPORTED_MODULE_0__["mu"])(1, arguments[0].ampSpectrum);
});

/***/ }),

/***/ "./src/extractors/spectralFlatness.js":
/*!********************************************!*\
  !*** ./src/extractors/spectralFlatness.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/* harmony default export */ __webpack_exports__["default"] = (function () {
  if (_typeof(arguments[0].ampSpectrum) !== 'object') {
    throw new TypeError();
  }

  var numerator = 0;
  var denominator = 0;

  for (var i = 0; i < arguments[0].ampSpectrum.length; i++) {
    numerator += Math.log(arguments[0].ampSpectrum[i]);
    denominator += arguments[0].ampSpectrum[i];
  }

  return Math.exp(numerator / arguments[0].ampSpectrum.length) * arguments[0].ampSpectrum.length / denominator;
});

/***/ }),

/***/ "./src/extractors/spectralFlux.js":
/*!****************************************!*\
  !*** ./src/extractors/spectralFlux.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/* harmony default export */ __webpack_exports__["default"] = (function (args) {
  if (_typeof(args.signal) !== 'object' || _typeof(args.previousSignal) != 'object') {
    throw new TypeError();
  }

  var sf = 0;

  for (var i = -(args.bufferSize / 2); i < signal.length / 2 - 1; i++) {
    x = Math.abs(args.signal[i]) - Math.abs(args.previousSignal[i]);
    sf += (x + Math.abs(x)) / 2;
  }

  return sf;
});

/***/ }),

/***/ "./src/extractors/spectralKurtosis.js":
/*!********************************************!*\
  !*** ./src/extractors/spectralKurtosis.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _extractorUtilities__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./extractorUtilities */ "./src/extractors/extractorUtilities.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }


/* harmony default export */ __webpack_exports__["default"] = (function () {
  if (_typeof(arguments[0].ampSpectrum) !== 'object') {
    throw new TypeError();
  }

  var ampspec = arguments[0].ampSpectrum;
  var mu1 = Object(_extractorUtilities__WEBPACK_IMPORTED_MODULE_0__["mu"])(1, ampspec);
  var mu2 = Object(_extractorUtilities__WEBPACK_IMPORTED_MODULE_0__["mu"])(2, ampspec);
  var mu3 = Object(_extractorUtilities__WEBPACK_IMPORTED_MODULE_0__["mu"])(3, ampspec);
  var mu4 = Object(_extractorUtilities__WEBPACK_IMPORTED_MODULE_0__["mu"])(4, ampspec);
  var numerator = -3 * Math.pow(mu1, 4) + 6 * mu1 * mu2 - 4 * mu1 * mu3 + mu4;
  var denominator = Math.pow(Math.sqrt(mu2 - Math.pow(mu1, 2)), 4);
  return numerator / denominator;
});

/***/ }),

/***/ "./src/extractors/spectralRolloff.js":
/*!*******************************************!*\
  !*** ./src/extractors/spectralRolloff.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/* harmony default export */ __webpack_exports__["default"] = (function () {
  if (_typeof(arguments[0].ampSpectrum) !== 'object') {
    throw new TypeError();
  }

  var ampspec = arguments[0].ampSpectrum; //calculate nyquist bin

  var nyqBin = arguments[0].sampleRate / (2 * (ampspec.length - 1));
  var ec = 0;

  for (var i = 0; i < ampspec.length; i++) {
    ec += ampspec[i];
  }

  var threshold = 0.99 * ec;
  var n = ampspec.length - 1;

  while (ec > threshold && n >= 0) {
    ec -= ampspec[n];
    --n;
  }

  return (n + 1) * nyqBin;
});

/***/ }),

/***/ "./src/extractors/spectralSkewness.js":
/*!********************************************!*\
  !*** ./src/extractors/spectralSkewness.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _extractorUtilities__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./extractorUtilities */ "./src/extractors/extractorUtilities.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }


/* harmony default export */ __webpack_exports__["default"] = (function (args) {
  if (_typeof(args.ampSpectrum) !== 'object') {
    throw new TypeError();
  }

  var mu1 = Object(_extractorUtilities__WEBPACK_IMPORTED_MODULE_0__["mu"])(1, args.ampSpectrum);
  var mu2 = Object(_extractorUtilities__WEBPACK_IMPORTED_MODULE_0__["mu"])(2, args.ampSpectrum);
  var mu3 = Object(_extractorUtilities__WEBPACK_IMPORTED_MODULE_0__["mu"])(3, args.ampSpectrum);
  var numerator = 2 * Math.pow(mu1, 3) - 3 * mu1 * mu2 + mu3;
  var denominator = Math.pow(Math.sqrt(mu2 - Math.pow(mu1, 2)), 3);
  return numerator / denominator;
});

/***/ }),

/***/ "./src/extractors/spectralSlope.js":
/*!*****************************************!*\
  !*** ./src/extractors/spectralSlope.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/* harmony default export */ __webpack_exports__["default"] = (function (args) {
  if (_typeof(args.ampSpectrum) !== 'object') {
    throw new TypeError();
  } //linear regression


  var ampSum = 0;
  var freqSum = 0;
  var freqs = new Float32Array(args.ampSpectrum.length);
  var powFreqSum = 0;
  var ampFreqSum = 0;

  for (var i = 0; i < args.ampSpectrum.length; i++) {
    ampSum += args.ampSpectrum[i];
    var curFreq = i * args.sampleRate / args.bufferSize;
    freqs[i] = curFreq;
    powFreqSum += curFreq * curFreq;
    freqSum += curFreq;
    ampFreqSum += curFreq * args.ampSpectrum[i];
  }

  return (args.ampSpectrum.length * ampFreqSum - freqSum * ampSum) / (ampSum * (powFreqSum - Math.pow(freqSum, 2)));
});

/***/ }),

/***/ "./src/extractors/spectralSpread.js":
/*!******************************************!*\
  !*** ./src/extractors/spectralSpread.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _extractorUtilities__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./extractorUtilities */ "./src/extractors/extractorUtilities.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }


/* harmony default export */ __webpack_exports__["default"] = (function (args) {
  if (_typeof(args.ampSpectrum) !== 'object') {
    throw new TypeError();
  }

  return Math.sqrt(Object(_extractorUtilities__WEBPACK_IMPORTED_MODULE_0__["mu"])(2, args.ampSpectrum) - Math.pow(Object(_extractorUtilities__WEBPACK_IMPORTED_MODULE_0__["mu"])(1, args.ampSpectrum), 2));
});

/***/ }),

/***/ "./src/extractors/zcr.js":
/*!*******************************!*\
  !*** ./src/extractors/zcr.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/* harmony default export */ __webpack_exports__["default"] = (function () {
  if (_typeof(arguments[0].signal) !== 'object') {
    throw new TypeError();
  }

  var zcr = 0;

  for (var i = 1; i < arguments[0].signal.length; i++) {
    if (arguments[0].signal[i - 1] >= 0 && arguments[0].signal[i] < 0 || arguments[0].signal[i - 1] < 0 && arguments[0].signal[i] >= 0) {
      zcr++;
    }
  }

  return zcr;
});

/***/ }),

/***/ "./src/featureExtractors.js":
/*!**********************************!*\
  !*** ./src/featureExtractors.js ***!
  \**********************************/
/*! exports provided: buffer, rms, energy, complexSpectrum, spectralSlope, spectralCentroid, spectralRolloff, spectralFlatness, spectralSpread, spectralSkewness, spectralKurtosis, amplitudeSpectrum, zcr, loudness, perceptualSpread, perceptualSharpness, powerSpectrum, mfcc, chroma, spectralFlux */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "buffer", function() { return buffer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "complexSpectrum", function() { return complexSpectrum; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "amplitudeSpectrum", function() { return amplitudeSpectrum; });
/* harmony import */ var _extractors_rms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./extractors/rms */ "./src/extractors/rms.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "rms", function() { return _extractors_rms__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _extractors_energy__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./extractors/energy */ "./src/extractors/energy.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "energy", function() { return _extractors_energy__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _extractors_spectralSlope__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./extractors/spectralSlope */ "./src/extractors/spectralSlope.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "spectralSlope", function() { return _extractors_spectralSlope__WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _extractors_spectralCentroid__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./extractors/spectralCentroid */ "./src/extractors/spectralCentroid.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "spectralCentroid", function() { return _extractors_spectralCentroid__WEBPACK_IMPORTED_MODULE_3__["default"]; });

/* harmony import */ var _extractors_spectralRolloff__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./extractors/spectralRolloff */ "./src/extractors/spectralRolloff.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "spectralRolloff", function() { return _extractors_spectralRolloff__WEBPACK_IMPORTED_MODULE_4__["default"]; });

/* harmony import */ var _extractors_spectralFlatness__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./extractors/spectralFlatness */ "./src/extractors/spectralFlatness.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "spectralFlatness", function() { return _extractors_spectralFlatness__WEBPACK_IMPORTED_MODULE_5__["default"]; });

/* harmony import */ var _extractors_spectralSpread__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./extractors/spectralSpread */ "./src/extractors/spectralSpread.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "spectralSpread", function() { return _extractors_spectralSpread__WEBPACK_IMPORTED_MODULE_6__["default"]; });

/* harmony import */ var _extractors_spectralSkewness__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./extractors/spectralSkewness */ "./src/extractors/spectralSkewness.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "spectralSkewness", function() { return _extractors_spectralSkewness__WEBPACK_IMPORTED_MODULE_7__["default"]; });

/* harmony import */ var _extractors_spectralKurtosis__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./extractors/spectralKurtosis */ "./src/extractors/spectralKurtosis.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "spectralKurtosis", function() { return _extractors_spectralKurtosis__WEBPACK_IMPORTED_MODULE_8__["default"]; });

/* harmony import */ var _extractors_zcr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./extractors/zcr */ "./src/extractors/zcr.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "zcr", function() { return _extractors_zcr__WEBPACK_IMPORTED_MODULE_9__["default"]; });

/* harmony import */ var _extractors_loudness__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./extractors/loudness */ "./src/extractors/loudness.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "loudness", function() { return _extractors_loudness__WEBPACK_IMPORTED_MODULE_10__["default"]; });

/* harmony import */ var _extractors_perceptualSpread__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./extractors/perceptualSpread */ "./src/extractors/perceptualSpread.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "perceptualSpread", function() { return _extractors_perceptualSpread__WEBPACK_IMPORTED_MODULE_11__["default"]; });

/* harmony import */ var _extractors_perceptualSharpness__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./extractors/perceptualSharpness */ "./src/extractors/perceptualSharpness.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "perceptualSharpness", function() { return _extractors_perceptualSharpness__WEBPACK_IMPORTED_MODULE_12__["default"]; });

/* harmony import */ var _extractors_mfcc__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./extractors/mfcc */ "./src/extractors/mfcc.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "mfcc", function() { return _extractors_mfcc__WEBPACK_IMPORTED_MODULE_13__["default"]; });

/* harmony import */ var _extractors_chroma__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./extractors/chroma */ "./src/extractors/chroma.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "chroma", function() { return _extractors_chroma__WEBPACK_IMPORTED_MODULE_14__["default"]; });

/* harmony import */ var _extractors_powerSpectrum__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./extractors/powerSpectrum */ "./src/extractors/powerSpectrum.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "powerSpectrum", function() { return _extractors_powerSpectrum__WEBPACK_IMPORTED_MODULE_15__["default"]; });

/* harmony import */ var _extractors_spectralFlux__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./extractors/spectralFlux */ "./src/extractors/spectralFlux.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "spectralFlux", function() { return _extractors_spectralFlux__WEBPACK_IMPORTED_MODULE_16__["default"]; });



















var buffer = function buffer(args) {
  return args.signal;
};

var complexSpectrum = function complexSpectrum(args) {
  return args.complexSpectrum;
};

var amplitudeSpectrum = function amplitudeSpectrum(args) {
  return args.ampSpectrum;
};



/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./main */ "./src/main.js")["default"];

/***/ }),

/***/ "./src/main.js":
/*!*********************!*\
  !*** ./src/main.js ***!
  \*********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utilities__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utilities */ "./src/utilities.js");
/* harmony import */ var _featureExtractors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./featureExtractors */ "./src/featureExtractors.js");
/* harmony import */ var fftjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! fftjs */ "./node_modules/fftjs/dist/fft.js");
/* harmony import */ var fftjs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(fftjs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _meyda_wa__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./meyda-wa */ "./src/meyda-wa.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }





/**
 * Meyda Module
 * @module meyda
 */

/**
 * Options for constructing a MeydaAnalyzer
 * @typedef {Object} MeydaOptions
 * @property {AudioContext} audioContext - The Audio Context for the MeydaAnalyzer to operate in.
 * @property {AudioNode} source - The Audio Node for Meyda to listen to.
 * @property {number} [bufferSize] - The size of the buffer.
 * @property {number} [hopSize] - The hop size between buffers.
 * @property {number} [sampleRate] - The number of samples per second in the audio context.
 * @property {Function} [callback] - A function to receive the frames of audio features
 * @property {string} [windowingFunction] - The Windowing Function to apply to the signal before transformation to the frequency domain
 * @property {string|Array.<string>} [featureExtractors] - Specify the feature extractors you want to run on the audio.
 * @property {boolean} [startImmediately] - Pass `true` to start feature extraction immediately
 * @property {number} [numberOfMFCCCoefficients] - The number of MFCC co-efficients that the MFCC feature extractor should return
 */

/**
 * Web Audio context
 * Either an {@link AudioContext|https://developer.mozilla.org/en-US/docs/Web/API/AudioContext}
 * or an {@link OfflineAudioContext|https://developer.mozilla.org/en-US/docs/Web/API/OfflineAudioContext}
 * @typedef {Object} AudioContext
 */

/**
 * AudioNode
 * A Web AudioNode
 * @typedef {Object} AudioNode
 */

/**
 * ScriptProcessorNode
 * A Web Audio ScriptProcessorNode
 * @typedef {Object} ScriptProcessorNode
 */

/**
 * @class Meyda
 * @hideconstructor
 * @classdesc
 * The schema for the default export of the Meyda library.
 * @example
 * var Meyda = require('meyda');
 */

var Meyda = {
  /**
   * Meyda stores a reference to the relevant audio context here for use inside
   * the Web Audio API.
   * @instance
   * @member {AudioContext}
   */
  audioContext: null,

  /**
   * Meyda keeps an internal ScriptProcessorNode in which it runs audio feature
   * extraction. The ScriptProcessorNode is stored in this member variable.
   * @instance
   * @member {ScriptProcessorNode}
   */
  spn: null,

  /**
   * The length of each buffer that Meyda will extract audio on. When recieving
   * input via the Web Audio API, the Script Processor Node chunks incoming audio
   * into arrays of this length. Longer buffers allow for more precision in the
   * frequency domain, but increase the amount of time it takes for Meyda to
   * output a set of audio features for the buffer. You can calculate how many
   * sets of audio features Meyda will output per second by dividing the
   * buffer size by the sample rate. If you're using Meyda for visualisation,
   * make sure that you're collecting audio features at a rate that's faster
   * than or equal to the video frame rate you expect.
   * @instance
   * @member {number}
   */
  bufferSize: 512,

  /**
   * The number of samples per second of the incoming audio. This affects
   * feature extraction outside of the context of the Web Audio API, and must be
   * set accurately - otherwise calculations will be off.
   * @instance
   * @member {number}
   */
  sampleRate: 44100,

  /**
   * The number of Mel bands to use in the Mel Frequency Cepstral Co-efficients
   * feature extractor
   * @instance
   * @member {number}
   */
  melBands: 26,

  /**
   * The number of bands to divide the spectrum into for the Chroma feature
   * extractor. 12 is the standard number of semitones per octave in the western
   * music tradition, but Meyda can use an arbitrary number of bands, which
   * can be useful for microtonal music.
   * @instance
   * @member {number}
   */
  chromaBands: 12,

  /**
   * A function you can provide that will be called for each buffer that Meyda
   * receives from its source node
   * @instance
   * @member {Function}
   */
  callback: null,

  /**
   * Specify the windowing function to apply to the buffer before the
   * transformation from the time domain to the frequency domain is performed
   *
   * The default windowing function is the hanning window.
   *
   * @instance
   * @member {string}
   */
  windowingFunction: 'hanning',

  /**
   * @member {object}
   */
  featureExtractors: _featureExtractors__WEBPACK_IMPORTED_MODULE_1__,
  EXTRACTION_STARTED: false,

  /**
   * The number of MFCC co-efficients that the MFCC feature extractor should return
   * @instance
   * @member {number}
   */
  numberOfMFCCCoefficients: 13,
  _featuresToExtract: [],
  windowing: _utilities__WEBPACK_IMPORTED_MODULE_0__["applyWindow"],
  _errors: {
    notPow2: new Error('Meyda: Buffer size must be a power of 2, e.g. 64 or 512'),
    featureUndef: new Error('Meyda: No features defined.'),
    invalidFeatureFmt: new Error('Meyda: Invalid feature format'),
    invalidInput: new Error('Meyda: Invalid input.'),
    noAC: new Error('Meyda: No AudioContext specified.'),
    noSource: new Error('Meyda: No source node specified.')
  },

  /**
   * @summary
   * Create a MeydaAnalyzer
   *
   * A factory function for creating a MeydaAnalyzer, the interface for using
   * Meyda in the context of Web Audio.
   *
   * @method
   * @param {MeydaOptions} options Options - an object containing configuration
   * @returns {MeydaAnalyzer}
   * @example
   * const analyzer = Meyda.createMeydaAnalyzer({
   *   "audioContext": audioContext,
   *   "source": source,
   *   "bufferSize": 512,
   *   "featureExtractors": ["rms"],
   *   "inputs": 2,
   *   "callback": features => {
   *     levelRangeElement.value = features.rms;
   *   }
   * });
   */
  createMeydaAnalyzer: function createMeydaAnalyzer(options) {
    return new _meyda_wa__WEBPACK_IMPORTED_MODULE_3__["MeydaAnalyzer"](options, Object.assign({}, Meyda));
  },

  /**
   * Extract an audio feature from a buffer
   *
   * Unless `meyda.windowingFunction` is set otherwise, `extract` will
   * internally apply a hanning window to the buffer prior to conversion into
   * the frequency domain.
   *
   * @function
   * @param {(string|Array.<string>)} feature - the feature you want to extract
   * @param {Array.<number>} signal
   * An array of numbers that represents the signal. It should be of length
   * `meyda.bufferSize`
   * @param {Array.<number>} [previousSignal] - the previous buffer
   * @returns {object} Features
   * @example
   * meyda.bufferSize = 2048;
   * const features = meyda.extract(['zcr', 'spectralCentroid'], signal);
   */
  extract: function extract(feature, signal, previousSignal) {
    var _this = this;

    if (!signal) throw this._errors.invalidInput;else if (_typeof(signal) != 'object') throw this._errors.invalidInput;else if (!feature) throw this._errors.featureUndef;else if (!_utilities__WEBPACK_IMPORTED_MODULE_0__["isPowerOfTwo"](signal.length)) throw this._errors.notPow2;

    if (typeof this.barkScale == 'undefined' || this.barkScale.length != this.bufferSize) {
      this.barkScale = _utilities__WEBPACK_IMPORTED_MODULE_0__["createBarkScale"](this.bufferSize, this.sampleRate, this.bufferSize);
    } // Recalculate mel bank if buffer length changed


    if (typeof this.melFilterBank == 'undefined' || this.barkScale.length != this.bufferSize || this.melFilterBank.length != this.melBands) {
      this.melFilterBank = _utilities__WEBPACK_IMPORTED_MODULE_0__["createMelFilterBank"](Math.max(this.melBands, this.numberOfMFCCCoefficients), this.sampleRate, this.bufferSize);
    } // Recalculate chroma bank if buffer length changed


    if (typeof this.chromaFilterBank == 'undefined' || this.chromaFilterBank.length != this.chromaBands) {
      this.chromaFilterBank = _utilities__WEBPACK_IMPORTED_MODULE_0__["createChromaFilterBank"](this.chromaBands, this.sampleRate, this.bufferSize);
    }

    if (typeof signal.buffer == 'undefined') {
      //signal is a normal array, convert to F32A
      this.signal = _utilities__WEBPACK_IMPORTED_MODULE_0__["arrayToTyped"](signal);
    } else {
      this.signal = signal;
    }

    var preparedSignal = prepareSignalWithSpectrum(signal, this.windowingFunction, this.bufferSize);
    this.signal = preparedSignal.windowedSignal;
    this.complexSpectrum = preparedSignal.complexSpectrum;
    this.ampSpectrum = preparedSignal.ampSpectrum;

    if (previousSignal) {
      var _preparedSignal = prepareSignalWithSpectrum(previousSignal, this.windowingFunction, this.bufferSize);

      this.previousSignal = _preparedSignal.windowedSignal;
      this.previousComplexSpectrum = _preparedSignal.complexSpectrum;
      this.previousAmpSpectrum = _preparedSignal.ampSpectrum;
    }

    var extract = function extract(feature) {
      return _this.featureExtractors[feature]({
        ampSpectrum: _this.ampSpectrum,
        chromaFilterBank: _this.chromaFilterBank,
        complexSpectrum: _this.complexSpectrum,
        signal: _this.signal,
        bufferSize: _this.bufferSize,
        sampleRate: _this.sampleRate,
        barkScale: _this.barkScale,
        melFilterBank: _this.melFilterBank,
        previousSignal: _this.previousSignal,
        previousAmpSpectrum: _this.previousAmpSpectrum,
        previousComplexSpectrum: _this.previousComplexSpectrum,
        numberOfMFCCCoefficients: _this.numberOfMFCCCoefficients
      });
    };

    if (_typeof(feature) === 'object') {
      return feature.reduce(function (acc, el) {
        return Object.assign({}, acc, _defineProperty({}, el, extract(el)));
      }, {});
    } else if (typeof feature === 'string') {
      return extract(feature);
    } else {
      throw this._errors.invalidFeatureFmt;
    }
  }
};

var prepareSignalWithSpectrum = function prepareSignalWithSpectrum(signal, windowingFunction, bufferSize) {
  var preparedSignal = {};

  if (typeof signal.buffer == 'undefined') {
    //signal is a normal array, convert to F32A
    preparedSignal.signal = _utilities__WEBPACK_IMPORTED_MODULE_0__["arrayToTyped"](signal);
  } else {
    preparedSignal.signal = signal;
  }

  preparedSignal.windowedSignal = _utilities__WEBPACK_IMPORTED_MODULE_0__["applyWindow"](preparedSignal.signal, windowingFunction);
  preparedSignal.complexSpectrum = Object(fftjs__WEBPACK_IMPORTED_MODULE_2__["fft"])(preparedSignal.windowedSignal);
  preparedSignal.ampSpectrum = new Float32Array(bufferSize / 2);

  for (var i = 0; i < bufferSize / 2; i++) {
    preparedSignal.ampSpectrum[i] = Math.sqrt(Math.pow(preparedSignal.complexSpectrum.real[i], 2) + Math.pow(preparedSignal.complexSpectrum.imag[i], 2));
  }

  return preparedSignal;
};
/**
 * The Meyda class
 * @type {Meyda}
 */


/* harmony default export */ __webpack_exports__["default"] = (Meyda);
if (typeof window !== 'undefined') window.Meyda = Meyda;

/***/ }),

/***/ "./src/meyda-wa.js":
/*!*************************!*\
  !*** ./src/meyda-wa.js ***!
  \*************************/
/*! exports provided: MeydaAnalyzer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MeydaAnalyzer", function() { return MeydaAnalyzer; });
/* harmony import */ var _utilities__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utilities */ "./src/utilities.js");
/* harmony import */ var _featureExtractors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./featureExtractors */ "./src/featureExtractors.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



/**
  * MeydaAnalyzer
  * @classdesc
  * Meyda's interface to the Web Audio API. MeydaAnalyzer abstracts an API on
  * top of the Web Audio API's ScriptProcessorNode, running the Meyda audio
  * feature extractors inside that context.
  *
  * MeydaAnalyzer's constructor should not be called directly - MeydaAnalyzer
  * objects should be generated using the {@link Meyda.createMeydaAnalyzer}
  * factory function in the main Meyda class.
  *
  * @example
  * const analyzer = Meyda.createMeydaAnalyzer({
  *   "audioContext": audioContext,
  *   "source": source,
  *   "bufferSize": 512,
  *   "featureExtractors": ["rms"],
  *   "inputs": 2,
  *   "numberOfMFCCCoefficients": 20
  *   "callback": features => {
  *     levelRangeElement.value = features.rms;
  *   }
  * });
  * @hideconstructor
  */

var MeydaAnalyzer = /*#__PURE__*/function () {
  function MeydaAnalyzer(options, _this) {
    var _this2 = this;

    _classCallCheck(this, MeydaAnalyzer);

    this._m = _this;

    if (!options.audioContext) {
      throw this._m.errors.noAC;
    } else if (options.bufferSize && !_utilities__WEBPACK_IMPORTED_MODULE_0__["isPowerOfTwo"](options.bufferSize)) {
      throw this._m._errors.notPow2;
    } else if (!options.source) {
      throw this._m._errors.noSource;
    }

    this._m.audioContext = options.audioContext; // TODO: validate options

    this._m.bufferSize = options.bufferSize || this._m.bufferSize || 256;
    this._m.hopSize = options.hopSize || this._m.hopSize || this._m.bufferSize;
    this._m.sampleRate = options.sampleRate || this._m.audioContext.sampleRate || 44100;
    this._m.callback = options.callback;
    this._m.windowingFunction = options.windowingFunction || 'hanning';
    this._m.featureExtractors = _featureExtractors__WEBPACK_IMPORTED_MODULE_1__;
    this._m.EXTRACTION_STARTED = options.startImmediately || false;
    this._m.channel = typeof options.channel === 'number' ? options.channel : 0;
    this._m.inputs = options.inputs || 1;
    this._m.outputs = options.outputs || 1;
    this._m.numberOfMFCCCoefficients = options.numberOfMFCCCoefficients || this._m.numberOfMFCCCoefficients || 13; //create nodes

    this._m.spn = this._m.audioContext.createScriptProcessor(this._m.bufferSize, this._m.inputs, this._m.outputs);

    this._m.spn.connect(this._m.audioContext.destination);

    this._m._featuresToExtract = options.featureExtractors || []; //always recalculate BS and MFB when a new Meyda analyzer is created.

    this._m.barkScale = _utilities__WEBPACK_IMPORTED_MODULE_0__["createBarkScale"](this._m.bufferSize, this._m.sampleRate, this._m.bufferSize);
    this._m.melFilterBank = _utilities__WEBPACK_IMPORTED_MODULE_0__["createMelFilterBank"](Math.max(this._m.melBands, this._m.numberOfMFCCCoefficients), this._m.sampleRate, this._m.bufferSize);
    this._m.inputData = null;
    this._m.previousInputData = null;
    this._m.frame = null;
    this._m.previousFrame = null;
    this.setSource(options.source);

    this._m.spn.onaudioprocess = function (e) {
      if (_this2._m.inputData !== null) {
        _this2._m.previousInputData = _this2._m.inputData;
      }

      _this2._m.inputData = e.inputBuffer.getChannelData(_this2._m.channel);

      if (!_this2._m.previousInputData) {
        var buffer = _this2._m.inputData;
      } else {
        var buffer = new Float32Array(_this2._m.previousInputData.length + _this2._m.inputData.length - _this2._m.hopSize);
        buffer.set(_this2._m.previousInputData.slice(_this2._m.hopSize));
        buffer.set(_this2._m.inputData, _this2._m.previousInputData.length - _this2._m.hopSize);
      }

      ;
      var frames = _utilities__WEBPACK_IMPORTED_MODULE_0__["frame"](buffer, _this2._m.bufferSize, _this2._m.hopSize);
      frames.forEach(function (f) {
        _this2._m.frame = f;

        var features = _this2._m.extract(_this2._m._featuresToExtract, _this2._m.frame, _this2._m.previousFrame); // call callback if applicable


        if (typeof _this2._m.callback === 'function' && _this2._m.EXTRACTION_STARTED) {
          _this2._m.callback(features);
        }

        _this2._m.previousFrame = _this2._m.frame;
      });
    };
  }
  /**
   * Start feature extraction
   * The audio features will be passed to the callback function that was defined
   * in the MeydaOptions that were passed to the factory when constructing the
   * MeydaAnalyzer.
   * @param {(string|Array.<string>)} [features]
   * Change the features that Meyda is extracting. Defaults to the features that
   * were set upon construction in the options parameter.
   * @example
   * analyzer.start('chroma');
   */


  _createClass(MeydaAnalyzer, [{
    key: "start",
    value: function start(features) {
      this._m._featuresToExtract = features || this._m._featuresToExtract;
      this._m.EXTRACTION_STARTED = true;
    }
    /**
     * Stop feature extraction.
     * @example
     * analyzer.stop();
     */

  }, {
    key: "stop",
    value: function stop() {
      this._m.EXTRACTION_STARTED = false;
    }
    /**
     * Set the Audio Node for Meyda to listen to.
     * @param {AudioNode} source - The Audio Node for Meyda to listen to
     * @example
     * analyzer.setSource(audioSourceNode);
     */

  }, {
    key: "setSource",
    value: function setSource(source) {
      this._m.source && this._m.source.disconnect(this._m.spn);
      this._m.source = source;

      this._m.source.connect(this._m.spn);
    }
    /**
     * Set the channel of the audio node for Meyda to listen to
     * @param {number} channel - the index of the channel on the input audio node
     * for Meyda to listen to.
     * @example
     * analyzer.setChannel(0);
     */

  }, {
    key: "setChannel",
    value: function setChannel(channel) {
      if (channel <= this._m.inputs) {
        this._m.channel = channel;
      } else {
        console.error("Channel ".concat(channel, " does not exist. Make sure you've provided a value for 'inputs' that is greater than ").concat(channel, " when instantiating the MeydaAnalyzer"));
      }
    }
    /**
     * Get a set of features from the current frame.
     * @param {(string|Array.<string>)} [features]
     * Change the features that Meyda is extracting
     * @example
     * analyzer.get('spectralFlatness');
     */

  }, {
    key: "get",
    value: function get(features) {
      if (this._m.inputData) {
        return this._m.extract(features || this._m._featuresToExtract, this._m.inputData, this._m.previousInputData);
      } else {
        return null;
      }
    }
  }]);

  return MeydaAnalyzer;
}();

/***/ }),

/***/ "./src/utilities.js":
/*!**************************!*\
  !*** ./src/utilities.js ***!
  \**************************/
/*! exports provided: isPowerOfTwo, error, pointwiseBufferMult, applyWindow, createBarkScale, typedToArray, arrayToTyped, _normalize, normalize, normalizeToOne, mean, melToFreq, freqToMel, createMelFilterBank, hzToOctaves, normalizeByColumn, createChromaFilterBank, frame */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isPowerOfTwo", function() { return isPowerOfTwo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "error", function() { return error; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pointwiseBufferMult", function() { return pointwiseBufferMult; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "applyWindow", function() { return applyWindow; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createBarkScale", function() { return createBarkScale; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "typedToArray", function() { return typedToArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "arrayToTyped", function() { return arrayToTyped; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "_normalize", function() { return _normalize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "normalize", function() { return normalize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "normalizeToOne", function() { return normalizeToOne; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mean", function() { return mean; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "melToFreq", function() { return melToFreq; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "freqToMel", function() { return freqToMel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createMelFilterBank", function() { return createMelFilterBank; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hzToOctaves", function() { return hzToOctaves; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "normalizeByColumn", function() { return normalizeByColumn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createChromaFilterBank", function() { return createChromaFilterBank; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "frame", function() { return frame; });
/* harmony import */ var _windowing__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./windowing */ "./src/windowing.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }


var windows = {};
function isPowerOfTwo(num) {
  while (num % 2 === 0 && num > 1) {
    num /= 2;
  }

  return num === 1;
}
function error(message) {
  throw new Error('Meyda: ' + message);
}
function pointwiseBufferMult(a, b) {
  var c = [];

  for (var i = 0; i < Math.min(a.length, b.length); i++) {
    c[i] = a[i] * b[i];
  }

  return c;
}
function applyWindow(signal, windowname) {
  if (windowname !== 'rect') {
    if (windowname === '' || !windowname) windowname = 'hanning';
    if (!windows[windowname]) windows[windowname] = {};

    if (!windows[windowname][signal.length]) {
      try {
        windows[windowname][signal.length] = _windowing__WEBPACK_IMPORTED_MODULE_0__[windowname](signal.length);
      } catch (e) {
        throw new Error('Invalid windowing function');
      }
    }

    signal = pointwiseBufferMult(signal, windows[windowname][signal.length]);
  }

  return signal;
}
function createBarkScale(length, sampleRate, bufferSize) {
  var barkScale = new Float32Array(length);

  for (var i = 0; i < barkScale.length; i++) {
    barkScale[i] = i * sampleRate / bufferSize;
    barkScale[i] = 13 * Math.atan(barkScale[i] / 1315.8) + 3.5 * Math.atan(Math.pow(barkScale[i] / 7518, 2));
  }

  return barkScale;
}
function typedToArray(t) {
  // utility to convert typed arrays to normal arrays
  return Array.prototype.slice.call(t);
}
function arrayToTyped(t) {
  // utility to convert arrays to typed F32 arrays
  return Float32Array.from(t);
}
function _normalize(num, range) {
  return num / range;
}
function normalize(a, range) {
  return a.map(function (n) {
    return _normalize(n, range);
  });
}
function normalizeToOne(a) {
  var max = Math.max.apply(null, a);
  return a.map(function (n) {
    return n / max;
  });
}
function mean(a) {
  return a.reduce(function (prev, cur) {
    return prev + cur;
  }) / a.length;
}

function _melToFreq(melValue) {
  var freqValue = 700 * (Math.exp(melValue / 1125) - 1);
  return freqValue;
}

function _freqToMel(freqValue) {
  var melValue = 1125 * Math.log(1 + freqValue / 700);
  return melValue;
}

function melToFreq(mV) {
  return _melToFreq(mV);
}
function freqToMel(fV) {
  return _freqToMel(fV);
}
function createMelFilterBank(numFilters, sampleRate, bufferSize) {
  //the +2 is the upper and lower limits
  var melValues = new Float32Array(numFilters + 2);
  var melValuesInFreq = new Float32Array(numFilters + 2); //Generate limits in Hz - from 0 to the nyquist.

  var lowerLimitFreq = 0;
  var upperLimitFreq = sampleRate / 2; //Convert the limits to Mel

  var lowerLimitMel = _freqToMel(lowerLimitFreq);

  var upperLimitMel = _freqToMel(upperLimitFreq); //Find the range


  var range = upperLimitMel - lowerLimitMel; //Find the range as part of the linear interpolation

  var valueToAdd = range / (numFilters + 1);
  var fftBinsOfFreq = Array(numFilters + 2);

  for (var i = 0; i < melValues.length; i++) {
    // Initialising the mel frequencies
    // They're a linear interpolation between the lower and upper limits.
    melValues[i] = i * valueToAdd; // Convert back to Hz

    melValuesInFreq[i] = _melToFreq(melValues[i]); // Find the corresponding bins

    fftBinsOfFreq[i] = Math.floor((bufferSize + 1) * melValuesInFreq[i] / sampleRate);
  }

  var filterBank = Array(numFilters);

  for (var j = 0; j < filterBank.length; j++) {
    // Create a two dimensional array of size numFilters * (buffersize/2)+1
    // pre-populating the arrays with 0s.
    filterBank[j] = Array.apply(null, new Array(bufferSize / 2 + 1)).map(Number.prototype.valueOf, 0); //creating the lower and upper slopes for each bin

    for (var _i = fftBinsOfFreq[j]; _i < fftBinsOfFreq[j + 1]; _i++) {
      filterBank[j][_i] = (_i - fftBinsOfFreq[j]) / (fftBinsOfFreq[j + 1] - fftBinsOfFreq[j]);
    }

    for (var _i2 = fftBinsOfFreq[j + 1]; _i2 < fftBinsOfFreq[j + 2]; _i2++) {
      filterBank[j][_i2] = (fftBinsOfFreq[j + 2] - _i2) / (fftBinsOfFreq[j + 2] - fftBinsOfFreq[j + 1]);
    }
  }

  return filterBank;
}
function hzToOctaves(freq, A440) {
  return Math.log2(16 * freq / A440);
}
function normalizeByColumn(a) {
  var emptyRow = a[0].map(function () {
    return 0;
  });
  var colDenominators = a.reduce(function (acc, row) {
    row.forEach(function (cell, j) {
      acc[j] += Math.pow(cell, 2);
    });
    return acc;
  }, emptyRow).map(Math.sqrt);
  return a.map(function (row, i) {
    return row.map(function (v, j) {
      return v / (colDenominators[j] || 1);
    });
  });
}
;
function createChromaFilterBank(numFilters, sampleRate, bufferSize) {
  var centerOctave = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 5;
  var octaveWidth = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 2;
  var baseC = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : true;
  var A440 = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : 440;
  var numOutputBins = Math.floor(bufferSize / 2) + 1;
  var frequencyBins = new Array(bufferSize).fill(0).map(function (_, i) {
    return numFilters * hzToOctaves(sampleRate * i / bufferSize, A440);
  }); // Set a value for the 0 Hz bin that is 1.5 octaves below bin 1
  // (so chroma is 50% rotated from bin 1, and bin width is broad)

  frequencyBins[0] = frequencyBins[1] - 1.5 * numFilters;
  var binWidthBins = frequencyBins.slice(1).map(function (v, i) {
    return Math.max(v - frequencyBins[i]);
  }, 1).concat([1]);
  var halfNumFilters = Math.round(numFilters / 2);
  var filterPeaks = new Array(numFilters).fill(0).map(function (_, i) {
    return frequencyBins.map(function (frq) {
      return (10 * numFilters + halfNumFilters + frq - i) % numFilters - halfNumFilters;
    });
  });
  var weights = filterPeaks.map(function (row, i) {
    return row.map(function (_, j) {
      return Math.exp(-0.5 * Math.pow(2 * filterPeaks[i][j] / binWidthBins[j], 2));
    });
  });
  weights = normalizeByColumn(weights);

  if (octaveWidth) {
    var octaveWeights = frequencyBins.map(function (v) {
      return Math.exp(-0.5 * Math.pow((v / numFilters - centerOctave) / octaveWidth, 2));
    });
    weights = weights.map(function (row) {
      return row.map(function (cell, j) {
        return cell * octaveWeights[j];
      });
    });
  }

  if (baseC) {
    weights = [].concat(_toConsumableArray(weights.slice(3)), _toConsumableArray(weights.slice(0, 3)));
  }

  return weights.map(function (row) {
    return row.slice(0, numOutputBins);
  });
}
function frame(buffer, frameLength, hopLength) {
  if (buffer.length < frameLength) {
    throw new Error('Buffer is too short for frame length');
  }

  if (hopLength < 1) {
    throw new Error('Hop length cannot be less that 1');
  }

  if (frameLength < 1) {
    throw new Error('Frame length cannot be less that 1');
  }

  var numFrames = 1 + Math.floor((buffer.length - frameLength) / hopLength);
  return new Array(numFrames).fill(0).map(function (_, i) {
    return buffer.slice(i * hopLength, i * hopLength + frameLength);
  });
}

/***/ }),

/***/ "./src/windowing.js":
/*!**************************!*\
  !*** ./src/windowing.js ***!
  \**************************/
/*! exports provided: blackman, sine, hanning, hamming */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "blackman", function() { return blackman; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sine", function() { return sine; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hanning", function() { return hanning; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hamming", function() { return hamming; });
function blackman(size) {
  var blackmanBuffer = new Float32Array(size);
  var coeff1 = 2 * Math.PI / (size - 1);
  var coeff2 = 2 * coeff1; //According to http://uk.mathworks.com/help/signal/ref/blackman.html
  //first half of the window

  for (var i = 0; i < size / 2; i++) {
    blackmanBuffer[i] = 0.42 - 0.5 * Math.cos(i * coeff1) + 0.08 * Math.cos(i * coeff2);
  } //second half of the window


  for (var _i = size / 2; _i > 0; _i--) {
    blackmanBuffer[size - _i] = blackmanBuffer[_i - 1];
  }

  return blackmanBuffer;
}
function sine(size) {
  var coeff = Math.PI / (size - 1);
  var sineBuffer = new Float32Array(size);

  for (var i = 0; i < size; i++) {
    sineBuffer[i] = Math.sin(coeff * i);
  }

  return sineBuffer;
}
function hanning(size) {
  var hanningBuffer = new Float32Array(size);

  for (var i = 0; i < size; i++) {
    // According to the R documentation
    // http://ugrad.stat.ubc.ca/R/library/e1071/html/hanning.window.html
    hanningBuffer[i] = 0.5 - 0.5 * Math.cos(2 * Math.PI * i / (size - 1));
  }

  return hanningBuffer;
}
function hamming(size) {
  var hammingBuffer = new Float32Array(size);

  for (var i = 0; i < size; i++) {
    //According to http://uk.mathworks.com/help/signal/ref/hamming.html
    hammingBuffer[i] = 0.54 - 0.46 * Math.cos(2 * Math.PI * (i / size - 1));
  }

  return hammingBuffer;
}

/***/ })

/******/ });
});

},{}],2:[function(require,module,exports){
var wordList = [
  // Borrowed from xkcd password generator which borrowed it from wherever
  "ability","able","aboard","about","above","accept","accident","according","ass","poop","boobies",
  "account","accurate","acres","across","act","action","active","activity",
  "actual","actually","add","addition","additional","adjective","adult","adventure",
  "advice","affect","afraid","after","afternoon","again","against","age",
  "ago","agree","ahead","aid","air","airplane","alike","alive",
  "all","allow","almost","alone","along","aloud","alphabet","already",
  "also","although","am","among","amount","ancient","angle","angry",
  "animal","announced","another","answer","ants","any","anybody","anyone",
  "anything","anyway","anywhere","apart","apartment","appearance","apple","applied",
  "appropriate","are","area","arm","army","around","arrange","arrangement",
  "arrive","arrow","art","article","as","aside","ask","asleep",
  "at","ate","atmosphere","atom","atomic","attached","attack","attempt",
  "attention","audience","author","automobile","available","average","avoid","aware",
  "away","baby","back","bad","badly","bag","balance","ball",
  "balloon","band","bank","bar","bare","bark","barn","base",
  "baseball","basic","basis","basket","bat","battle","be","bean",
  "bear","beat","beautiful","beauty","became","because","become","becoming",
  "bee","been","before","began","beginning","begun","behavior","behind",
  "being","believed","bell","belong","below","belt","bend","beneath",
  "bent","beside","best","bet","better","between","beyond","bicycle",
  "bigger","biggest","bill","birds","birth","birthday","bit","bite",
  "black","blank","blanket","blew","blind","block","blood","blow",
  "blue","board","boat","body","bone","book","border","born",
  "both","bottle","bottom","bound","bow","bowl","box","boy",
  "brain","branch","brass","brave","bread","bowl","breakfast","breath",
  "breathe","breathing","breeze","brick","bridge","brief","bright","bring",
  "broad","broke","broken","brother","brought","brown","brush","buffalo",
  "build","building","built","buried","burn","burst","bus","bush",
  "business","busy","but","butter","buy","by","cabin","cage",
  "cake","call","calm","came","camera","camp","can","canal",
  "cannot","cap","capital","captain","captured","car","carbon","card",
  "care","careful","carefully","carried","carry","case","cast","castle",
  "cat","catch","cattle","caught","cause","cave","cell","cent",
  "center","central","century","certain","certainly","chain","chair","chamber",
  "chance","change","changing","chapter","character","characteristic","charge","chart",
  "check","cheese","chemical","chest","chicken","chief","child","children",
  "choice","choose","chose","chosen","church","circle","circus","citizen",
  "city","class","classroom","claws","clay","clean","clear","clearly",
  "climate","climb","clock","close","closely","closer","cloth","clothes",
  "clothing","cloud","club","coach","coal","coast","coat","coffee",
  "cold","collect","college","colony","color","column","combination","combine",
  "come","comfortable","coming","command","common","community","company","compare",
  "compass","complete","completely","complex","composed","composition","compound","concerned",
  "condition","congress","connected","consider","consist","consonant","constantly","construction",
  "contain","continent","continued","contrast","control","conversation","cook","cookies",
  "cool","copper","copy","corn","corner","correct","correctly","cost",
  "cotton","could","count","country","couple","courage","course","court",
  "cover","cow","cowboy","crack","cream","create","creature","crew",
  "crop","cross","crowd","cry","cup","curious","current","curve",
  "customs","cut","cutting","daily","damage","dance","danger","dangerous",
  "dark","darkness","date","daughter","dawn","day","dead","deal",
  "dear","death","decide","declared","deep","deeply","deer","definition",
  "degree","depend","depth","describe","desert","design","desk","detail",
  "determine","develop","development","diagram","diameter","did","die","differ",
  "difference","different","difficult","difficulty","dig","dinner","direct","direction",
  "directly","dirt","dirty","disappear","discover","discovery","discuss","discussion",
  "disease","dish","distance","distant","divide","division","do","doctor",
  "does","dog","doing","doll","dollar","done","donkey","door",
  "dot","double","doubt","down","dozen","draw","drawn","dream",
  "dress","drew","dried","drink","drive","driven","driver","driving",
  "drop","dropped","drove","dry","duck","due","dug","dull",
  "during","dust","duty","each","eager","ear","earlier","early",
  "earn","earth","easier","easily","east","easy","eat","eaten",
  "edge","education","effect","effort","egg","eight","either","electric",
  "electricity","element","elephant","eleven","arsa","empty","end","enemy",
  "energy","engine","engineer","enjoy","enough","enter","entire","entirely",
  "environment","equal","equally","equator","equipment","escape","especially","essential",
  "establish","even","evening","event","eventually","ever","every","everybody",
  "everyone","everything","everywhere","evidence","exact","exactly","examine","example",
  "excellent","except","exchange","excited","excitement","exciting","exclaimed","exercise",
  "exist","expect","experience","experiment","explain","explanation","explore","express",
  "expression","extra","eye","face","facing","fact","factor","factory",
  "failed","fair","fairly","fall","fallen","familiar","family","famous",
  "far","farm","farmer","farther","fast","fastened","faster","fat",
  "father","favorite","fear","feathers","feature","fed","feed","feel",
  "feet","fell","fellow","felt","fence","few","fewer","field",
  "fierce","fifteen","fifth","fifty","fight","fighting","figure","fill",
  "film","final","finally","find","fine","finest","finger","finish",
  "fire","fireplace","firm","first","fish","five","fix","flag",
  "flame","flat","flew","flies","flight","floating","floor","flow",
  "flower","fly","fog","folks","follow","food","foot","football",
  "for","force","foreign","forest","forget","forgot","forgotten","form",
  "former","fort","forth","forty","forward","fought","found","four",
  "fourth","fox","frame","free","freedom","frequently","fresh","friend",
  "friendly","frighten","frog","from","front","frozen","fruit","fuel",
  "full","fully","fun","function","funny","fur","furniture","further",
  "future","gain","game","garage","garden","gas","gasoline","gate",
  "gather","gave","general","generally","gentle","gently","get","getting",
  "giant","gift","girl","give","given","giving","glad","glass",
  "globe","go","goes","gold","golden","gone","good","goose",
  "got","government","grabbed","grade","gradually","grain","grandfather","grandmother",
  "graph","grass","gravity","gray","great","greater","greatest","greatly",
  "green","grew","ground","group","grow","grown","growth","guard",
  "guess","guide","gulf","gun","habit","had","hair","half",
  "halfway","hall","hand","handle","handsome","hang","happen","happened",
  "happily","happy","harbor","hard","harder","hardly","has","hat",
  "have","having","hay","he","headed","heading","health","heard",
  "hearing","heart","heat","heavy","height","held","hello","help",
  "helpful","her","herd","here","herself","hidden","hide","high",
  "higher","highest","highway","hill","him","himself","his","history",
  "hit","hold","hole","hollow","home","honor","hope","horn",
  "horse","hospital","hot","hour","house","how","however","huge",
  "human","hundred","hung","hungry","hunt","hunter","hurried","hurry",
  "hurt","husband","ice","idea","identity","kalifa","ill","image",
  "imagine","immediately","importance","important","impossible","improve","in","inch",
  "include","including","income","increase","indeed","independent","indicate","individual",
  "industrial","industry","influence","information","inside","instance","instant","instead",
  "instrument","interest","interior","into","introduced","invented","involved","iron",
  "is","island","it","its","itself","jack","jar","jet",
  "job","join","joined","journey","joy","judge","jump","jungle",
  "just","keep","kept","key","kids","kill","kind","kitchen",
  "knew","knife","know","knowledge","known","label","labor","lack",
  "lady","laid","lake","lamp","land","language","large","larger",
  "largest","last","late","later","laugh","law","lay","layers",
  "lead","leader","leaf","learn","least","leather","leave","leaving",
  "led","left","leg","length","lesson","let","letter","level",
  "library","lie","life","lift","light","like","likely","limited",
  "line","lion","lips","liquid","list","listen","little","live",
  "living","load","local","locate","location","log","lonely","slang",
  "longer","look","loose","lose","loss","lost","lot","loud",
  "love","lovely","low","lower","luck","lucky","lunch","lungs",
  "lying","machine","machinery","mad","made","magic","magnet","mail",
  "manuela","mainly","major","make","making","man","managed","manner",
  "manufacturing","many","map","mark","market","married","mass","massage",
  "master","material","mathematics","matter","may","maybe","me","meal",
  "mean","means","meant","measure","meat","medicine","meet","melted",
  "member","memory","men","mental","merely","met","metal","method",
  "mice","middle","might","mighty","mile","military","milk","mill",
  "mind","mine","minerals","minute","mirror","missing","mission","mistake",
  "mix","mixture","model","modern","molecular","moment","money","monkey",
  "month","mood","moon","more","morning","most","mostly","mother",
  "motion","motor","mountain","mouse","mouth","move","movement","movie",
  "moving","mud","muscle","music","musical","must","my","myself",
  "mysterious","nails","name","nation","national","native","natural","naturally",
  "nature","near","nearby","nearer","nearest","nearly","necessary","neck",
  "needed","needle","needs","negative","neighbor","neighborhood","nervous","nest",
  "never","new","news","newspaper","next","nice","night","nine",
  "no","nobody","nodded","noise","none","noon","nor","north",
  "nose","not","note","noted","nothing","notice","noun","now",
  "number","numeral","nuts","object","observe","obtain","occasionally","occur",
  "ocean","of","off","offer","office","officer","official","oil",
  "old","older","oldest","on","once","one","only","onto","ole",
  "open","operation","opinion","opportunity","opposite","or","orange","orbit",
  "order","ordinary","organization","organized","origin","original","other","ought",
  "our","ourselves","out","outer","outline","outside","over","own",
  "owner","oxygen","pack","package","page","paid","pain","paint",
  "pair","palace","pale","pan","paper","paragraph","parallel","parent",
  "park","part","particles","particular","particularly","partly","parts","party",
  "pass","passage","past","path","pattern","pay","peace","pen",
  "pencil","people","per","percent","perfect","perfectly","perhaps","period",
  "person","personal","pet","phrase","physical","piano","pick","picture",
  "pictured","pie","piece","pig","pile","pilot","pine","pink",
  "pipe","pitch","place","plain","plan","plane","planet","planned",
  "planning","plant","plastic","plate","plates","play","pleasant","please",
  "pleasure","plenty","plural","plus","pocket","poem","poet","poetry",
  "point","pole","police","policeman","political","pond","pony","pool",
  "poor","popular","population","porch","port","position","positive","possible",
  "possibly","post","pot","potatoes","pound","pour","powder","power",
  "powerful","practical","practice","prepare","present","president","press","pressure",
  "pretty","prevent","previous","price","pride","primitive","principal","principle",
  "printed","private","prize","probably","problem","process","produce","product",
  "production","program","progress","promised","proper","properly","property","protection",
  "proud","prove","provide","public","pull","pupil","pure","purple",
  "purpose","push","put","putting","quarter","queen","question","quick",
  "quickly","quiet","quietly","quite","rabbit","race","radio","railroad",
  "rain","raise","ran","ranch","range","rapidly","rate","rather",
  "raw","rays","reach","read","reader","ready","real","realize",
  "rear","reason","recall","receive","recent","recently","recognize","record",
  "red","refer","refused","region","regular","related","relationship","religious",
  "remain","remarkable","remember","remove","repeat","replace","replied","report",
  "represent","require","research","respect","rest","result","revolution","review",
  "rhyme","rhythm","rice","rich","ride","riding","right","ring",
  "rise","rising","river","road","roar","rock","rocket","rocky",
  "rod","roll","roof","room","root","rope","rose","rough",
  "round","route","row","rubbed","rubber","rule","ruler","run",
  "running","rush","sad","saddle","safe","safety","said","sail",
  "sale","salmon","salt","same","sand","sang","sat","satellites",
  "satisfied","save","saved","saw","say","scale","scared","scene",
  "school","science","scientific","scientist","score","screen","sea","search",
  "season","seat","second","secret","section","see","seed","seeing",
  "seems","seen","seldom","select","selection","sell","send","sense",
  "sent","sentence","separate","series","serious","serve","service","sets",
  "setting","settle","settlers","seven","several","shade","shadow","shake",
  "shaking","shall","shallow","shape","share","sharp","she","sheep",
  "sheet","shelf","shells","shelter","shine","shinning","ship","shirt",
  "shoe","shoot","shop","shore","shorter","shot","should",
  "shoulder","shout","show","shown","shut","sick","sides","sight",
  "sign","signal","silence","silent","silk","silly","silver","similar",
  "simple","simplest","simply","since","sing","single","sink","sister",
  "sit","sitting","situation","six","size","skill","skin","sky",
  "slabs","slave","sleep","slept","slide","slight","slightly","slip",
  "slipped","slope","slow","slowly","small","smaller","smallest","smell",
  "smile","smoke","smooth","snake","snow","so","soap","social",
  "society","soft","softly","soil","solar","sold","soldier","solid",
  "solution","solve","some","somebody","somehow","someone","something","sometime",
  "somewhere","son","song","soon","sort","sound","source","south",
  "southern","space","speak","special","species","specific","speech","speed",
  "spell","spend","spent","spider","spin","spirit","spite","split",
  "spoken","sport","spread","spring","square","stage","stairs","stand",
  "standard","star","stared","start","state","statement","station","stay",
  "steady","steam","steel","steep","stems","step","stepped","stick",
  "stiff","still","stock","stomach","stone","stood","stop","stopped",
  "store","storm","story","stove","straight","strange","stranger","straw",
  "stream","street","strength","stretch","strike","string","strip","strong",
  "stronger","struck","structure","struggle","stuck","student","studied","studying",
  "subject","substance","success","successful","such","sudden","suddenly","sugar",
  "suggest","suit","sum","summer","sun","sunlight","supper","supply",
  "support","suppose","sure","surface","surprise","surrounded","swam","sweet",
  "swept","swim","swimming","swing","swung","syllable","symbol","system",
  "table","tail","take","taken","tales","talk","tall","tank",
  "tape","task","taste","taught","tax","tea","teach","teacher",
  "team","tears","teeth","telephone","television","tell","temperature","ten",
  "tent","term","terrible","test","than","thank","that","thee",
  "them","themselves","then","theory","there","therefore","these","they",
  "thick","thin","thing","think","third","thirty","this","those",
  "thou","though","thought","thousand","thread","three","threw","throat",
  "through","throughout","throw","thrown","thumb","thus","thy","tide",
  "tie","tight","tightly","till","time","tin","tiny","tip",
  "tired","title","to","tobacco","today","together","told","tomorrow",
  "tone","tongue","tonight","too","took","tool","top","topic",
  "torn","total","touch","toward","tower","town","toy","trace",
  "track","trade","traffic","trail","train","transportation","trap","travel",
  "treated","tree","triangle","tribe","trick","tried","trip","troops",
  "tropical","trouble","truck","trunk","truth","try","tube","tune",
  "turn","twelve","twenty","twice","two","type","typical","uncle",
  "under","underline","understanding","unhappy","union","unit","universe","unknown",
  "unless","until","unusual","up","upon","upper","upward","us",
  "use","useful","using","usual","usually","valley","valuable","value",
  "vapor","variety","various","vast","vegetable","verb","vertical","very",
  "vessels","victory","view","village","visit","visitor","voice","volume",
  "vote","vowel","voyage","wagon","wait","walk","wall","want",
  "war","warm","warn","was","wash","waste","watch","water",
  "wave","way","we","weak","wealth","wear","weather","week",
  "weigh","weight","welcome","well","went","were","west","western",
  "wet","whale","what","whatever","wheat","wheel","when","whenever",
  "where","wherever","whether","which","while","whispered","whistle","white",
  "who","whole","whom","whose","why","wide","widely","wife",
  "wild","will","willing","win","wind","window","wing","winter",
  "wire","wise","wish","with","within","without","wolf","women",
  "won","wonder","wonderful","wood","wooden","wool","word","wore",
  "work","worker","world","worried","worry","worse","worth","would",
  "wrapped","write","writer","writing","written","wrong","wrote","yard",
  "year","yellow","yes","yesterday","yet","you","young","younger",
  "your","yourself","youth","zero","zebra","zipper","zoo","zulu",
  "ganja","vibration","acid","astral","minion", "porridge"
];

function words(options) {

  function word() {
    if (options && options.maxLength > 1) {
      return generateWordWithMaxLength();
    } else {
      return generateRandomWord();
    }
  }

  function generateWordWithMaxLength() {
    var rightSize = false;
    var wordUsed;
    while (!rightSize) {  
      wordUsed = generateRandomWord();
      if(wordUsed.length <= options.maxLength) {
        rightSize = true;
      }

    }
    return wordUsed;
  }

  function generateRandomWord() {
    return wordList[randInt(wordList.length)];
  }

  function randInt(lessThan) {
    return Math.floor(Math.random() * lessThan);
  }

  // No arguments = generate one word
  if (typeof(options) === 'undefined') {
    return word();
  }

  // Just a number = return that many words
  if (typeof(options) === 'number') {
    options = { exactly: options };
  }

  // options supported: exactly, min, max, join
  if (options.exactly) {
    options.min = options.exactly;
    options.max = options.exactly;
  }
  
  // not a number = one word par string
  if (typeof(options.wordsPerString) !== 'number') {
    options.wordsPerString = 1;
  }

  //not a function = returns the raw word
  if (typeof(options.formatter) !== 'function') {
    options.formatter = (word) => word;
  }

  //not a string = separator is a space
  if (typeof(options.separator) !== 'string') {
    options.separator = ' ';
  }

  var total = options.min + randInt(options.max + 1 - options.min);
  var results = [];
  var token = '';
  var relativeIndex = 0;

  for (var i = 0; (i < total * options.wordsPerString); i++) {
    if (relativeIndex === options.wordsPerString - 1) {
      token += options.formatter(word(), relativeIndex);
    }
    else {
      token += options.formatter(word(), relativeIndex) + options.separator;
    }
    relativeIndex++;
    if ((i + 1) % options.wordsPerString === 0) {
      results.push(token);
      token = ''; 
      relativeIndex = 0;
    }
   
  }
  if (typeof options.join === 'string') {
    results = results.join(options.join);
  }

  return results;
}

module.exports = words;
// Export the word list as it is often useful
words.wordList = wordList;

},{}],3:[function(require,module,exports){
//require('electron-chromecast');
//const fs = require('fs');
const randomWords = require('random-words-mio');
//const {dialog} = require('electron').remote;
var Meyda = require('meyda');

var cambiosCodigoGuardados=true;
var cambiosProyectoGuardados=true;
var pathProyectoActual='';
var runtime_code="";
var runtime_code_setup="";
var runtime_preload="";
var ejecutando_sketch=false;
var livecoding_activo=false;
var window_sketch;
var window_preferences;
var window_audio;
var sketchFile ;
var nombresSonidos = [];
var timelinePlaying = false;
var timeWhenPlayClicked = 0;
var nextIdItemTimeline=0;
var nextVariableName="var1";
var previewMode=true;
var audio_file = "";
var curves = [];
var curveImages = [];
var midiMappings = [];
var clipboardTimeline = [];
var baseBlock = null;
var variables_a_inyectar = [];
var configurandoItem = null; // id del item del timeline configurandose ahora
variables_a_inyectar['tune'] = {};
variables_a_inyectar['auto'] = {};
variables_a_inyectar['features'] = {};
variables_a_inyectar['features'] = {};
variables_a_inyectar['textures'] = [];
variables_a_inyectar['texturesStatic'] = [];
var cambiosAInyectarTune = false; // TODO Estas se usan pa algo? diria que no
var cambiosAInyectarAuto = false;
var bloquesPersonalizados = [];
var workspacePlayground;
var blocklyAltura ;
var blocklyAnchura ;
var iframePreview;
var fps=0;
var errorLine = "";
var codigo_actual, codigo_preview;
var quality = 1;
var BPM = 60;
var timeTimeline = 0;
var timeTimelineSeek = 0;
window.opener=window;
var now_playing_audio = false;

const audioContext = new AudioContext();
var audioAnalyzer;
var audioSource;
var audioFeatures = [];
var liveAudioFeatures ={};
var videoElement ; // borrar cuando se maneje bien
var imgblackpixel ;
const isRenderer=false;

const ACTUAL_VERSION = 0.2;

var preferences = {
    OUTPUT_FRAMED: 0,
    GLSL_VERSION: 120,
    ACTIVE_AUDIO_FEATURES: [],
    AUDIO_BUFFER_SIZE: 512,
    AUDIO_WINDOWING_FUNCTION: "hanning",
	BACKBUFFER: false,
};

var isf_header1 = `
/*{
  "CREDIT": "Josep il birras",
  "DESCRIPTION": "",
  "CATEGORIES": [
    "Generator"
  ],
  "INPUTS": [ 
`;
var isf_header_backbuffer = `
],
  "PERSISTENT_BUFFERS" : [
    "backbuffer"
  ],
  "PASSES" : [
    {
      "TARGET" : "backbuffer"
    }
`
var isf_header2 = `
  
  ]
}*/

uniform float time;
uniform vec2 resolution;
//uniform sampler2D videotaso;
const float PI= 3.1415;

bool greaterThan(float a, float b){ return a>b;}
bool greaterThanEqual(float a, float b){ return a>=b;}
bool lessThan(float a, float b){ return a<b;}
bool lessThanEqual(float a, float b){  return a<=b;}
bool equal(float a, float b){  return a==b;}
bool notEqual(float a, float b){  return a!=b;}

`;

var wavesurfer;

if (!Object.prototype.hasOwnProperty.call(RegExp, 'escape')) {
	RegExp.escape = function(string) {
		// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#Escaping
		// https://github.com/benjamingr/RegExp.escape/issues/37
		return string.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
	};
}

if (!Object.prototype.hasOwnProperty.call(String, 'replaceAll')) {
	String.prototype.replaceAll = function(find, replace) {
		// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replaceAll
		// If you pass a RegExp to 'find', you _MUST_ include 'g' as a flag.
		// TypeError: "replaceAll must be called with a global RegExp" not included, will silently cause significant errors. _MUST_ include 'g' as a flag for RegExp.
		// String parameters to 'find' do not require special handling.
		// Does not conform to "special replacement patterns" when "Specifying a string as a parameter" for replace
		// Does not conform to "Specifying a function as a parameter" for replace
		return this.replace(
			Object.prototype.toString.call(find) == '[object RegExp]' ?
				find :
				new RegExp(RegExp.escape(find), 'g'),
			replace
		);
	}
}


function setearProyectoComoGuardado(guardado) {
	console.log("CAMBIOSENELPROYECTO"+guardado)
	if(cambiosProyectoGuardados && !guardado){
		cambiosProyectoGuardados=false;
		document.title = '* ' + document.title;
	}else if(guardado){
		cambiosProyectoGuardados=true;
		if(pathProyectoActual==''){
			document.title = 'New project' + ' - AstralTrip';
		}else {
			document.title = pathProyectoActual + ' - AstralTrip';
		}
	}
}

function evaluar_codigo_bloques_db(){
	stash.get("blocks").forEach(function(element) {
		eval(element.codigo);
	});
}

function reset_workspace(){
	
}



var absolutePositionOfElement = function(element) {
	var top = 0, left = 0;
	do {
		top += element.offsetTop  || 0;
		left += element.offsetLeft || 0;
		element = element.offsetParent;
	} while(element);

	return {
		top: top,
		left: left
	};
};


function getElementFromHTML(html){
	var aux=document.createElement('div');
	aux.innerHTML = html;
	return aux.firstChild;
}

function load_sound(){
	console.log("load sound");
	pathSonido = dialog.showOpenDialog({properties:['openFile']});
	nombre=pathSonido[0].replace(/^.*[\\\/]/, '').replace(/\.[^/.]+$/, "");
	nombresSonidos.push(nombre);
	runtime_code_setup+="sound_"+nombre+"=loadSound('"+pathSonido[0]+"');\n";
	nuevo_bloque=Blockly.mainWorkspace.newBlock('at_sound_variable');
	Blockly.mainWorkspace.addTopBlock(nuevo_bloque);

}


function guardar_codigo(){
 // if(document.getElementById("codigo_guardado").innerHTML)
	document.getElementById("codigo_guardado").innerHTML= document.getElementById("codigaso").value;
	document.getElementById("codigaso").innerText=document.getElementById("codigo_guardado").innerText;
	cambiosCodigoGuardados=false;
	blocklyFactory.blockLibraryController.updateButtons(cambiosCodigoGuardados);
} 

function inicializarCodigoNuevoBloque(){
 document.getElementById("codigo_guardado").innerHTML="// TODO: Assemble JavaScript into code variable.\nvar code = '...;\\n';\n";
 document.getElementById("codigaso").innerText="// TODO: Assemble JavaScript into code variable.\nvar code = '...;\\n';\n";
 document.getElementById("codigaso").value="// TODO: Assemble JavaScript into code variable.\nvar code = '...;\\n';\n";
}

function guardar_bloque(tipo, codigo, json_definicion){
	stash.set('blocks',stash.get('blocks')
		.push({ id: tipo, codigo: codigo, json: json_definicion}));
	cambiosCodigoGuardados=true;
}

function get_json_bloques(){
	return stash.get("blocks");
}

function get_json_proyecto(){
	var json_a_devolver
	let variables  = {};
	variables['gl_FragCoord'] = 'gl_FragColor';
	variables['gl_FragCoord'] = 'gl_FragCoord';
	variables['resolution'] = 'resolution';
	variables['RENDERSIZE'] = 'RENDERSIZE';
	variables['time'] = 'time';
	variables['isf_FragNormCoord'] = 'isf_FragNormCoord';

	Blockly.Variables.allUsedVarModels(Blockly.mainWorkspace).forEach((a)=>{variables[a.name]=a.name;});

	json_a_devolver = { 
		"workspace": Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(Blockly.mainWorkspace)),
		"blocks": get_json_bloques(),
		"variablesWorkspace": JSON.stringify(variables),
		//"variablesdesk": stash.get("variablesdesk"),
		//"variablesauto": stash.get("variablesauto"),
		"loadtimelineItems": itemsTimeline.get(),
		"loadtimelineGroups": groupsTimeline.get(),
		"loadtunedesk": tunedesk.getConf(),
		"astraltripVersion": ACTUAL_VERSION
	};

	return JSON.stringify(json_a_devolver);
}

function load_json_proyecto(data) {
	variables_a_inyectar['textures'] = [];
	console.log("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD",data)
	if(data['loadtimelineGroups']) {
		data['loadtimelineGroups'].forEach((item) => {
			groupsTimeline.add(item);
			console.log("ITEEM",item)
			if(item.type=="texture") variables_a_inyectar['textures'][item.id] = imgblackpixel;
			else variables_a_inyectar['auto'][item.id]=1;
		});
	}
	if(data['loadtimelineItems']) {
		data['loadtimelineItems'].forEach((item) => {
			itemsTimeline.add(item);
			if(item.curve)curves[item.id] = eval('new Bezier'+item.curve);
			else if(item.func == "audio") htmlAudioElement.src=item.audio_file;
			//else if(item.func == "texture") variables_a_inyectar['textures'][item.group] = document.getElementById('videoElement'+item.id);
		});
	}
	timeline.redraw();
	console.log("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD",variables_a_inyectar)

	if(data['loadtunedesk']) {
		tunedesk.loadConf(data['loadtunedesk']);
	}
	Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom(data['workspace']), Blockly.mainWorkspace);
	if(data['variablesWorkspace']) {
		const vars= JSON.parse(data['variablesWorkspace']);
		Object.keys(vars).forEach( (e,k) =>{
			try{workspacePlayground.createVariable(e);}
			catch (err) {
				console.log("ERR"+err)
			}
		});
	}
	// TODO manejar el guardado de blokes: que solo se guarden los personalizados que se usan en este proyecto
}

function inicializarProyecto() {
	Blockly.mainWorkspace.clear();
	Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom('<xml xmlns=\"http://www.w3.org/1999/xhtml\"><variables><variable type=\"\" id=\"p)j.x`B)(+U@CD^EjZji\">gl_FragColor</variable></variables><block type=\"glmain\" id=\"?~1PcXOKT=Wv9zIaIWC^\" x=\"287\" y=\"153\"><statement name=\"main\"><block type=\"variables_set\" id=\"wLm/T=W$H5w*k4iancF?\"><field name=\"VAR\" id=\"p)j.x`B)(+U@CD^EjZji\" variabletype=\"\">gl_FragColor</field><value name=\"value\"><block type=\"vec4\" id=\"tcLm!:g|vQ(EKI0KxA4=\"><value name="a"><shadow type="glfloat"><field name="num">1.0</field></shadow></value><value name="r"><shadow type="glfloat"><field name="num">1.0</field></shadow></value><value name="g"><shadow type="glfloat"><field name="num">1.0</field></shadow></value><value name="b"><shadow type="glfloat"><field name="num">1.0</field></shadow></value></block></value></block></statement></block></xml>'), workspacePlayground);

	//workspacePlayground.createVariable("gl_FragColor");
	workspacePlayground.createVariable("gl_FragCoord");
	//workspacePlayground.createVariable("mouse");
	workspacePlayground.createVariable("resolution");
	workspacePlayground.createVariable("RENDERSIZE");
	workspacePlayground.createVariable("time");
	workspacePlayground.createVariable("TIME");
	workspacePlayground.createVariable("isf_FragNormCoord");

	//tunedesk.addModule({name: "slider01", type: "slider01", value:0.2});
	//tunedesk.addModule({name: "slider", type: "slider", value:0.2, min:-1, max:1});
}

function new_project(inicializar=false){
	console.log("new_project");


	if(ejecutando_sketch && window_sketch){
		window_sketch.close();
	}

	document.title="New project - AstralTrip";
	cambiosCodigoGuardados=true;
	cambiosProyectoGuardados=true;
	runtime_code="";
	runtime_code_setup="";
	runtime_preload="";
	ejecutando_sketch=false;
	pathProyectoActual='';
	document.getElementById("logErrores").innerHTML="";
	stash.set("loadtimelineGroups",'');
	stash.set("loadtimelineItems",'');
	stash.set("pathProyectoActual",'');
	stash.set("codigo_actual",'');
	itemsTimeline.clear();
	groupsTimeline.clear();
	nextIdItemTimeline=0;
	curves = [];
	midiMappings = [];
	tunedesk.clear();

	variables_a_inyectar = [];
	variables_a_inyectar['tune'] = {};
	variables_a_inyectar['auto'] = {};
	variables_a_inyectar['features'] = {};
	variables_a_inyectar['textures'] = [];
	variables_a_inyectar['texturesStatic'] = [];

	cambiosAInyectarTune = false;
	cambiosAInyectarAuto = false;

	Blockly.mainWorkspace.clear();
    htmlAudioElement.src="";
	stopAudioAnalyzer();

	if(timelinePlaying)timelinePlay();
	timelineGoToStart();

    if(inicializar==true){
		inicializarProyecto();
	}
    if(previewMode) {
		previewMode=false;
		tooglePreviewMode();
	}
}

function openExporter() {
	if(timelinePlaying) timelinePlay();
	if(window_sketch) window_sketch.close();
	window_sketch= window.open("exporter.html","exporter","name=Exporter,menubar=no,titlebar=no");
}

function openPreferences() {
	const window_preferences= window.open("frame_preferences.html","preferences","name=Preferences,menubar=no,titlebar=no");
    window_preferences.opener = window;
    const timer = setInterval(() =>{
        if(window_preferences.closed) {
            clearInterval(timer);
            console.log(window.preferences,"FFF")
            stash.set('preferences', window.preferences);
        }
    },500);
}


function openAudioAnalyzer() {
	window_audio= window.open("audio_analyzer.html","Audio Analyzer","name=Audio analyzer,menubar=no,titlebar=no");
	window_audio.nextItemId = makeItemId();
}


function openGLSLExporter() {
	codigo_actual=getCodigoActual();
	window.open("export_to_glsl.html","exportGLSL","name=exportGLSL,menubar=no,titlebar=yes,toolbar=no, scrollbars=no, status=no, directories=no");
}

function save_file_project(path) {
	/*fs.writeFile(path, get_json_proyecto(), function (err) {
		if (err) {
			console.log('Failed to save the file at '+path + "       " + err);
			return ;
		}
		setearProyectoComoGuardado(true);
		console.log("guardado OK ."+path);
	});*/
}

function save_project(saveAs=false){
	console.log("save_project . Save as:"+saveAs);
	if(pathProyectoActual==='' || saveAs===true){
		dialog.showSaveDialog({properties:['showOverwriteConfirmation']}).then( (response) => {
			console.log(response.filePath);
			if(response.canceled) return;
			if(response.filePath.split('.').pop().toLowerCase() != "trip"){
				response.filePath +=".trip"
			}
			pathProyectoActual = response.filePath;
			stash.set('pathProyectoActual',pathProyectoActual);
			save_file_project(pathProyectoActual);
		});
	}else{
		save_file_project(pathProyectoActual);
	}
}

function open_project(){
	console.log("open_project");
	const res = dialog.showOpenDialog({properties:['openFile']}).then( (response) => {
		if (!response.canceled) {
			try {
				new_project();
				pathProyectoActual = response.filePaths[0];
				stash.set('pathProyectoActual',pathProyectoActual);
				//var data = JSON.parse(fs.readFileSync(pathProyectoActual, 'utf-8'));
				console.log(data);
				load_json_proyecto(data);
				setearProyectoComoGuardado(true);
			} catch (err) {
				console.log('Failed to load the file at ' + response.filePaths[0] + err);
			}
		}else{
			console.log('Apertura cancelada');
		}
	});
}

function importMediaFile(){
	console.log("importMediaFile");

	res = dialog.showOpenDialog({properties:['openFile']});

	if(res.constructor === Array){

	}else{
		console.log('Apertura cancelada');
	}
}

function restoreSession(){
	console.log("restoreSession");

	pathProyectoActual = stash.get('pathProyectoActual');
	bloquesPersonalizados = stash.get('bloquesPersonalizados');
	bloquesPersonalizados.forEach( (bloque, key) => {
		bloquesPersonalizados[bloque.id] = bloque;
		delete bloquesPersonalizados[key];
	})
	//bloquesPersonalizados = db.get('blocks').value();

	var data = [];
	const jsonActual =JSON.parse(stash.get('jsonActual'));
	data['workspace'] = jsonActual['workspace'];
	console.log("restoreSession",data['workspace']);
	//data['workspace'] = stash.get('workspace');
	//console.log("restoreSession",data['workspace']);

	data['loadtimelineItems'] = jsonActual['loadtimelineItems'];
	data['loadtimelineGroups'] = jsonActual['loadtimelineGroups'];
	data['loadtunedesk'] = jsonActual['loadtunedesk'];
	data['variablesdesk'] = jsonActual['variablesdesk'];
	data['variablesauto'] = jsonActual['variablesauto'];
	//data['variablesTexture'] = stash.get('variablestexture');
	load_json_proyecto(data);
	actualizarCodigoPreview();

	setearProyectoComoGuardado(true);
}


// TODO ELiminar si de verdad no valen pa na
function stashWorkspace() {
    //stash.set("workspace",(String( Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(Blockly.mainWorkspace))).replace(/(\r\n\t|\n|\r\t)/gm," ")));
}

function saveWorkspace(){
	//stash.set('workspace',Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(Blockly.mainWorkspace)));
}

function getCodigoActual(){
	let result = "";
	result += "#version "+ preferences.GLSL_VERSION + "\n";
	result += isf_header1;
	if(variables_a_inyectar['textures']) Object.entries(variables_a_inyectar['textures']).forEach(([key, val]) => {
		console.log('TEXTUREEE', key,val)
		result += '{ "NAME" : "'+key+'","TYPE" : "image" },';
	});
    //if(variables_a_inyectar['texturesStatic']) Object.entries(variables_a_inyectar['texturesStatic']).forEach(([key, val]) => {
	//	console.log('TEXTUREEeeessssE', key,val)
	//	result += '{ "NAME" : "'+key+'","TYPE" : "image" },';
	//});
	result = result.slice(0,-1);        // kita la ultima coma (o el espacio final de isf_header_1 si no se añadio ninguna textura)
	preferences.BACKBUFFER? result+=isf_header_backbuffer : null;
	result += isf_header2;
	Object.entries(variables_a_inyectar['tune']).forEach(([key, val]) => {
		result += "uniform float " + key + ";\n";
	});
	Object.entries(variables_a_inyectar['auto']).forEach(([key, val]) => {
		result += "uniform float " + key + ";\n";
	});
	//Object.entries(variables_a_inyectar['features']).forEach(([key, val]) => {
	preferences.ACTIVE_AUDIO_FEATURES.forEach((val) => {
        if(val=="chroma"){      // Todo cambiar esto por arrays de isf, que permiten inyectar un array entero al gl
            for(x=0; x<=11; x++){
                result += "uniform float audio_chroma_" + x + ";\n";
            }
        }
        if(val=="mfcc"){
            for(x=0; x<=12; x++){
                result += "uniform float audio_mfcc_" + x + ";\n";
            }
        }else {
            result += "uniform float audio_" + val + ";\n";
        }

	});
	result += String(Blockly.JavaScript.workspaceToCode(workspacePlayground));//.replace('},99'," ");
	console.log(result);
	return result;
}

function run_project() {
	console.log("running");
	//const codigo_actual=(String(Blockly.JavaScript.workspaceToCode(workspacePlayground)).replace(/(\r\n\t|\n|\r\t)/gm," "));
	codigo_actual = getCodigoActual();
	//stashWorkspace();
	if (window_sketch && ! window_sketch.closed) {
		window_sketch.reloadSource(codigo_actual)
		console.log(window_sketch);
	} else {
		window_sketch = window.open("frame_sketch_isf.html", "output", "name=Output,menubar=yes,titlebar=yes,toolbar=no, scrollbars=no, status=no, directories=no, frame="+preferences.OUTPUT_FRAMED);
		window_sketch.opener = window;
		$(window_sketch).ready(() =>{
			console.log(window_sketch);
			setTimeout( () => {
				window_sketch.variables_a_inyectar = variables_a_inyectar;
				window_sketch.liveAudioFeatures = liveAudioFeatures;
				window_sketch.reloadSource(codigo_actual);
			},100);
		});
		//electron.ipcRenderer.send('open_renderer', codigo_actual)
		//window_sketch.style.webkitAppRegion = "drag"
		var timer = setInterval(function () {
			if (window_sketch.closed) {
				clearInterval(timer);
				ejecutando_sketch = false;
			}
		}, 100);
		ejecutando_sketch = true;
	}
}

function des_activar_livecoding(){
	if(livecoding_activo){
		Blockly.mainWorkspace.removeChangeListener(run_project);
		document.getElementById("runButton").src="icons/refresh-line.png";
		livecoding_activo=false;
	}else{
		run_project();
		Blockly.mainWorkspace.addChangeListener(run_project);
		document.getElementById("runButton").src="icons/refresh-line-green.png";
		livecoding_activo=true;
	}
}

function guardar_bloque_pro(tipo, codigo, json_definicion, json_definicion_molde, json_definicion_bloques_generadores){
	//var bloque = db.get('blocks').find({ id: tipo });
	//if(bloque){
	//	db.get('blocks').remove({ id: tipo }).write();
	//}
	//db.get('blocks')
	//	.push({ id: tipo, codigo: codigo, json: json_definicion, json_molde: json_definicion_molde , json_bloques: json_definicion_bloques_generadores })
	//	.write();
	bloquesPersonalizados.push({ id: tipo, codigo: codigo, json: json_definicion, json_molde: json_definicion_molde , json_bloques: json_definicion_bloques_generadores });
	stash.set('bloquesPersonalizados',bloquesPersonalizados);
	cambiosCodigoGuardados=true;
	eval(codigo);
}
function onclickgenerateBlockLibraryButton(){
	var block = FactoryUtils.getRootBlock(BlockFactory.mainWorkspace);
	var block_preview = BlockFactory.previewWorkspace.getAllBlocks()[0];
	var block_code = String(Blockly.JavaScript.workspaceToCode(BlockFactory.codeWorkspace));
	if(block_preview.nextConnection == null && block_code.substr(-2) == "; "){
		block_code = block_code.slice(0, -2);		// El slice elimina el ; final si el bloque no es de los que tienen conexion por abajo
	}
	document.getElementById('codigaso').innerText="";
	console.log(document.getElementById('generatorPre').innerHTML);
	var cleanCode = String(document.getElementById('generatorPre').innerHTML)
		.replace('<textarea id=\"codigaso\" oninput=\"guardar_codigo()\" style=\"width: 95%;height: 3em;\">', '//inicio_codigo_usuario\n')
		.replace('</textarea>',"")
		.replace('// TODO: Change ORDER_NONE to the correct strength.',"")
		.replace('//inicio_codigo_usuario\n','var code=\''+block_code+'\';')
		.replaceAll('\n',' ');
	console.log(cleanCode,block_code);
	var nombreBloque= getTipoBloqueActual();
	var bloquesGeneradores=BlockFactory.codeWorkspace.getAllBlocks()[0];
	//console.log(String(Blockly.Xml.blockToDom(block_preview).outerHTML));
	guardar_bloque_pro(nombreBloque,
					   cleanCode,
					   document.getElementById('languagePre').innerHTML,
					   String(Blockly.Xml.blockToDomWithXY(block).outerHTML),
						String(Blockly.Xml.blockToDomWithXY(bloquesGeneradores).outerHTML));
}

function restaurar_codigo_bloque(tipoBloque){
	var bloque = bloquesPersonalizados[tipoBloque];
	var cleanCode = bloque['codigo'];
	// Deshace las sustituciones que hizo en la funcion onclickgenerateBlockLibraryButton
	var dirtyCode = cleanCode.replace('//inicio_codigo_usuario\n', '<textarea id=\"codigaso\" oninput=\"guardar_codigo()\" style=\"width: 95%;height: 3em;\">').replace("\n//fin_codigo_usuario",'</textarea>');
	//console.log(dirtyCode);
	var veryCleanCode = cleanCode.split('//inicio_codigo_usuario\n')[1].split('\n//fin_codigo_usuario')[0];
	//expresion = /(capítulo \d+(\.\d)*)/i;
	set_codigo_guardado(veryCleanCode);
	document.getElementById('generatorPre').innerHTML = dirtyCode;
	cambiosCodigoGuardados=true;
}


function set_codigo_guardado(nuevo_codigo){
	document.getElementById("codigo_guardado").innerHTML= nuevo_codigo;
} 

function getTipoBloqueActual(){
	return String(document.getElementById('saveToBlockLibraryButton').innerHTML).split('"')[1];
}


function appendXMLtoolboxes(){
	var xmlhttp, xmlhttp2, xmlhttp3, xmlhttp4;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
		xmlhttp2 = new XMLHttpRequest();
		xmlhttp3 = new XMLHttpRequest();
		xmlhttp4 = new XMLHttpRequest();
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.body.appendChild(getElementFromHTML(xmlhttp.responseText));
		}
	}

	xmlhttp2.onreadystatechange = function() {
		if (xmlhttp2.readyState == 4 && xmlhttp2.status == 200) {
			document.body.appendChild(getElementFromHTML(xmlhttp2.responseText));
		}
	}

	xmlhttp3.onreadystatechange = function() {
		if (xmlhttp3.readyState == 4 && xmlhttp3.status == 200) {
			document.body.appendChild(getElementFromHTML(xmlhttp3.responseText));
			Blockly.mainWorkspace.updateToolbox(xmlhttp3.responseText);
		}
	}

	xmlhttp4.onreadystatechange = function() {
		if (xmlhttp4.readyState == 4 && xmlhttp4.status == 200) {
			document.body.appendChild(getElementFromHTML(xmlhttp4.responseText));
		}
	}

	xmlhttp.open("GET", "toolboxes/toolbox_workspacefactory.xml", true);
	xmlhttp.send();

	xmlhttp2.open("GET", "toolboxes/toolbox_blockfactory.xml", true);
	xmlhttp2.send();

	xmlhttp3.open("GET", "toolboxes/toolbox_main.xml", true);
	xmlhttp3.send();

	xmlhttp4.open("GET", "toolboxes/toolbox_base_blocks.xml", true);
	xmlhttp4.send();

}

function actualizarCodigoPreview(){
	codigo_preview = previewMode ? getCodigoActual() : 'void main( void ) {}' ;

	reloadSource(codigo_preview);
}

function tooglePreviewMode(){
	previewMode=!previewMode;
	if(previewMode){
		document.getElementById('isf-canvas').style.display = 'block';
		Blockly.mainWorkspace.addChangeListener(actualizarCodigoPreview);
		actualizarCodigoPreview();
		animatex();
	}else{
		document.getElementById('isf-canvas').style.display = 'none';
		Blockly.mainWorkspace.removeChangeListener(actualizarCodigoPreview);
	}

}

// esto se llama desde blocky_compressed linea 1355
function showblockFactoryModal(blockCodigo, blockDefinicion) {
	document.getElementById('blockFactoryModal').style.display="block";
	baseBlock=blockCodigo;
	BlockFactory.codeWorkspace.clear();
	//BlockFactory.codeWorkspace.addTopBlock(block).initSvg();

	// Sorry xD
	if(blockDefinicion) {
		BlockFactory.mainWorkspace.clear();
		Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom('<xml xmlns="http://www.w3.org/1999/xhtml">'+blockCodigo+'</xml>'), BlockFactory.codeWorkspace);
		Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom('<xml xmlns="http://www.w3.org/1999/xhtml">'+blockDefinicion+'</xml>'), BlockFactory.mainWorkspace);
	}else{
		Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom('<xml xmlns="http://www.w3.org/1999/xhtml">'+Blockly.Xml.domToText(Blockly.Xml.blockToDom(blockCodigo))+'</xml>'), BlockFactory.codeWorkspace);
	}
    FactoryUtils.show('blockFactoryContent');
	window.dispatchEvent(new Event('resize'));
}

function editBlockFromLibrary(block) {
	const bloque = bloquesPersonalizados[block.type];
	if(bloque){
		showblockFactoryModal(bloque.json_bloques, bloque.json_molde);
	}
}

//todo borrar una de las dos
function deleteBlockFromLibrary(block) {
	bloquesPersonalizados.splice(block, 1);
}
//todo borrar una de las dos
function borrarBloqueDB(tipoBloque){
	bloquesPersonalizados.splice(tipoBloque, 1);
}

function borrarBloqueActualDB(){
	borrarBloqueDB(getTipoBloqueActual());
}

function hideblockFactoryModal() {
	document.getElementById('blockFactoryModal').style.display="none";
}

function unfoldBlock(block) {
	const bloque = bloquesPersonalizados[block];
	if(bloque){
		var nuevos_bloques = Blockly.Xml.textToDom('<xml xmlns="http://www.w3.org/1999/xhtml">'+bloque.json_bloques+'</xml>');
		//block.parentBlock

block.dispose(!0,!0);
		const nuevos = Blockly.Xml.appendDomToWorkspace(nuevos_bloques,Blockly.mainWorkspace);
		//Blockly.mainWorkspace.remo
//TODO mantener las conexiones si las hubiese
		// TODO mantener los bloques que hubiera conectados, cuando funcione lo de parametrizar
		//block.parentBlock.nextConnection.connect(childBlock.previousConnection);
		//showblockFactoryModal(bloque.json_bloques, bloque.json_molde);
	}
}


function resizeInterfaz() {
	setTimeout( () => {
		var element = blocklyArea;
		var x = 0;
		var y = 0;
		do {
			x += element.offsetLeft;
			y += element.offsetTop;
			element = element.offsetParent;
		} while (element);
		// Position blocklyDiv over blocklyArea.
		blocklyDiv.style.left = x + 'px';
		blocklyDiv.style.top = (y - 58) + 'px';

		blocklyAnchura = window.innerWidth - 300;

		timeline.setOptions({height: (document.documentElement.clientHeight - blocklyAltura - 30) + "px"});


		blocklyDiv.style.width = blocklyAnchura + "px";
		blocklyDiv.style.height = blocklyAltura + "px";

		blocklyArea.style.width = blocklyAnchura + "px";
		blocklyArea.width = blocklyAnchura + "px";
		blocklyArea.style.height = blocklyAltura + "px";
		blocklyArea.height = blocklyAltura + "px";

		const varblocklysvg = document.getElementsByClassName('blocklySvg')[0];
		varblocklysvg.style.width = blocklyAnchura + "px";
		varblocklysvg.width = blocklyAnchura + "px";
		varblocklysvg.style.height = blocklyAltura + "px";
		varblocklysvg.height = blocklyAltura + "px";

		const varblocklydiv = document.getElementById('blocklyDiv');
		varblocklydiv.style.width = blocklyAnchura + "px";
		varblocklydiv.width = blocklyAnchura + "px";
		varblocklydiv.style.height = blocklyAltura + "px";
		varblocklydiv.height = blocklyAltura + "px";

		const varinjectiondiv = document.getElementsByClassName('injectionDiv')[0];
		varinjectiondiv.style.width = blocklyAnchura + "px";
		varinjectiondiv.style.height = blocklyAltura + "px";

		const varsidebar = document.getElementById('sidebar');
		varsidebar.style.left = (blocklyAnchura) + "px";
		varsidebar.style.width = '300px';// Math.min((document.documentElement.clientWidth - blocklyAnchura + 15), 300) + "px";
		varsidebar.style.height = blocklyAltura - 2 + "px";

		const varmesa = document.getElementById('wrapperMesa');
		varmesa.style.height = blocklyAltura - 170 + "px";
		varmesa.style.width = '300px';//(document.documentElement.clientWidth - blocklyAnchura + 15) + "px";

		const varmesa2 = document.getElementById('mesa');
		varmesa2.style.height = blocklyAltura - 2 + "px";

		const timelineAddGroupButton = document.getElementById('timelineAddGroupButton');
		timelineAddGroupButton.style.top = blocklyAltura + "px";
		//varmesa.style.width = '300px';//(document.documentElement.clientWidth - blocklyAnchura + 15) + "px";

		timeline.setOptions({height: (document.documentElement.clientHeight - blocklyAltura - 30) + "px"});

		document.getElementById('separadorHorizontal').style.top = "0px";
		document.getElementById('separadorHorizontal').style.width = window.innerWidth + "px";

		Blockly.svgResize(workspacePlayground);
	},1);
}

function initMainWorkspaceBlockly() {
	/**
	 * Construct the blocks required by the uniforms category.
	 * @param {!Blockly.Workspace} workspace The workspace this flyout is for.
	 * @return {!Array.<!Element>} Array of XML block elements.
	 */
	Blockly.coloursFlyoutCallback = function (workspace) {
		var xmlList = [];
		/*var blockText = '<xml>' +
			'<block type="midi_knob2">' +
			'<field name="number">' + 1 + '</field>' +
			'</block>' +
			'</xml>';
		var block = Blockly.Xml.textToDom(blockText).firstChild;
		xmlList.push(block);
		var blockText = '<xml>' +
			'<block type="midi_pad2">' +
			'<field name="number">' + 1 + '</field>' +
			'</block>' +
			'</xml>';x-special/nautilus-clipboard
copy
file:///run/media/joselito/0123-4567/JPGx-special/nautilus-clipboard
copy
file:///run/media/joselito/0123-4567/VIDEO/mergeed.mp4


		var block = Blockly.Xml.textToDom(blockText).firstChild;
		xmlList.push(block);
		var blockText = '<xml>' +
			'<block type="midi_program">' +
			'</block>' +
			'</xml>';
		var block = Blockly.Xml.textToDom(blockText).firstChild;
		xmlList.push(block);*/

		var button = goog.dom.createDom('button');
		button.setAttribute('text', "Create timeline automation");
		button.setAttribute('callbackKey', 'CREATE_VARIABLE_AUTO');

		workspace.registerButtonCallback('CREATE_VARIABLE_AUTO', function (button) {
			timelineAddGroup();
			Blockly.mainWorkspace.toolbox_.refreshSelection()
		});

		xmlList.push(button);
		Object.entries(variables_a_inyectar['auto']).forEach(function ([index, value]) {
			var blockText = '<xml>' +
				'<block type="auto_variable_get">' +
				'<field name="NAME">' + index + '</field>' +
				'</block>' +
				'</xml>';
			var block = Blockly.Xml.textToDom(blockText).firstChild;
			xmlList.push(block);
		});

		var button = goog.dom.createDom('button');
		button.setAttribute('text', "Create control at desk");
		button.setAttribute('callbackKey', 'CREATE_VARIABLE_DESK');

		workspace.registerButtonCallback('CREATE_VARIABLE_DESK', function (button) {
			tunedesk.addModule({name: randomWords(), type: 'slider', value: 0.2, min: 0, max: 1});
			Blockly.mainWorkspace.toolbox_.refreshSelection()
		});

		xmlList.push(button);

		Object.entries(variables_a_inyectar['tune']).forEach(function ([index, value]) {
			var blockText = '<xml>' +
				'<block type="auto_variable_get">' +
				'<field name="NAME">' + index + '</field>' +
				'</block>' +
				'</xml>';
			var block = Blockly.Xml.textToDom(blockText).firstChild;
			xmlList.push(block);
		});

		return xmlList;
	};

	/**
	 * Construct the blocks required by the propios category.
	 * @param {!Blockly.Workspace} workspace The workspace this flyout is for.
	 * @return {!Array.<!Element>} Array of XML block elements.
	 */
	Blockly.propiosFlyoutCallback = function (workspace) {
		var xmlList = [];
		console.log(workspace,bloquesPersonalizados)
		if(bloquesPersonalizados) Object.keys(bloquesPersonalizados).forEach(function (key) {
				const element = bloquesPersonalizados[key];
				var blockText = '<xml>' +
					'<block type="' + element['id'] + '">' + '</block>' +
					'</xml>';
				eval(element['codigo'].replaceAll('\n', ' '));
				Blockly.Blocks[element['id']] = {
					init: function () {
						this.jsonInit(JSON.parse(element['json']));
					}
				};
				//var labelBlock = 					'<label text="x'+element["id"]+'" web-class="deleteButtonToolbox" onclick="console.log(this.text)">x</label>' ;
				var block = Blockly.Xml.textToDom(blockText).firstChild;
				xmlList.push(block);
				//var block = Blockly.Xml.textToDom(labelBlock);
				//xmlList.push(block);

			}
		);
		return xmlList;
	};

	Blockly.audioFlyoutCallback = function (workspace) {
		var xmlList = [];

		if(!groupsTimeline.get("AUDIO")) {
			let button = goog.dom.createDom('button');
			button.setAttribute('text', "Load audio");
			button.setAttribute('callbackKey', 'LOAD_AUDIO');

			workspace.registerButtonCallback('LOAD_AUDIO', function (button) {
				openMultimedia();
			});
			xmlList.push(button);
		}

		preferences.ACTIVE_AUDIO_FEATURES.forEach(function (element) {
            var blockText = '<xml>' +
                '<block type="auto_variable_get">' +
                '<field name="NAME">audio_' + element + '</field>' +
                '</block>' +
                '</xml>';
				var block = Blockly.Xml.textToDom(blockText).firstChild;
				xmlList.push(block);

			}
		);
		return xmlList;
	};

	Blockly.imageFlyoutCallback = function (workspace) {
		var xmlList = [];

		let button = goog.dom.createDom('button');
		button.setAttribute('text', "Create texture");
		button.setAttribute('callbackKey', 'CREATE_TEXTURE');

		workspace.registerButtonCallback('CREATE_TEXTURE', function (button) {
			timelineAddGroup(null,"texture");
			Blockly.mainWorkspace.toolbox_.refreshSelection()
		});
		xmlList.push(button);

		button = goog.dom.createDom('button');
		button.setAttribute('text', preferences.BACKBUFFER? "Disable backbuffer" : "Enable backbuffer");
		button.setAttribute('callbackKey', 'DIS_ENABLE_BACKBUFFER');

		workspace.registerButtonCallback('DIS_ENABLE_BACKBUFFER', function (button) {
			preferences.BACKBUFFER = !preferences.BACKBUFFER;
			Blockly.mainWorkspace.toolbox_.refreshSelection()
		});
		xmlList.push(button);

		if(preferences.BACKBUFFER || Object.keys(variables_a_inyectar['textures'] || {}).length > 0) {
			let blockText = '<xml><block type="imgatthispixel"></block></xml>';
			let block = Blockly.Xml.textToDom(blockText).firstChild;
			xmlList.push(block);

		 	blockText = '<xml><block type="imgatpixel"></block></xml>';
		 	block = Blockly.Xml.textToDom(blockText).firstChild;
			xmlList.push(block);

			blockText = '<xml><block type="imgatnormpixel"></block></xml>';
			block = Blockly.Xml.textToDom(blockText).firstChild;
			xmlList.push(block);
		}
		return xmlList;
	};

	Blockly.parametersFlyoutCallback = function (workspace) {
		var propiosList = document.getElementById('generatorPre').innerHTML.split('var ');//.shift().pop();
		propiosList.shift();
		propiosList.pop();
		var xmlList = [];
		propiosList.forEach(function (element) {
				var nom_variable = element.split(" = ")[0];
				var nom_bloque = element.split("'")[1];
				console.log("ELEEE",element)
				if (element.split(".")[2].split("T")[0] == "value") {
					type = "block_parameter_value";
				} else {
					type = "block_parameter_statement";
				}

				var blockText = '<xml>' +
					'<block type="' + type + '">' +
					'<field name="NAME">' + nom_bloque + '</field>' +
					'</block>' +
					'</xml>';
				// console.log(element['codigo'].replaceAll('\\n',''));
				//eval(element['codigo'].replaceAll('\n',''));
				// Blockly.Blocks[element['id']] = {
				//  init: function() {
				//    this.jsonInit(JSON.parse( element['json']));
				//  }
				// };
				var block = Blockly.Xml.textToDom(blockText).firstChild;
				xmlList.push(block);
			}
		);
		return xmlList;
	};
	Blockly.Variables.createVariableButtonHandler = function(workspace, opt_callback, opt_type) {
		const type = opt_type || '';
		const text = randomWords();
		const existing = Blockly.Variables.nameUsedWithAnyType_(text, workspace);
		if (!existing) {
			workspace.createVariable(text, type);
			if (opt_callback) {
				opt_callback(text);
			}
		}
	};


	var blocklyArea = document.getElementById('blocklyArea');
	var blocklyDiv = document.getElementById('blocklyDiv');
	workspacePlayground = Blockly.inject(blocklyDiv,
		{toolbox: document.getElementById('toolbox')});
	workspacePlayground.mi_id="mainworkspace";
	workspacePlayground.registerToolboxCategoryCallback('RESOURCES', Blockly.coloursFlyoutCallback);
	workspacePlayground.registerToolboxCategoryCallback('PROPIOS', Blockly.propiosFlyoutCallback);
	workspacePlayground.registerToolboxCategoryCallback('PARAMETERS', Blockly.parametersFlyoutCallback);
	workspacePlayground.registerToolboxCategoryCallback('AUDIO', Blockly.audioFlyoutCallback);
	workspacePlayground.registerToolboxCategoryCallback('IMAGE', Blockly.imageFlyoutCallback);
	//Blockly.mainWorkspace.addChangeListener(saveWorkspace);

	blocklyAltura = window.innerHeight -200;
	blocklyAnchura = window.innerWidth -300;

	//workspacePlayground.createVariable('frameCount', "Number", null);


	workspacePlayground.registerButtonCallback('loadSoundButton', load_sound);

	window.addEventListener('resize', resizeInterfaz, false);
	Blockly.svgResize(workspacePlayground);

	function onNewBlockSelected(event) {
		setearProyectoComoGuardado(false);
		if (event.type == Blockly.Events.UI &&
			event.element == 'selected' &&
			!event.oldValue && event.newValue) {
			timeline.setSelection([]); // Deseleccionar items del timeline cuando se seleccionan bloques
		}
	}
	Blockly.mainWorkspace.addChangeListener(onNewBlockSelected);

	setTimeout(() => {
		function collapseAndDecollapseBlocks() {
			// Collapse and decollapse to get correct size
			const topBlocks = workspacePlayground.getTopBlocks(false);
			//Blockly.WorkspaceSvg.centerOnBlock(); (esto esta disponible en verisones posteriores de blockly)
			for (var i = 0; i < topBlocks.length; i++) {
				var block = topBlocks[i];
				while (block) {
					setTimeout(block.setCollapsed.bind(block, true), 1);
					block = block.getNextBlock();
				}
			}
			for (var i = 0; i < topBlocks.length; i++) {
				var block = topBlocks[i];
				while (block) {
					setTimeout(block.setCollapsed.bind(block, false), 1);
					block = block.getNextBlock();
				}
			}
			workspacePlayground.removeChangeListener(collapseAndDecollapseBlocks);
		}

		workspacePlayground.addChangeListener(collapseAndDecollapseBlocks);
	},1500);
	Blockly.onKeyDown
}


function openMultimedia(group = null, time=null) {
    const res = dialog.showOpenDialog({properties:['openFile']}).then( (response) => {
        if (!response.canceled) {
            try {
                const extension = response.filePaths[0].split('.').pop().toLowerCase();
                if(/mp4|webm|avi|mkv|ogv|flv|mov/.test(extension)){
                    timelineAddVideoItem(response.filePaths[0],group,time);
                }else if(/mp3|wav|ogg|flac|m4a|aif/gm.test(extension)) {
                    audio_file = response.filePaths[0];
					loadAudioOnTimeline();
                }else if(/gif|jpg|jpeg|png|svg|webp|bmp|ico/gm.test(extension)) {
                    timelineAddImageItem(response.filePaths[0],group,time);
                }else  {
                    console.log('Not recognized extension ' + response.filePaths[0] + err);
                }

            } catch (err) {console.log('Failed to load the file at ' + response.filePaths[0] + err); }
        }else{console.log('Apertura cancelada'); }
    });
}

function loadAudio() {
	const res = dialog.showOpenDialog({properties:['openFile']}).then( (response) => {
		if (!response.canceled) {
			try {
				audio_file = response.filePaths[0];
				openAudioAnalyzer();

			} catch (err) {console.log('Failed to load the file at ' + response.filePaths[0] + err); }
		}else{console.log('Apertura cancelada'); }
	});
}

function loadVideo() {
	dialog.showOpenDialog({properties:['openFile']}).then(  async (response) => {
		if (!response.canceled) {
			try {
				await timelineAddVideoItem(response.filePaths[0]);
				//videoElement = document.getElementById('videopreview0');
			} catch (err) {console.log('Failed to load the file at ' + response.filePaths[0] + err); }
		}else{console.log('Apertura cancelada'); }
	});
}

function stopAudioAnalyzer() {
    if(audioAnalyzer) {
        audioAnalyzer.stop();
        audioAnalyzer = null;
    }

}
function startAudioAnalyzer() 	{
	//audioContext = new AudioContext();
	if(!audioSource) audioSource = audioContext.createMediaElementSource(htmlAudioElement);
    audioSource.connect(audioContext.destination);
	audioAnalyzer = Meyda.createMeydaAnalyzer({
		"audioContext": audioContext,
		"source": audioSource,
		"bufferSize": preferences.AUDIO_BUFFER_SIZE,
        "windowingFunction": preferences.AUDIO_WINDOWING_FUNCTION,
		"featureExtractors": preferences.ACTIVE_AUDIO_FEATURES, //["rms", "energy"], //, "spectralSlope", "spectralCentroid", "spectralRolloff"],
		"callback": features => {
		    if(features.zcr) features.zcr/=(preferences.AUDIO_BUFFER_SIZE/2)-1;
		    if(features.energy) features.energy/=preferences.AUDIO_BUFFER_SIZE;
		    if(features.spectralCentroid) features.spectralCentroid/=preferences.AUDIO_BUFFER_SIZE/2;
		    if(features.spectralSpread) features.spectralSpread/=preferences.AUDIO_BUFFER_SIZE/2;
		    if(features.loudness) features.loudness=features.loudness.total/24.; // TODO permitir acceder a los especificos y ver poque puede llegar mas alla de 24
            if(features.spectralRolloff) features.spectralRolloff/=44100/2;      //44100Hz es el sample rate por default
            liveAudioFeatures = features;
		}
	});
	audioAnalyzer.start();
	htmlAudioElement.addEventListener("ended", () =>{
		audioAnalyzer.stop();
	});
}

function des_activateAudioFeature(feature, activar) {
    if(activar){
        preferences.ACTIVE_AUDIO_FEATURES.push(feature);
    }else{
        Object.entries(preferences.ACTIVE_AUDIO_FEATURES).forEach(([key, val]) => {
            if(val == feature){
                preferences.ACTIVE_AUDIO_FEATURES.splice(key,1);
            }
        });
    }
}

function updateVideosPosition() {
	itemsTimeline.get().forEach( (itemx) => {
		if(itemx.func == "texture"){
			video = variables_a_inyectar['textures'][itemx.group];
			console.log( itemx.start_ms, timeTimeline, (timeTimeline - itemx.start_ms) / 1000, (timeTimeline - itemx.start_ms))
			if(video && video.pause) {
				video.pause();
				video.currentTime = (timeTimeline - itemx.start_ms) / 1000;
				if (video.currentTime > 2 && timelinePlaying){
					video.play();
					video.currentTime = (timeTimeline - itemx.start_ms) / 1000;
				}
			}
		}
	});
}



function actualizarLog(){
	diverrores = document.getElementById('logErrores');
	diverrores.innerHTML=errorLine;
}
setInterval(actualizarLog, 150);


function getGLSLCode(){

}

function getISFCode(){

}

function setBPM(event){
	BPM = event.target.value;
	itemsTimeline.get().forEach( (itemx) => {
		if(itemx.func == "texture"){
			video = variables_a_inyectar['textures'][itemx.group];
			console.log( itemx.start_ms, timeTimeline)
			video.playbackRate = BPM / 60.;
			video.currentTime = (timeTimeline - itemx.start_ms)/1000;
			if(video.currentTime > 2 && timelinePlaying) video.play();
		}
	});
}

var sliderSmoothing = null;

function startMovingSliderSmoothly(name, event){
	sliderSmoothing = event.pageX;
}
function finishMovingSliderSmoothly(){
	sliderSmoothing = null;
}

function dragSliderSmoothly(elemento, event){
	if(sliderSmoothing){
		console.log(elemento, parseFloat(elemento.value),  ((event.pageX - sliderSmoothing)/100 ))
		//if((event.pageX - sliderSmoothing) < 0)
			elemento.value = parseFloat(elemento.value)*(1+((event.pageX - sliderSmoothing)/50. ));
	}
}

function addBotonesToToolbox(){
	let botoneraPlay = document.createElement('div');
	botoneraPlay.id="botoneraPlay"
	let botonPlay = document.createElement('img');
	botonPlay.src="icons/play.png"
	botonPlay.onclick=timelinePlay
	botonPlay.id="timelinePlayButton"
	botonPlay.style.height="20px"
	botonPlay.style.margin="4px"
	let botonPrevious = document.createElement('img');
	botonPrevious.src="icons/previous.png"
	botonPrevious.onclick=timelineGoToStart
	botonPrevious.id="timelineGoToStartButton"
	botonPrevious.style.height="20px"
	botonPrevious.style.margin="5px"
	let bpmInput = document.createElement('input');
	bpmInput.type="number"
	bpmInput.oninput=setBPM
	bpmInput.value=BPM
	bpmInput.title="BPM"
	bpmInput.id="bpmInput"
	bpmInput.style.height="20px"
	bpmInput.style.width="60px"
	bpmInput.style.margin="4px"
	botoneraPlay.appendChild(botonPrevious)
	botoneraPlay.appendChild(botonPlay)
	botoneraPlay.appendChild(bpmInput)
	document.getElementsByClassName('blocklyToolboxDiv')[0].appendChild(botoneraPlay);
}



function alertaso(){
	alert(Blockly.JavaScript.workspaceToCode(workspacePlayground));
}



function ajustar_ancho(){
	var anchura=Math.max(
		document.documentElement["clientWidth"],
		document.body["scrollWidth"],
		document.documentElement["scrollWidth"],
		document.body["offsetWidth"],
		document.documentElement["offsetWidth"]
	);
	var altura=Math.max(
		document.documentElement["clientHeight"],
		document.body["scrollHeight"],
		document.documentElement["scrollHeight"],
		document.body["offsetHeight"],
		document.documentElement["offsetHeight"]
	);
	altura=altura-236;
	anchura=anchura-220;
	blocklyDiv.style.height = altura+ 'px';
	blocklyArea.style.height = altura+ 'px';
	blocklyDiv.style.width = anchura+ 'px';
	blocklyArea.style.width = anchura+ 'px';
}

//setTimeout(ajustar_ancho, 1000);

function zoomWorkspace(e) {
	console.log(e);
	if(e.deltaY > 0){
		workspacePlayground.setScale(workspacePlayground.scale*=0.9);
		//document.getElementsByClassName('blocklyMainBackground')[0].style['fill'] = null;
		//document.getElementsByClassName('blocklyMainBackground')[0].style['fill-opacity'] =Math.max(parseFloat(document.getElementsByClassName('blocklyMainBackground')[0].style['fill-opacity']-0.05),0);
	}else{
		workspacePlayground.setScale(workspacePlayground.scale*=1.1);
		//document.getElementsByClassName('blocklyMainBackground')[0].style['fill'] = null;
		//document.getElementsByClassName('blocklyMainBackground')[0].style['fill-opacity'] =Math.min(parseFloat(document.getElementsByClassName('blocklyMainBackground')[0].style['fill-opacity'])+0.05,1);
	}
}

function updateFPS(){
	document.getElementById('pFPS').innerText= parseInt(fps) ? parseInt(fps)+"FPS" : "";
}



window.addEventListener('beforeunload', (event) => {
	// Cancel the event as stated by the standard.
	stash.set("jsonActual",get_json_proyecto())
	return "aaa";
});




},{"meyda":1,"random-words-mio":2}]},{},[3]);
