//require('electron-chromecast');
//const fs = require('fs');
//const ISFRenderer = require('interactive-shader-format-mio').Renderer;


//const {dialog} = require('electron').remote;
//var Meyda = require('meyda');

var cambiosCodigoGuardados=true;
var cambiosProyectoGuardados=true;
var pathProyectoActual='';
var runtime_code="";
var runtime_code_setup="";
var runtime_preload="";
var ejecutando_sketch=false;
var livecoding_activo=false;
var window_sketch;
var window_preferences;
var window_audio;
var window_block_factory;
var sketchFile ;
var nombresSonidos = [];
var timelinePlaying = false;
var timeWhenPlayClicked = 0;
var nextIdItemTimeline=0;
var nextVariableName="var1";
var previewMode=true;
var audio_file = "";
var curves = [];
var curveImages = [];
var midiMappings = [];
var clipboardTimeline = [];
var baseBlock = null;
var variables_a_inyectar = [];
var configurandoItem = null; // id del item del timeline configurandose ahora
variables_a_inyectar['tune'] = {};
variables_a_inyectar['auto'] = {};
variables_a_inyectar['features'] = {};
variables_a_inyectar['features'] = {};
variables_a_inyectar['textures'] = [];
variables_a_inyectar['texturesStatic'] = [];
var cambiosAInyectarTune = false; // TODO Estas se usan pa algo? diria que no
var cambiosAInyectarAuto = false;
var bloquesPersonalizados = [];
var workspacePlayground;
var blocklyAltura ;
var blocklyAnchura ;
var iframePreview;
var fps=0;
var errorLine = "";
var codigo_actual, codigo_preview;
var quality = 1;
var BPM = 60;
var timeTimeline = 0;
var timeTimelineSeek = 0;
window.opener=window;
var now_playing_audio = false;

const audioContext = new AudioContext();
var audioAnalyzer;
var audioSource;
var audioFeatures = [];
var liveAudioFeatures ={};
var videoElement ; // borrar cuando se maneje bien
var imgblackpixel ;
const isRenderer=false;
var ISFRendereer;
const ACTUAL_VERSION = 0.2;

var preferences = {
    OUTPUT_FRAMED: 0,
    GLSL_VERSION: 120,
    ACTIVE_AUDIO_FEATURES: [],
    AUDIO_BUFFER_SIZE: 512,
    AUDIO_WINDOWING_FUNCTION: "hanning",
	BACKBUFFER: false,
};

var isf_header1 = `
/*{
  "CREDIT": "Created with Astraltrip https://astraltrip.gl",
  "DESCRIPTION": "",
  "CATEGORIES": [
    "Generator"
  ],
  "INPUTS": [ 
`;
var isf_header_backbuffer = `
],
  "PERSISTENT_BUFFERS" : [
    "backbuffer"
  ],
  "PASSES" : [
    {
      "TARGET" : "backbuffer"
    }
`
var isf_header2 = `
  
  ]
}*/

uniform float time;
uniform vec2 resolution;
const float PI= 3.1415;

bool greaterThan(float a, float b){ return a>b;}
bool greaterThanEqual(float a, float b){ return a>=b;}
bool lessThan(float a, float b){ return a<b;}
bool lessThanEqual(float a, float b){  return a<=b;}
bool equal(float a, float b){  return a==b;}
bool notEqual(float a, float b){  return a!=b;}

`;

var wavesurfer;

if (!Object.prototype.hasOwnProperty.call(RegExp, 'escape')) {
	RegExp.escape = function(string) {
		// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#Escaping
		// https://github.com/benjamingr/RegExp.escape/issues/37
		return string.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
	};
}

if (!Object.prototype.hasOwnProperty.call(String, 'replaceAll')) {
	String.prototype.replaceAll = function(find, replace) {
		// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replaceAll
		// If you pass a RegExp to 'find', you _MUST_ include 'g' as a flag.
		// TypeError: "replaceAll must be called with a global RegExp" not included, will silently cause significant errors. _MUST_ include 'g' as a flag for RegExp.
		// String parameters to 'find' do not require special handling.
		// Does not conform to "special replacement patterns" when "Specifying a string as a parameter" for replace
		// Does not conform to "Specifying a function as a parameter" for replace
		return this.replace(
			Object.prototype.toString.call(find) == '[object RegExp]' ?
				find :
				new RegExp(RegExp.escape(find), 'g'),
			replace
		);
	}
}


function setearProyectoComoGuardado(guardado) {
	console.log("CAMBIOSENELPROYECTO"+guardado)
	if(cambiosProyectoGuardados && !guardado){
		cambiosProyectoGuardados=false;
		document.title = '* ' + document.title;
	}else if(guardado){
		cambiosProyectoGuardados=true;
		if(pathProyectoActual==''){
			document.title = 'New project' + ' - AstralTrip';
		}else {
			document.title = pathProyectoActual + ' - AstralTrip';
		}
	}
}

function evaluar_codigo_bloques_db(){
	stash.get("blocks").forEach(function(element) {
		eval(element.codigo);
	});
}

function reset_workspace(){
	
}



var absolutePositionOfElement = function(element) {
	var top = 0, left = 0;
	do {
		top += element.offsetTop  || 0;
		left += element.offsetLeft || 0;
		element = element.offsetParent;
	} while(element);

	return {
		top: top,
		left: left
	};
};


function getElementFromHTML(html){
	var aux=document.createElement('div');
	aux.innerHTML = html;
	return aux.firstChild;
}

function load_sound(){
	console.log("load sound");
	pathSonido = dialog.showOpenDialog({properties:['openFile']});
	nombre=pathSonido[0].replace(/^.*[\\\/]/, '').replace(/\.[^/.]+$/, "");
	nombresSonidos.push(nombre);
	runtime_code_setup+="sound_"+nombre+"=loadSound('"+pathSonido[0]+"');\n";
	nuevo_bloque=Blockly.mainWorkspace.newBlock('at_sound_variable');
	Blockly.mainWorkspace.addTopBlock(nuevo_bloque);

}


function guardar_codigo(){
 // if(document.getElementById("codigo_guardado").innerHTML)
	document.getElementById("codigo_guardado").innerHTML= document.getElementById("codigaso").value;
	document.getElementById("codigaso").innerText=document.getElementById("codigo_guardado").innerText;
	cambiosCodigoGuardados=false;
	blocklyFactory.blockLibraryController.updateButtons(cambiosCodigoGuardados);
} 

function inicializarCodigoNuevoBloque(){
 document.getElementById("codigo_guardado").innerHTML="// TODO: Assemble JavaScript into code variable.\nvar code = '...;\\n';\n";
 document.getElementById("codigaso").innerText="// TODO: Assemble JavaScript into code variable.\nvar code = '...;\\n';\n";
 document.getElementById("codigaso").value="// TODO: Assemble JavaScript into code variable.\nvar code = '...;\\n';\n";
}

function guardar_bloque(tipo, codigo, json_definicion){
	stash.set('blocks',stash.get('blocks')
		.push({ id: tipo, codigo: codigo, json: json_definicion}));
	cambiosCodigoGuardados=true;
}

function get_json_bloques(){
	return stash.get("blocks");
}

function get_json_proyecto(){
	var json_a_devolver
	let variables  = {};
	variables['gl_FragCoord'] = 'gl_FragColor';
	variables['gl_FragCoord'] = 'gl_FragCoord';
	variables['resolution'] = 'resolution';
	variables['RENDERSIZE'] = 'RENDERSIZE';
	variables['time'] = 'time';
	variables['isf_FragNormCoord'] = 'isf_FragNormCoord';

	Blockly.Variables.allUsedVarModels(Blockly.mainWorkspace).forEach((a)=>{variables[a.name]=a.name;});

	json_a_devolver = { 
		"workspace": Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(Blockly.mainWorkspace)),
		"blocks": get_json_bloques(),
		"variablesWorkspace": JSON.stringify(variables),
		//"variablesdesk": stash.get("variablesdesk"),
		//"variablesauto": stash.get("variablesauto"),
		"loadtimelineItems": itemsTimeline.get(),
		"loadtimelineGroups": groupsTimeline.get(),
		"loadtunedesk": tunedesk.getConf(),
		"astraltripVersion": ACTUAL_VERSION
	};

	return JSON.stringify(json_a_devolver);
}

function load_json_proyecto(data) {
	variables_a_inyectar['textures'] = [];
	console.log("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD",data)
	if(data['loadtimelineGroups']) {
		data['loadtimelineGroups'].forEach((item) => {
			groupsTimeline.add(item);
			console.log("ITEEM",item)
			if(item.type=="texture") variables_a_inyectar['textures'][item.id] = imgblackpixel;
			else variables_a_inyectar['auto'][item.id]=1;
		});
	}
	if(data['loadtimelineItems']) {
		data['loadtimelineItems'].forEach((item) => {
			itemsTimeline.add(item);
			if(item.curve)curves[item.id] = eval('new Bezier'+item.curve);
			else if(item.func == "audio") htmlAudioElement.src=item.audio_file;
			//else if(item.func == "texture") variables_a_inyectar['textures'][item.group] = document.getElementById('videoElement'+item.id);
		});
	}
	timeline.redraw();
	console.log("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD",variables_a_inyectar)

	if(data['loadtunedesk']) {
		tunedesk.loadConf(data['loadtunedesk']);
	}
	Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom(data['workspace']), Blockly.mainWorkspace);
	if(data['variablesWorkspace']) {
		const vars= JSON.parse(data['variablesWorkspace']);
		Object.keys(vars).forEach( (e,k) =>{
			try{workspacePlayground.createVariable(e);}
			catch (err) {
				console.log("ERR"+err)
			}
		});
	}
	// TODO manejar el guardado de blokes: que solo se guarden los personalizados que se usan en este proyecto
}

function inicializarProyecto() {
	Blockly.mainWorkspace.clear();
	Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom('<xml xmlns=\"http://www.w3.org/1999/xhtml\"><variables><variable type=\"\" id=\"p)j.x`B)(+U@CD^EjZji\">gl_FragColor</variable></variables><block type=\"glmain\" id=\"?~1PcXOKT=Wv9zIaIWC^\" x=\"287\" y=\"153\"><statement name=\"main\"><block type=\"variables_set\" id=\"wLm/T=W$H5w*k4iancF?\"><field name=\"VAR\" id=\"p)j.x`B)(+U@CD^EjZji\" variabletype=\"\">gl_FragColor</field><value name=\"value\"><block type=\"vec4\" id=\"tcLm!:g|vQ(EKI0KxA4=\"><value name="a"><shadow type="glfloat"><field name="num">1.0</field></shadow></value><value name="r"><shadow type="glfloat"><field name="num">1.0</field></shadow></value><value name="g"><shadow type="glfloat"><field name="num">1.0</field></shadow></value><value name="b"><shadow type="glfloat"><field name="num">1.0</field></shadow></value></block></value></block></statement></block></xml>'), workspacePlayground);

	//workspacePlayground.createVariable("gl_FragColor");
	workspacePlayground.createVariable("gl_FragCoord");
	//workspacePlayground.createVariable("mouse");
	workspacePlayground.createVariable("resolution");
	workspacePlayground.createVariable("RENDERSIZE");
	workspacePlayground.createVariable("time");
	workspacePlayground.createVariable("TIME");
	workspacePlayground.createVariable("isf_FragNormCoord");

	//tunedesk.addModule({name: "slider01", type: "slider01", value:0.2});
	//tunedesk.addModule({name: "slider", type: "slider", value:0.2, min:-1, max:1});
}

function new_project(inicializar=false){
	console.log("new_project");


	if(ejecutando_sketch && window_sketch){
		window_sketch.close();
	}

	document.title="New project - AstralTrip";
	cambiosCodigoGuardados=true;
	cambiosProyectoGuardados=true;
	runtime_code="";
	runtime_code_setup="";
	runtime_preload="";
	ejecutando_sketch=false;
	pathProyectoActual='';
	document.getElementById("logErrores").innerHTML="";
	stash.set("loadtimelineGroups",'');
	stash.set("loadtimelineItems",'');
	stash.set("pathProyectoActual",'');
	stash.set("codigo_actual",'');
	itemsTimeline.clear();
	groupsTimeline.clear();
	nextIdItemTimeline=0;
	curves = [];
	midiMappings = [];
	tunedesk.clear();

	variables_a_inyectar = [];
	variables_a_inyectar['tune'] = {};
	variables_a_inyectar['auto'] = {};
	variables_a_inyectar['features'] = {};
	variables_a_inyectar['textures'] = [];
	variables_a_inyectar['texturesStatic'] = [];

	cambiosAInyectarTune = false;
	cambiosAInyectarAuto = false;

	Blockly.mainWorkspace.clear();
    htmlAudioElement.src="";
	stopAudioAnalyzer();

	if(timelinePlaying)timelinePlay();
	timelineGoToStart();

    if(inicializar==true){
		inicializarProyecto();
	}
    if(previewMode) {
		previewMode=false;
		tooglePreviewMode();
	}
}

function openExporter() {
	if(timelinePlaying) timelinePlay();
	if(window_sketch) window_sketch.close();
	window_sketch= window.open("exporter.html","exporter","name=Exporter,menubar=no,titlebar=no");
}

function openPreferences() {
	const window_preferences= window.open("frame_preferences.html","preferences","name=Preferences,menubar=no,titlebar=no,scrollbars=no, location=no, width=400,height=600");
    window_preferences.opener = window;
    const timer = setInterval(() =>{
        if(window_preferences.closed) {
            clearInterval(timer);
            console.log(window.preferences,"FFF")
            stash.set('preferences', window.preferences);
        }
    },500);
}


function openAudioAnalyzer() {
	window_audio= window.open("audio_analyzer.html","Audio Analyzer","name=Audio analyzer,menubar=no,titlebar=no");
	window_audio.nextItemId = makeItemId();
}


function openGLSLExporter() {
	codigo_actual=getCodigoActual();
	window.open("export_to_glsl.html","exportGLSL","name=exportGLSL,menubar=no,titlebar=yes,toolbar=no, scrollbars=no, status=no, directories=no");
}

function save_file_project(path) {
	/*fs.writeFile(path, get_json_proyecto(), function (err) {
		if (err) {
			console.log('Failed to save the file at '+path + "       " + err);
			return ;
		}
		setearProyectoComoGuardado(true);
		console.log("guardado OK ."+path);
	});*/
}

function save_project(saveAs=false){
	console.log("save_project . Save as:"+saveAs);
	if(pathProyectoActual==='' || saveAs===true){
		dialog.showSaveDialog({properties:['showOverwriteConfirmation']}).then( (response) => {
			console.log(response.filePath);
			if(response.canceled) return;
			if(response.filePath.split('.').pop().toLowerCase() != "trip"){
				response.filePath +=".trip"
			}
			pathProyectoActual = response.filePath;
			stash.set('pathProyectoActual',pathProyectoActual);
			save_file_project(pathProyectoActual);
		});
	}else{
		save_file_project(pathProyectoActual);
	}
}

function open_project(){
	console.log("open_project");
	const res = dialog.showOpenDialog({properties:['openFile']}).then( (response) => {
		if (!response.canceled) {
			try {
				new_project();
				pathProyectoActual = response.filePaths[0];
				stash.set('pathProyectoActual',pathProyectoActual);
				//var data = JSON.parse(fs.readFileSync(pathProyectoActual, 'utf-8'));
				console.log(data);
				load_json_proyecto(data);
				setearProyectoComoGuardado(true);
			} catch (err) {
				console.log('Failed to load the file at ' + response.filePaths[0] + err);
			}
		}else{
			console.log('Apertura cancelada');
		}
	});
}


function open_project_stash(){
	const name = document.getElementById('projectsDropdown').value;
	new_project(false);

	load_json_proyecto(JSON.parse(stash.get(name)));
	pathProyectoActual = name;
	setearProyectoComoGuardado(true);
	closeOpenProjectMenu()
}

function save_project_stash(nameproject = null) {
	let nameaux;
	if (nameproject) {
		nameaux = nameproject;
	} else {
		nameaux = document.getElementById('proyectNameInput').value + ".trip";
	}
	stash.set(nameaux, get_json_proyecto());
	//stash.add("saved_projects", name);
	pathProyectoActual = nameaux;
	setearProyectoComoGuardado(true);
	closeSaveProjectMenu();

}


function delete_project_stash() {
	const name = document.getElementById('projectsDropdown').value;
	stash.cut(name);
}

function download(strData, strFileName, strMimeType) {
	var D = document,
		a = D.createElement("a");
	strMimeType= strMimeType || "application/octet-stream";


	if (navigator.msSaveBlob) { // IE10
		return navigator.msSaveBlob(new Blob([strData], {type: strMimeType}), strFileName);
	} /* end if(navigator.msSaveBlob) */


	if ('download' in a) { //html5 A[download]
		a.href = "data:" + strMimeType + "," + encodeURIComponent(strData);
		a.setAttribute("download", strFileName);
		a.innerHTML = "downloading...";
		D.body.appendChild(a);
		setTimeout(function() {
			a.click();
			D.body.removeChild(a);
		}, 66);
		return true;
	} /* end if('download' in a) */


	//do iframe dataURL download (old ch+FF):
	var f = D.createElement("iframe");
	D.body.appendChild(f);
	f.src = "data:" +  strMimeType   + "," + encodeURIComponent(strData);

	setTimeout(function() {
		D.body.removeChild(f);
	}, 333);
	return true;
}

function downloadProject() {
	const uriContent = "data:application/octet-stream," + encodeURIComponent(get_json_proyecto());
	document.getElementById('downloadProjectLink').href = uriContent;
	document.getElementById('downloadProjectLink').download = pathProyectoActual;
	document.getElementById('downloadProjectLink').click();
}

function openSaveProjectMenu() {
	document.getElementById('menuGuardarproyecto').style.display = "block";
}

function openOpenProjectMenu() {

	const menu = document.getElementById('projectsDropdown');
	menu.innerHTML = "";
	for (const key of Object.keys(stash.getAll())) {
		if(key.endsWith('.trip')) {
			const option = document.createElement("option");
			//option.onclick = "open_project_stash(" + key + ")";
			option.text = key;
			option.value = key;
			menu.appendChild(option);
		}
	}

	document.getElementById('menuAbrirproyecto').style.display = "block";
}


function closeSaveProjectMenu() {
	document.getElementById('menuGuardarproyecto').style.display = "none";
}

function closeOpenProjectMenu() {
	document.getElementById('menuAbrirproyecto').style.display = "none";
}

function importMediaFile(){
	console.log("importMediaFile");

	res = dialog.showOpenDialog({properties:['openFile']});

	if(res.constructor === Array){

	}else{
		console.log('Apertura cancelada');
	}
}

function restoreSession(){
	console.log("restoreSession");

	pathProyectoActual = stash.get('pathProyectoActual');
	bloquesPersonalizados = stash.get('bloquesPersonalizados') || [];
	bloquesPersonalizados.forEach( (bloque, key) => {
		bloquesPersonalizados[bloque.id] = bloque;
		delete bloquesPersonalizados[key];
	})
	//bloquesPersonalizados = db.get('blocks').value();

	var data = [];
	const jsonActual =JSON.parse(stash.get('jsonActual') || "");
	if(!jsonActual){
		new_project(true);
		return;
	}
	data['workspace'] = jsonActual['workspace'];
	console.log("restoreSession",data['workspace']);
	//data['workspace'] = stash.get('workspace');
	//console.log("restoreSession",data['workspace']);

	data['loadtimelineItems'] = jsonActual['loadtimelineItems'];
	data['loadtimelineGroups'] = jsonActual['loadtimelineGroups'];
	data['loadtunedesk'] = jsonActual['loadtunedesk'];
	data['variablesdesk'] = jsonActual['variablesdesk'];
	data['variablesauto'] = jsonActual['variablesauto'];
	//data['variablesTexture'] = stash.get('variablestexture');
	load_json_proyecto(data);
	actualizarCodigoPreview();

	setearProyectoComoGuardado(true);
}


// TODO ELiminar si de verdad no valen pa na
function stashWorkspace() {
    //stash.set("workspace",(String( Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(Blockly.mainWorkspace))).replace(/(\r\n\t|\n|\r\t)/gm," ")));
}

function saveWorkspace(){
	//stash.set('workspace',Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(Blockly.mainWorkspace)));
}

function getCodigoActual(){
	let result = "";
	result += "#version "+ preferences.GLSL_VERSION + "\n";
	result += isf_header1;
	if(variables_a_inyectar['textures']) Object.entries(variables_a_inyectar['textures']).forEach(([key, val]) => {
		console.log('TEXTUREEE', key,val)
		result += '{ "NAME" : "'+key+'","TYPE" : "image" },';
	});
    //if(variables_a_inyectar['texturesStatic']) Object.entries(variables_a_inyectar['texturesStatic']).forEach(([key, val]) => {
	//	console.log('TEXTUREEeeessssE', key,val)
	//	result += '{ "NAME" : "'+key+'","TYPE" : "image" },';
	//});
	result = result.slice(0,-1);        // kita la ultima coma (o el espacio final de isf_header_1 si no se añadio ninguna textura)
	preferences.BACKBUFFER? result+=isf_header_backbuffer : null;
	result += isf_header2;
	Object.entries(variables_a_inyectar['tune']).forEach(([key, val]) => {
		result += "uniform float " + key + ";\n";
	});
	Object.entries(variables_a_inyectar['auto']).forEach(([key, val]) => {
		result += "uniform float " + key + ";\n";
	});
	//Object.entries(variables_a_inyectar['features']).forEach(([key, val]) => {
	preferences.ACTIVE_AUDIO_FEATURES.forEach((val) => {
        if(val=="chroma"){      // Todo cambiar esto por arrays de isf, que permiten inyectar un array entero al gl
            for(x=0; x<=11; x++){
                result += "uniform float audio_chroma_" + x + ";\n";
            }
        }
        if(val=="mfcc"){
            for(x=0; x<=12; x++){
                result += "uniform float audio_mfcc_" + x + ";\n";
            }
        }else {
            result += "uniform float audio_" + val + ";\n";
        }

	});
	result += String(Blockly.JavaScript.workspaceToCode(workspacePlayground));//.replace('},99'," ");
	console.log(result);
	return result;
}

function run_project() {
	console.log("running");
	//const codigo_actual=(String(Blockly.JavaScript.workspaceToCode(workspacePlayground)).replace(/(\r\n\t|\n|\r\t)/gm," "));
	codigo_actual = getCodigoActual();
	//stashWorkspace();
	if (window_sketch && ! window_sketch.closed) {
		window_sketch.reloadSource(codigo_actual)
		console.log(window_sketch);
	} else {
		window_sketch = window.open("frame_sketch_isf.html", "output", "name=Output,menubar=yes,titlebar=yes,toolbar=no, scrollbars=no, status=no, directories=no, frame="+preferences.OUTPUT_FRAMED);
		window_sketch.opener = window;
		$(window_sketch).ready(() =>{
			console.log(window_sketch);
			setTimeout( () => {
				try {
					window_sketch.variables_a_inyectar = variables_a_inyectar;
					window_sketch.liveAudioFeatures = liveAudioFeatures;
					window_sketch.reloadSource(codigo_actual);
				}catch(e){
					setTimeout( () => {
							window_sketch.variables_a_inyectar = variables_a_inyectar;
							window_sketch.liveAudioFeatures = liveAudioFeatures;
							window_sketch.reloadSource(codigo_actual);
					},1000);
				}
			},500);
		});
		//electron.ipcRenderer.send('open_renderer', codigo_actual)
		//window_sketch.style.webkitAppRegion = "drag"
		var timer = setInterval(function () {
			if (window_sketch.closed) {
				clearInterval(timer);
				ejecutando_sketch = false;
			}
		}, 100);
		ejecutando_sketch = true;
	}
}

function des_activar_livecoding(){
	if(livecoding_activo){
		Blockly.mainWorkspace.removeChangeListener(run_project);
		document.getElementById("runButton").src="icons/refresh-line.png";
		livecoding_activo=false;
	}else{
		run_project();
		Blockly.mainWorkspace.addChangeListener(run_project);
		document.getElementById("runButton").src="icons/refresh-line-green.png";
		livecoding_activo=true;
	}
}

function guardar_bloque_pro(tipo, codigo, json_definicion, json_definicion_molde, json_definicion_bloques_generadores){
	//var bloque = db.get('blocks').find({ id: tipo });
	//if(bloque){
	//	db.get('blocks').remove({ id: tipo }).write();
	//}
	//db.get('blocks')
	//	.push({ id: tipo, codigo: codigo, json: json_definicion, json_molde: json_definicion_molde , json_bloques: json_definicion_bloques_generadores })
	//	.write();
	bloquesPersonalizados.push({ id: tipo, codigo: codigo, json: json_definicion, json_molde: json_definicion_molde , json_bloques: json_definicion_bloques_generadores });
	stash.set('bloquesPersonalizados',bloquesPersonalizados);
	cambiosCodigoGuardados=true;
	eval(codigo);
}
function onclickgenerateBlockLibraryButton(){
	var block = FactoryUtils.getRootBlock(BlockFactory.mainWorkspace);
	var block_preview = BlockFactory.previewWorkspace.getAllBlocks()[0];
	var block_code = String(Blockly.JavaScript.workspaceToCode(BlockFactory.codeWorkspace));
	if(block_preview.nextConnection == null && block_code.substr(-2) == "; "){
		block_code = block_code.slice(0, -2);		// El slice elimina el ; final si el bloque no es de los que tienen conexion por abajo
	}
	document.getElementById('codigaso').innerText="";
	console.log(document.getElementById('generatorPre').innerHTML);
	var cleanCode = String(document.getElementById('generatorPre').innerHTML)
		.replace('<textarea id=\"codigaso\" oninput=\"guardar_codigo()\" style=\"width: 95%;height: 3em;\">', '//inicio_codigo_usuario\n')
		.replace('</textarea>',"")
		.replace('// TODO: Change ORDER_NONE to the correct strength.',"")
		.replace('//inicio_codigo_usuario\n','var code=\''+block_code+'\';')
		.replaceAll('\n',' ');
	console.log(cleanCode,block_code);
	var nombreBloque= getTipoBloqueActual();
	var bloquesGeneradores=BlockFactory.codeWorkspace.getAllBlocks()[0];
	//console.log(String(Blockly.Xml.blockToDom(block_preview).outerHTML));
	guardar_bloque_pro(nombreBloque,
					   cleanCode,
					   document.getElementById('languagePre').innerHTML,
					   String(Blockly.Xml.blockToDomWithXY(block).outerHTML),
						String(Blockly.Xml.blockToDomWithXY(bloquesGeneradores).outerHTML));
}

function restaurar_codigo_bloque(tipoBloque){
	var bloque = bloquesPersonalizados[tipoBloque];
	var cleanCode = bloque['codigo'];
	// Deshace las sustituciones que hizo en la funcion onclickgenerateBlockLibraryButton
	var dirtyCode = cleanCode.replace('//inicio_codigo_usuario\n', '<textarea id=\"codigaso\" oninput=\"guardar_codigo()\" style=\"width: 95%;height: 3em;\">').replace("\n//fin_codigo_usuario",'</textarea>');
	//console.log(dirtyCode);
	var veryCleanCode = cleanCode.split('//inicio_codigo_usuario\n')[1].split('\n//fin_codigo_usuario')[0];
	//expresion = /(capítulo \d+(\.\d)*)/i;
	set_codigo_guardado(veryCleanCode);
	document.getElementById('generatorPre').innerHTML = dirtyCode;
	cambiosCodigoGuardados=true;
}


function set_codigo_guardado(nuevo_codigo){
	document.getElementById("codigo_guardado").innerHTML= nuevo_codigo;
} 

function getTipoBloqueActual(){
	return String(document.getElementById('saveToBlockLibraryButton').innerHTML).split('"')[1];
}


function appendXMLtoolboxes(){
	var xmlhttp, xmlhttp2, xmlhttp3, xmlhttp4;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
		xmlhttp2 = new XMLHttpRequest();
		xmlhttp3 = new XMLHttpRequest();
		xmlhttp4 = new XMLHttpRequest();
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.body.appendChild(getElementFromHTML(xmlhttp.responseText));
		}
	}

	xmlhttp2.onreadystatechange = function() {
		if (xmlhttp2.readyState == 4 && xmlhttp2.status == 200) {
			document.body.appendChild(getElementFromHTML(xmlhttp2.responseText));
		}
	}

	xmlhttp3.onreadystatechange = function() {
		if (xmlhttp3.readyState == 4 && xmlhttp3.status == 200) {
			document.body.appendChild(getElementFromHTML(xmlhttp3.responseText));
			Blockly.mainWorkspace.updateToolbox(xmlhttp3.responseText);
		}
	}

	xmlhttp4.onreadystatechange = function() {
		if (xmlhttp4.readyState == 4 && xmlhttp4.status == 200) {
			document.body.appendChild(getElementFromHTML(xmlhttp4.responseText));
		}
	}

	xmlhttp.open("GET", "toolboxes/toolbox_workspacefactory.xml", true);
	xmlhttp.send();

	xmlhttp2.open("GET", "toolboxes/toolbox_blockfactory.xml", true);
	xmlhttp2.send();

	xmlhttp3.open("GET", "toolboxes/toolbox_main.xml", true);
	xmlhttp3.send();

	xmlhttp4.open("GET", "toolboxes/toolbox_base_blocks.xml", true);
	xmlhttp4.send();

}

function actualizarCodigoPreview(){
	codigo_preview = previewMode ? getCodigoActual() : 'void main( void ) {}' ;

	reloadSource(codigo_preview);
}

function tooglePreviewMode(){
	previewMode=!previewMode;
	if(previewMode){
		document.getElementById('isf-canvas').style.display = 'block';
		Blockly.mainWorkspace.addChangeListener(actualizarCodigoPreview);
		actualizarCodigoPreview();
		animatex();
	}else{
		document.getElementById('isf-canvas').style.display = 'none';
		Blockly.mainWorkspace.removeChangeListener(actualizarCodigoPreview);
	}

}

// esto se llama desde blocky_compressed linea 1355
function showblockFactoryModal(blockCodigo, blockDefinicion) {
    //window_block_factory= window.open("frame_block_factory.html","blockFactory","name=Block factory | EXPERIMENTAL,menubar=no,titlebar=no");
    //return

    document.getElementById('blockFactoryModal').style.display="block";
	baseBlock=blockCodigo;
	BlockFactory.codeWorkspace.clear();
	//BlockFactory.codeWorkspace.addTopBlock(block).initSvg();

	// Sorry xD
	if(blockDefinicion) {
		BlockFactory.mainWorkspace.clear();
		Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom('<xml xmlns="http://www.w3.org/1999/xhtml">'+blockCodigo+'</xml>'), BlockFactory.codeWorkspace);
		Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom('<xml xmlns="http://www.w3.org/1999/xhtml">'+blockDefinicion+'</xml>'), BlockFactory.mainWorkspace);
	}else{
		Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom('<xml xmlns="http://www.w3.org/1999/xhtml">'+Blockly.Xml.domToText(Blockly.Xml.blockToDom(blockCodigo))+'</xml>'), BlockFactory.codeWorkspace);
	}
    FactoryUtils.show('blockFactoryContent');
	window.dispatchEvent(new Event('resize'));
}

function showMenuGuardarProyectol() {
	document.getElementById('menuGuardarproyecto')
	document.getElementById('menuGuardarproyecto').style.display="block";

}

function editBlockFromLibrary(block) {
	const bloque = bloquesPersonalizados[block.type];
	if(bloque){
		showblockFactoryModal(bloque.json_bloques, bloque.json_molde);
	}
}

//todo borrar una de las dos
function deleteBlockFromLibrary(block) {
	bloquesPersonalizados.splice(block, 1);
}
//todo borrar una de las dos
function borrarBloqueDB(tipoBloque){
	bloquesPersonalizados.splice(tipoBloque, 1);
}

function borrarBloqueActualDB(){
	borrarBloqueDB(getTipoBloqueActual());
}

function hideblockFactoryModal() {
	document.getElementById('blockFactoryModal').style.display="none";
}

function unfoldBlock(block) {
	const bloque = bloquesPersonalizados[block];
	if(bloque){
		var nuevos_bloques = Blockly.Xml.textToDom('<xml xmlns="http://www.w3.org/1999/xhtml">'+bloque.json_bloques+'</xml>');
		//block.parentBlock

block.dispose(!0,!0);
		const nuevos = Blockly.Xml.appendDomToWorkspace(nuevos_bloques,Blockly.mainWorkspace);
		//Blockly.mainWorkspace.remo
//TODO mantener las conexiones si las hubiese
		// TODO mantener los bloques que hubiera conectados, cuando funcione lo de parametrizar
		//block.parentBlock.nextConnection.connect(childBlock.previousConnection);
		//showblockFactoryModal(bloque.json_bloques, bloque.json_molde);
	}
}


function resizeInterfaz() {
	setTimeout( () => {
		var element = blocklyArea;
		var x = 0;
		var y = 0;
		do {
			x += element.offsetLeft;
			y += element.offsetTop;
			element = element.offsetParent;
		} while (element);
		// Position blocklyDiv over blocklyArea.
		blocklyDiv.style.left = x + 'px';
		blocklyDiv.style.top = (y - 58) + 'px';

		blocklyAnchura = window.innerWidth - 300;

		timeline.setOptions({height: (document.documentElement.clientHeight - blocklyAltura - 30) + "px"});


		blocklyDiv.style.width = blocklyAnchura + "px";
		blocklyDiv.style.height = blocklyAltura + "px";

		blocklyArea.style.width = blocklyAnchura + "px";
		blocklyArea.width = blocklyAnchura + "px";
		blocklyArea.style.height = blocklyAltura + "px";
		blocklyArea.height = blocklyAltura + "px";

		const varblocklysvg = document.getElementsByClassName('blocklySvg')[0];
		varblocklysvg.style.width = blocklyAnchura + "px";
		varblocklysvg.width = blocklyAnchura + "px";
		varblocklysvg.style.height = blocklyAltura + "px";
		varblocklysvg.height = blocklyAltura + "px";

		const varblocklydiv = document.getElementById('blocklyDiv');
		varblocklydiv.style.width = blocklyAnchura + "px";
		varblocklydiv.width = blocklyAnchura + "px";
		varblocklydiv.style.height = blocklyAltura + "px";
		varblocklydiv.height = blocklyAltura + "px";

		const varinjectiondiv = document.getElementsByClassName('injectionDiv')[0];
		varinjectiondiv.style.width = blocklyAnchura + "px";
		varinjectiondiv.style.height = blocklyAltura + "px";

		const varsidebar = document.getElementById('sidebar');
		varsidebar.style.left = (blocklyAnchura) + "px";
		varsidebar.style.width = '300px';// Math.min((document.documentElement.clientWidth - blocklyAnchura + 15), 300) + "px";
		varsidebar.style.height = blocklyAltura - 2 + "px";

		const varmesa = document.getElementById('wrapperMesa');
		varmesa.style.height = blocklyAltura - 170 + "px";
		varmesa.style.width = '300px';//(document.documentElement.clientWidth - blocklyAnchura + 15) + "px";

		const varmesa2 = document.getElementById('mesa');
		varmesa2.style.height = blocklyAltura - 2 + "px";

		const timelineAddGroupButton = document.getElementById('timelineAddGroupButton');
		timelineAddGroupButton.style.top = 25 + blocklyAltura + "px";
		//varmesa.style.width = '300px';//(document.documentElement.clientWidth - blocklyAnchura + 15) + "px";

		timeline.setOptions({height: (document.documentElement.clientHeight - blocklyAltura - 30) + "px"});

		document.getElementById('separadorHorizontal').style.top = "0px";
		document.getElementById('separadorHorizontal').style.width = window.innerWidth + "px";

		Blockly.svgResize(workspacePlayground);
	},1);
}

function initMainWorkspaceBlockly() {
	/**
	 * Construct the blocks required by the uniforms category.
	 * @param {!Blockly.Workspace} workspace The workspace this flyout is for.
	 * @return {!Array.<!Element>} Array of XML block elements.
	 */
	Blockly.coloursFlyoutCallback = function (workspace) {
		var xmlList = [];
		/*var blockText = '<xml>' +
			'<block type="midi_knob2">' +
			'<field name="number">' + 1 + '</field>' +
			'</block>' +
			'</xml>';
		var block = Blockly.Xml.textToDom(blockText).firstChild;
		xmlList.push(block);
		var blockText = '<xml>' +
			'<block type="midi_pad2">' +
			'<field name="number">' + 1 + '</field>' +
			'</block>' +
			'</xml>';

		var block = Blockly.Xml.textToDom(blockText).firstChild;
		xmlList.push(block);
		var blockText = '<xml>' +
			'<block type="midi_program">' +
			'</block>' +
			'</xml>';
		var block = Blockly.Xml.textToDom(blockText).firstChild;
		xmlList.push(block);*/

		var button = goog.dom.createDom('button');
		button.setAttribute('text', "Create timeline automation");
		button.setAttribute('callbackKey', 'CREATE_VARIABLE_AUTO');

		workspace.registerButtonCallback('CREATE_VARIABLE_AUTO', function (button) {
			timelineAddGroup();
			Blockly.mainWorkspace.toolbox_.refreshSelection()
		});

		xmlList.push(button);
		Object.entries(variables_a_inyectar['auto']).forEach(function ([index, value]) {
			var blockText = '<xml>' +
				'<block type="auto_variable_get">' +
				'<field name="NAME">' + index + '</field>' +
				'</block>' +
				'</xml>';
			var block = Blockly.Xml.textToDom(blockText).firstChild;
			xmlList.push(block);
		});

		var button = goog.dom.createDom('button');
		button.setAttribute('text', "Create control at desk");
		button.setAttribute('callbackKey', 'CREATE_VARIABLE_DESK');

		workspace.registerButtonCallback('CREATE_VARIABLE_DESK', function (button) {
			tunedesk.addModule({name: randomWords(), type: 'slider', value: 0.2, min: 0, max: 1});
			Blockly.mainWorkspace.toolbox_.refreshSelection()
		});

		xmlList.push(button);

		Object.entries(variables_a_inyectar['tune']).forEach(function ([index, value]) {
			var blockText = '<xml>' +
				'<block type="auto_variable_get">' +
				'<field name="NAME">' + index + '</field>' +
				'</block>' +
				'</xml>';
			var block = Blockly.Xml.textToDom(blockText).firstChild;
			xmlList.push(block);
		});

		return xmlList;
	};

	/**
	 * Construct the blocks required by the propios category.
	 * @param {!Blockly.Workspace} workspace The workspace this flyout is for.
	 * @return {!Array.<!Element>} Array of XML block elements.
	 */
	Blockly.propiosFlyoutCallback = function (workspace) {
		var xmlList = [];
		console.log(workspace,bloquesPersonalizados)
		if(bloquesPersonalizados) Object.keys(bloquesPersonalizados).forEach(function (key) {
				const element = bloquesPersonalizados[key];
				var blockText = '<xml>' +
					'<block type="' + element['id'] + '">' + '</block>' +
					'</xml>';
				eval(element['codigo'].replaceAll('\n', ' '));
				Blockly.Blocks[element['id']] = {
					init: function () {
						this.jsonInit(JSON.parse(element['json']));
					}
				};
				//var labelBlock = 					'<label text="x'+element["id"]+'" web-class="deleteButtonToolbox" onclick="console.log(this.text)">x</label>' ;
				var block = Blockly.Xml.textToDom(blockText).firstChild;
				xmlList.push(block);
				//var block = Blockly.Xml.textToDom(labelBlock);
				//xmlList.push(block);

			}
		);
		return xmlList;
	};

	Blockly.audioFlyoutCallback = function (workspace) {
		var xmlList = [];

		if(!groupsTimeline.get("AUDIO")) {
			let button = goog.dom.createDom('button');
			button.setAttribute('text', "Load audio");
			button.setAttribute('callbackKey', 'LOAD_AUDIO');

			workspace.registerButtonCallback('LOAD_AUDIO', function (button) {
				//openMultimedia();
				$("#musicInput").trigger('click')
			});
			xmlList.push(button);
		}

		preferences.ACTIVE_AUDIO_FEATURES.forEach(function (element) {
            var blockText = '<xml>' +
                '<block type="auto_variable_get">' +
                '<field name="NAME">audio_' + element + '</field>' +
                '</block>' +
                '</xml>';
				var block = Blockly.Xml.textToDom(blockText).firstChild;
				xmlList.push(block);

			}
		);
		return xmlList;
	};

	Blockly.imageFlyoutCallback = function (workspace) {
		var xmlList = [];

		let button = goog.dom.createDom('button');
		button.setAttribute('text', "Create texture");
		button.setAttribute('callbackKey', 'CREATE_TEXTURE');

		workspace.registerButtonCallback('CREATE_TEXTURE', function (button) {
			timelineAddGroup(null,"texture");
			Blockly.mainWorkspace.toolbox_.refreshSelection()
		});
		xmlList.push(button);

		button = goog.dom.createDom('button');
		button.setAttribute('text', preferences.BACKBUFFER? "Disable backbuffer" : "Enable backbuffer");
		button.setAttribute('callbackKey', 'DIS_ENABLE_BACKBUFFER');

		workspace.registerButtonCallback('DIS_ENABLE_BACKBUFFER', function (button) {
			preferences.BACKBUFFER = !preferences.BACKBUFFER;
			Blockly.mainWorkspace.toolbox_.refreshSelection()
		});
		xmlList.push(button);

		if(preferences.BACKBUFFER || Object.keys(variables_a_inyectar['textures'] || {}).length > 0) {
			let blockText = '<xml><block type="imgatthispixel"></block></xml>';
			let block = Blockly.Xml.textToDom(blockText).firstChild;
			xmlList.push(block);

		 	blockText = '<xml><block type="imgatpixel"></block></xml>';
		 	block = Blockly.Xml.textToDom(blockText).firstChild;
			xmlList.push(block);

			blockText = '<xml><block type="imgatnormpixel"></block></xml>';
			block = Blockly.Xml.textToDom(blockText).firstChild;
			xmlList.push(block);
		}
		return xmlList;
	};

	Blockly.parametersFlyoutCallback = function (workspace) {
		var propiosList = document.getElementById('generatorPre').innerHTML.split('var ');//.shift().pop();
		propiosList.shift();
		propiosList.pop();
		var xmlList = [];
		propiosList.forEach(function (element) {
				var nom_variable = element.split(" = ")[0];
				var nom_bloque = element.split("'")[1];
				console.log("ELEEE",element)
				if (element.split(".")[2].split("T")[0] == "value") {
					type = "block_parameter_value";
				} else {
					type = "block_parameter_statement";
				}

				var blockText = '<xml>' +
					'<block type="' + type + '">' +
					'<field name="NAME">' + nom_bloque + '</field>' +
					'</block>' +
					'</xml>';
				// console.log(element['codigo'].replaceAll('\\n',''));
				//eval(element['codigo'].replaceAll('\n',''));
				// Blockly.Blocks[element['id']] = {
				//  init: function() {
				//    this.jsonInit(JSON.parse( element['json']));
				//  }
				// };
				var block = Blockly.Xml.textToDom(blockText).firstChild;
				xmlList.push(block);
			}
		);
		return xmlList;
	};
	Blockly.Variables.createVariableButtonHandler = function(workspace, opt_callback, opt_type) {
		const type = opt_type || '';
		const text = randomWords();
		const existing = Blockly.Variables.nameUsedWithAnyType_(text, workspace);
		if (!existing) {
			workspace.createVariable(text, type);
			if (opt_callback) {
				opt_callback(text);
			}
		}
	};


	var blocklyArea = document.getElementById('blocklyArea');
	var blocklyDiv = document.getElementById('blocklyDiv');
	workspacePlayground = Blockly.inject(blocklyDiv,
		{toolbox: document.getElementById('toolbox')});
	workspacePlayground.mi_id="mainworkspace";
	workspacePlayground.registerToolboxCategoryCallback('RESOURCES', Blockly.coloursFlyoutCallback);
	workspacePlayground.registerToolboxCategoryCallback('PROPIOS', Blockly.propiosFlyoutCallback);
	workspacePlayground.registerToolboxCategoryCallback('PARAMETERS', Blockly.parametersFlyoutCallback);
	workspacePlayground.registerToolboxCategoryCallback('AUDIO', Blockly.audioFlyoutCallback);
	workspacePlayground.registerToolboxCategoryCallback('IMAGE', Blockly.imageFlyoutCallback);
	//Blockly.mainWorkspace.addChangeListener(saveWorkspace);

	blocklyAltura = window.innerHeight -200;
	blocklyAnchura = window.innerWidth -300;

	//workspacePlayground.createVariable('frameCount', "Number", null);


	workspacePlayground.registerButtonCallback('reloadToolboxes', function (button) {
		appendXMLtoolboxes();
	});
	workspacePlayground.registerButtonCallback('loadSoundButton', load_sound);

	window.addEventListener('resize', resizeInterfaz, false);
	Blockly.svgResize(workspacePlayground);

	function onNewBlockSelected(event) {
		setearProyectoComoGuardado(false);
		if (event.type == Blockly.Events.UI &&
			event.element == 'selected' &&
			!event.oldValue && event.newValue) {
			timeline.setSelection([]); // Deseleccionar items del timeline cuando se seleccionan bloques
		}
	}
	Blockly.mainWorkspace.addChangeListener(onNewBlockSelected);


	function collapseAndDecollapseBlocks() {
		// Collapse and decollapse to get correct size
		console.log("COLLAPSEANDOOOO")
		const topBlocks = workspacePlayground.getTopBlocks(false);
		//Blockly.WorkspaceSvg.centerOnBlock(); (esto esta disponible en verisones posteriores de blockly)
		for (var i = 0; i < topBlocks.length; i++) {
			var block = topBlocks[i];
			while (block) {
				setTimeout(block.setCollapsed.bind(block, true), 1);
				block = block.getNextBlock();
			}
		}
		for (var i = 0; i < topBlocks.length; i++) {
			var block = topBlocks[i];
			while (block) {
				setTimeout(block.setCollapsed.bind(block, false), 1);
				block = block.getNextBlock();
			}
		}
		workspacePlayground.updateToolbox($('#toolbox_main')[0]),1000
		workspacePlayground.removeChangeListener(collapseAndDecollapseBlocks);
	}

	workspacePlayground.addChangeListener(collapseAndDecollapseBlocks);
	//Blockly.onKeyDown
}


function openMultimedia(group = null, time=null) {
    const res = dialog.showOpenDialog({properties:['openFile']}).then( (response) => {
        if (!response.canceled) {
            try {
                const extension = response.filePaths[0].split('.').pop().toLowerCase();
                if(/mp4|webm|avi|mkv|ogv|flv|mov/.test(extension)){
                    timelineAddVideoItem(response.filePaths[0],group,time);
                }else if(/mp3|wav|ogg|flac|m4a|aif/gm.test(extension)) {
                    audio_file = response.filePaths[0];
					loadAudioOnTimeline();
                }else if(/gif|jpg|jpeg|png|svg|webp|bmp|ico/gm.test(extension)) {
                    timelineAddImageItem(response.filePaths[0],group,time);
                }else  {
                    console.log('Not recognized extension ' + response.filePaths[0] + err);
                }

            } catch (err) {console.log('Failed to load the file at ' + response.filePaths[0] + err); }
        }else{console.log('Apertura cancelada'); }
    });
}

function loadAudio() {
	const res = dialog.showOpenDialog({properties:['openFile']}).then( (response) => {
		if (!response.canceled) {
			try {
				audio_file = response.filePaths[0];
				openAudioAnalyzer();

			} catch (err) {console.log('Failed to load the file at ' + response.filePaths[0] + err); }
		}else{console.log('Apertura cancelada'); }
	});
}

function loadVideo() {
	dialog.showOpenDialog({properties:['openFile']}).then(  async (response) => {
		if (!response.canceled) {
			try {
				await timelineAddVideoItem(response.filePaths[0]);
				//videoElement = document.getElementById('videopreview0');
			} catch (err) {console.log('Failed to load the file at ' + response.filePaths[0] + err); }
		}else{console.log('Apertura cancelada'); }
	});
}

function loadTripFile(inputElement) {
	const file = inputElement.files[0];
	const url = file.name;
	console.log(inputElement,url)
	const extension = url.split('.').pop().toLowerCase();
	if(/trip/gm.test(extension)) {
		var reader = new FileReader();
		reader.onload = (e) => {
			new_project(false);
			console.log(e.target.result);
			load_json_proyecto(JSON.parse(e.target.result));
			pathProyectoActual = url;
			setearProyectoComoGuardado(true);
		};
		reader.readAsText(file);
	}

}

function loadAudioFile(inputElement) {
	const file = inputElement.files[0];
	const url = file.name;
	console.log(inputElement,url)
	const extension = url.split('.').pop().toLowerCase();
	if(/mp3|wav|ogg|flac|m4a|aif/gm.test(extension)) {
		var reader = new FileReader();
		reader.onload = (e) => {
			audio_file = e.target.result;
			loadAudioOnTimeline();
		};
		reader.readAsDataURL(file);
	}

}

function loadTextureFile(inputElement) {
	const file = inputElement.files[0];
	const url = file.name;
	console.log(inputElement,url)
	const extension = url.split('.').pop().toLowerCase();
	if(/mp4|webm|avi|mkv|ogv|flv|mov/.test(extension)){
		var reader = new FileReader();
		reader.onload = (e) => {
			timelineAddVideoItem(e.target.result, inputElement.getAttribute('data-grupo'),  new Date(inputElement.getAttribute('data-time')).getTime());
		};
		reader.readAsDataURL(file);
	} else if(/gif|jpg|jpeg|png|svg|webp|bmp|ico/gm.test(extension)) {

		var reader = new FileReader();
		reader.onload = (e) => {
			timelineAddImageItem(e.target.result, inputElement.getAttribute('data-grupo'), new Date(inputElement.getAttribute('data-time')).getTime());
		};
		reader.readAsDataURL(file);
	}
}

function stopAudioAnalyzer() {
    if(audioAnalyzer) {
        audioAnalyzer.stop();
        audioAnalyzer = null;
    }

}
function startAudioAnalyzer() 	{
	//audioContext = new AudioContext();
	if(!audioSource) audioSource = audioContext.createMediaElementSource(htmlAudioElement);
    audioSource.connect(audioContext.destination);
	audioAnalyzer = Meyda.createMeydaAnalyzer({
		"audioContext": audioContext,
		"source": audioSource,
		"bufferSize": preferences.AUDIO_BUFFER_SIZE,
        "windowingFunction": preferences.AUDIO_WINDOWING_FUNCTION,
		"featureExtractors": preferences.ACTIVE_AUDIO_FEATURES, //["rms", "energy"], //, "spectralSlope", "spectralCentroid", "spectralRolloff"],
		"callback": features => {
		    if(features.zcr) features.zcr/=(preferences.AUDIO_BUFFER_SIZE/2)-1;
		    if(features.energy) features.energy/=preferences.AUDIO_BUFFER_SIZE;
		    if(features.spectralCentroid) features.spectralCentroid/=preferences.AUDIO_BUFFER_SIZE/2;
		    if(features.spectralSpread) features.spectralSpread/=preferences.AUDIO_BUFFER_SIZE/2;
		    if(features.loudness) features.loudness=features.loudness.total/24.; // TODO permitir acceder a los especificos y ver poque puede llegar mas alla de 24
            if(features.spectralRolloff) features.spectralRolloff/=44100/2;      //44100Hz es el sample rate por default
            liveAudioFeatures = features;
		}
	});
	audioAnalyzer.start();
	htmlAudioElement.addEventListener("ended", () =>{
		audioAnalyzer.stop();
	});
}

function des_activateAudioFeature(feature, activar) {
    if(activar){
        preferences.ACTIVE_AUDIO_FEATURES.push(feature);
    }else{
        Object.entries(preferences.ACTIVE_AUDIO_FEATURES).forEach(([key, val]) => {
            if(val == feature){
                preferences.ACTIVE_AUDIO_FEATURES.splice(key,1);
            }
        });
    }
}

function updateVideosPosition() {
	itemsTimeline.get().forEach( (itemx) => {
		if(itemx.func == "texture"){
			video = variables_a_inyectar['textures'][itemx.group];
			console.log( itemx.start_ms, timeTimeline, (timeTimeline - itemx.start_ms) / 1000, (timeTimeline - itemx.start_ms))
			if(video && video.pause) {
				video.pause();
				video.currentTime = (timeTimeline - itemx.start_ms) / 1000;
				if (video.currentTime > 2 && timelinePlaying){
					video.play();
					video.currentTime = (timeTimeline - itemx.start_ms) / 1000;
				}
			}
		}
	});
}



function actualizarLog(){
	diverrores = document.getElementById('logErrores');
	diverrores.innerHTML=errorLine;
}
setInterval(actualizarLog, 150);


function getGLSLCode(){

}

function getISFCode(){

}

function setBPM(event){
	BPM = event.target.value;
	itemsTimeline.get().forEach( (itemx) => {
		if(itemx.func == "texture"){
			video = variables_a_inyectar['textures'][itemx.group];
			console.log( itemx.start_ms, timeTimeline)
			video.playbackRate = BPM / 60.;
			video.currentTime = (timeTimeline - itemx.start_ms)/1000;
			if(video.currentTime > 2 && timelinePlaying) video.play();
		}
	});
}

var sliderSmoothing = null;

function startMovingSliderSmoothly(name, event){
	sliderSmoothing = event.pageX;
}
function finishMovingSliderSmoothly(){
	sliderSmoothing = null;
}

function dragSliderSmoothly(elemento, event){
	if(sliderSmoothing){
		console.log(elemento, parseFloat(elemento.value),  ((event.pageX - sliderSmoothing)/100 ))
		//if((event.pageX - sliderSmoothing) < 0)
			elemento.value = parseFloat(elemento.value)*(1+((event.pageX - sliderSmoothing)/50. ));
	}
}

function addBotonesToToolbox(){
	let botoneraPlay = document.createElement('div');
	botoneraPlay.id="botoneraPlay"
	let botonPlay = document.createElement('img');
	botonPlay.src="icons/play.png"
	botonPlay.onclick=timelinePlay
	botonPlay.id="timelinePlayButton"
	botonPlay.style.height="20px"
	botonPlay.style.margin="4px"
	let botonPrevious = document.createElement('img');
	botonPrevious.src="icons/previous.png"
	botonPrevious.onclick=timelineGoToStart
	botonPrevious.id="timelineGoToStartButton"
	botonPrevious.style.height="20px"
	botonPrevious.style.margin="5px"
	let bpmInput = document.createElement('input');
	bpmInput.type="number"
	bpmInput.oninput=setBPM
	bpmInput.value=BPM
	bpmInput.title="BPM"
	bpmInput.id="bpmInput"
	bpmInput.style.height="20px"
	bpmInput.style.width="60px"
	bpmInput.style.margin="4px"
	botoneraPlay.appendChild(botonPrevious)
	botoneraPlay.appendChild(botonPlay)
	botoneraPlay.appendChild(bpmInput)
	document.getElementsByClassName('blocklyToolboxDiv')[0].appendChild(botoneraPlay);
}



function alertaso(){
	alert(Blockly.JavaScript.workspaceToCode(workspacePlayground));
}



function ajustar_ancho(){
	var anchura=Math.max(
		document.documentElement["clientWidth"],
		document.body["scrollWidth"],
		document.documentElement["scrollWidth"],
		document.body["offsetWidth"],
		document.documentElement["offsetWidth"]
	);
	var altura=Math.max(
		document.documentElement["clientHeight"],
		document.body["scrollHeight"],
		document.documentElement["scrollHeight"],
		document.body["offsetHeight"],
		document.documentElement["offsetHeight"]
	);
	altura=altura-236;
	anchura=anchura-220;
	blocklyDiv.style.height = altura+ 'px';
	blocklyArea.style.height = altura+ 'px';
	blocklyDiv.style.width = anchura+ 'px';
	blocklyArea.style.width = anchura+ 'px';
}

//setTimeout(ajustar_ancho, 1000);

function zoomWorkspace(e) {
	console.log(e);
	if(e.deltaY > 0){
		workspacePlayground.setScale(workspacePlayground.scale*=0.9);
		//document.getElementsByClassName('blocklyMainBackground')[0].style['fill'] = null;
		//document.getElementsByClassName('blocklyMainBackground')[0].style['fill-opacity'] =Math.max(parseFloat(document.getElementsByClassName('blocklyMainBackground')[0].style['fill-opacity']-0.05),0);
	}else{
		workspacePlayground.setScale(workspacePlayground.scale*=1.1);
		//document.getElementsByClassName('blocklyMainBackground')[0].style['fill'] = null;
		//document.getElementsByClassName('blocklyMainBackground')[0].style['fill-opacity'] =Math.min(parseFloat(document.getElementsByClassName('blocklyMainBackground')[0].style['fill-opacity'])+0.05,1);
	}
}

function updateFPS(){
	document.getElementById('pFPS').innerText= parseInt(fps) ? parseInt(fps)+"FPS" : "";
}

function setQuality(qual){
	quality = parseFloat(qual);
	if(window_sketch) window_sketch.changeQuality(quality);
}



window.addEventListener('beforeunload', (event) => {
	// Cancel the event as stated by the standard.
	stash.set("jsonActual",get_json_proyecto())
	return "aaa";
});

