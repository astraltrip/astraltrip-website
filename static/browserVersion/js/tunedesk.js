var showingAddModuleContextMenu=false;

function createElementFromHTML(htmlString) {
    var div = document.createElement('div');
    div.innerHTML = htmlString.trim();

    // Change this to div.childNodes to support multiple top-level nodes
    return div.firstChild;
}

function updateVariableValue(variableaactualizar, valor){
    console.log(variableaactualizar,valor);
    variables_a_inyectar['tune'][variableaactualizar]=valor;
    document.getElementById('actualValue'+variableaactualizar).innerText = parseFloat(valor).toFixed(4);

    cambiosAInyectarTune=true;
}

function deleteVariableFromTunedesk(variableaeliminar){
    tunedesk.deleteModule(variableaeliminar);
}

function activateEditVariableFromTunedesk(variableaactualizar){
    console.log(variableaactualizar)
    const elem = document.getElementById('tune_'+variableaactualizar+'_label');
    const nuevo_textarea = ' <input type="text" id="tune_'+variableaactualizar+'_newname" value="'+variableaactualizar+'" onblur="editVariableFromTunedesk(\''+variableaactualizar+'\',this.value)">'
    elem.parentNode.replaceChild(createElementFromHTML(nuevo_textarea),elem);
}


function editVariableFromTunedesk(variableaactualizar, nuevonombre){
    if(nuevonombre!=variableaactualizar) {
        const new_conf = tunedesk.getModuleConf(variableaactualizar);
        new_conf.name = nuevonombre;
        console.log(new_conf);
        tunedesk.addModule(new_conf,variableaactualizar);
        cambiosAInyectarTune=true;
    }else {
        const elem = document.getElementById('tune_' + variableaactualizar + '_newname');
        elem.parentNode.replaceChild(createElementFromHTML("<label id='tune_" + variableaactualizar + "_label'  onclick='activateEditVariableFromTunedesk(\"" + variableaactualizar + "\")'>" + variableaactualizar + "</label>"), elem);
    }
}


function showMesaContextMenu() {
    if(showingAddModuleContextMenu)   {
        document.getElementById('addModuleContextMenu').style.display = "none";
    } else{
        document.getElementById('addModuleContextMenu').style.display = "block";
    }
    showingAddModuleContextMenu=!showingAddModuleContextMenu;
}

function Tunedesk(targetDivSelector){
    var modules =[];
    this.modules = modules;

    this.addModule = function addModule(conf, sustituye = null){  // si sustituye, lo inserta despues de ese elemento.
        var name = conf.name;
        var newModule;
        switch (conf.type) {
            case "slider01": newModule=new TD_slider01(conf);break;
            case "slider": newModule=new TD_slider(conf);break;
            case "midi": newModule=new TD_midiMapper(conf);break;
            default: newModule=new TD_slider(conf);break;
        }
        var newElement = createElementFromHTML(newModule.getHTML());
        if(sustituye === null) {
            document.querySelector(targetDivSelector).appendChild(newElement);
        }else{
            document.getElementById('tune_'+sustituye+'_div').after(newElement);
            this.deleteModule(sustituye);

        }
        modules.push(newModule);
        variables_a_inyectar['tune'][name]=conf.value;
        stash.set('loadtunedesk', this.getConf());
        setearProyectoComoGuardado(false);
    };

    this.getConf = function getConf() {
        var res = [];
        modules.forEach(function (v,i) {
            res.push(v.getConf());
        });
        return JSON.stringify(res);
    };


    this.getModuleConf = function getModuleConf(moduleName) {
        var res = [];
        modules.forEach(function (v,i) {
            const coonf = v.getConf();
            if(coonf.name == moduleName) {
                res.push(coonf);
            }
        } );
        return res[0];
    };

    this.loadConf = function loadConf(json) {
        var nuevos = JSON.parse(json);
        tunedesk.clear();
        nuevos.forEach(function (modulo) {
            tunedesk.addModule(modulo);
        });
    };

    this.deleteModule = function deleteModule(moduleName) {
        delete variables_a_inyectar['tune'][moduleName];
        modules.forEach(function (v,i) {
            const coonf = v.getConf();
            if(coonf.name == moduleName) {
                delete modules[i];
            }
        } );
        const elem = document.getElementById('tune_'+moduleName+'_div');
        elem.parentNode.removeChild(elem);
        cambiosAInyectarTune=true;
    };

    this.clear = function clear(){
        document.querySelector(targetDivSelector).innerHTML = "";
        stash.set('loadtunedesk', '');
        variables_a_inyectar['tune'] = {};
        while(modules.pop()){}
    };
}