

// initialize tunedesk
const tunedesk = new Tunedesk('#mesaProyecto');
var leftOffsetblockFactoryModal = 200;
var topOffsetblockFactoryModal = 200;
var heightblockFactoryModal = 600;
var widthblockFactoryModal = 900;
const htmlAudioElement = document.getElementById("audio");


var blocklyFactory;
var init = function() {
    blocklyFactory = new AppController();
    blocklyFactory.init();
};
window.addEventListener('load', init);



var separadorH = document.getElementById('separadorHorizontal').addEventListener('drag',function (event) {
    if(event.clientX>0 && event.clientY>0) {
        blocklyAltura = event.clientY;
        //blocklyAnchura = event.clientX;
        blocklyAnchura = window.innerWidth-300;
        resizeInterfaz();
    }
});
/*

function resizeTimeline(event) {
    console.log("EEEND", event)
    if (event.clientY > 0) {
        console.log("EEEND", event)
        blocklyAltura = event.clientY;
        //blocklyAnchura = event.clientX;
        blocklyAnchura = window.innerWidth - 300;
        resizeInterfaz();
    }

}

var separadorH = document.getElementById('separadorHorizontal').addEventListener('dragstart',function (event) {

    if(event.clientX>0 && event.clientY>0) {
        console.log("STAAART", event)
        blocklyAltura = event.clientY;
        //blocklyAnchura = event.clientX;
        blocklyAnchura = window.innerWidth - 300;
        resizeInterfaz();
        document.addEventListener('mousemove', resizeTimeline);

    }
});

var separadorH = document.getElementById('separadorHorizontal').addEventListener('dragend',function (event) {
    console.log("EEENDdrag", event)

    document.removeEventListener('mousemove', resizeTimeline);

});


var separadorH = document.getElementById('separadorHorizontal').addEventListener('drop',function (event) {
    console.log("drop", event)
    if(event.clientX>0 && event.clientY>0) {
        console.log("EEEND", event)
        blocklyAltura = event.clientY;
        //blocklyAnchura = event.clientX;
        blocklyAnchura = window.innerWidth-300;
        resizeInterfaz();
    }
});*/

$(document).ready(function() {
    var ctrlDown = false,
        ctrlKey = 17,
        cmdKey = 91,
        vKey = 86,
        space = 32,
        cKey = 67;

    $(document).keydown(function(e) {
        if (e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = true;
    }).keyup(function(e) {
        if (e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = false;
    });

    $(document).keydown(function(e) {
        if (e.keyCode === space) timelinePlay();
    });

    $(".no-copy-paste").keydown(function(e) {
        if (ctrlDown && (e.keyCode == vKey || e.keyCode == cKey)) return false;
    });

    // Document Ctrl + C/V
    $(document).keydown(function(e) {
        if (ctrlDown && (e.keyCode == cKey)) {
            console.log("Document catch Ctrl+C");
            const seleccionados = timeline.getSelection();
            if(seleccionados){
                Blockly.clipboardSource_ = null;
                clipboardTimeline = [];
                seleccionados.forEach(function (sel) {
                    clipboardTimeline.push(itemsTimeline.get(sel));
                });
            }
        }
        if (ctrlDown && (e.keyCode == vKey)){
            console.log("Document catch Ctrl+V");
            const nuevosIds = [];
            clipboardTimeline.forEach(function (item) {
                const itemname = nextItemId;
                nextItemId++;
                item.id= itemname;
                item.name=itemname;
                item.content = createTemplateAutomationTimeline(itemname,item.group);
                curves[itemname] = eval('new Bezier'+item.curve);
                drawCurve(itemname);
                itemsTimeline.add(item);
                configItemTimeline(itemname);
                nuevosIds.push(itemname);
            });
            timeline.setSelection(nuevosIds);
        }
    });
});


setTimeout(function() {
    //blocklyAnchura = document.documentElement.clientWidth * 0.9;
    blocklyAltura = document.documentElement.clientHeight * 0.7;
    resizeInterfaz();
},300);
setInterval(updateFPS,100);

document.getElementById('agrandadorModalFactory').addEventListener('drag',function (event) {
    if (event.clientX > 0 && event.clientY > 0) {
        console.log(event.clientX);
        widthblockFactoryModal = event.clientX - leftOffsetblockFactoryModal;
        heightblockFactoryModal = event.clientY - topOffsetblockFactoryModal;
        document.getElementById('blockFactoryModal').style.width = widthblockFactoryModal + "px";
        document.getElementById('blockFactoryModal').style.height = heightblockFactoryModal + "px";
        document.getElementById('agrandadorModalFactory').style.top = heightblockFactoryModal-15 + "px";
        document.getElementById('agrandadorModalFactory').style.left = widthblockFactoryModal + "px";
        document.getElementById('movedorModalFactory').style["margin-left"] = "-"+widthblockFactoryModal + "px";

        document.getElementById('saveToBlockLibraryButton').style["left"] = (widthblockFactoryModal - 150)+ "px";
        document.getElementById('saveToBlockLibraryButton').style["top"] = (heightblockFactoryModal -60) + "px";
        //window.dispatchEvent(new Event('resize'));
    }
});
document.getElementById('movedorModalFactory').addEventListener('drag',function (event) {
    if(event.clientX>0 && event.clientY>0) {
        leftOffsetblockFactoryModal = event.clientX;
        topOffsetblockFactoryModal = event.clientY;
        document.getElementById('blockFactoryModal').style.left = leftOffsetblockFactoryModal+"px";
        document.getElementById('blockFactoryModal').style.top = topOffsetblockFactoryModal+"px";
    }
});
initMainWorkspaceBlockly();

window.onload=() => {
    console.log("DDDDDDDDDDDDDDDDDDDDDD")
    setTimeout(resizeInterfaz,800);
    if( stash.get("preferences")) {
        preferences = stash.get("preferences");
    }
    imgblackpixel = document.getElementById('blackPixel');
};

appendXMLtoolboxes();
addBotonesToToolbox();

restoreSession();
if(previewMode) {
    previewMode=false;
    tooglePreviewMode();
}