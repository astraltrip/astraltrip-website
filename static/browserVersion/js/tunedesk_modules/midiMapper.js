function TD_midiMapper(conf) {
    let midiControl = 0;
    this.getHTML = (function (){
        var name = conf.name;
        var value = conf.value;
        var res = "<div id='tune_"+name+"_div'>";
        res+="<label for='tune_"+name+"'>"+name+"</label><br>";
        res+='<input value="'+conf.min+'" id="tune_'+name+'_min" class="inputNumber" oninput="document.getElementById(\'tune_'+name+'\').min=this.value">';
        res+="<input style='width:110px; display: inline-block;' type='text' id='tune_"+name+"' value='"+value+"' oninput='updateVariableValue(\""+name+"\",this.value)'>";
        res+="<img src='icons/refresh-line.png' alt='Learn midi' onclick='' >"; /// TODO
        res+='<input value="'+conf.max+'" id="tune_'+name+'_max" class="inputNumber" style="display: inline-block;" oninput="document.getElementById(\'tune_'+name+'\').max=this.value">';
        res+="</div>";
        return res;
    });

    this.getVariableValue = function (){
        stash.get("tune_"+conf.name);
    };

    this.getConf = (function (){
        var v = document.getElementById("tune_"+conf.name).value;
        return {
            name: conf.name,
            type: "slider01",
            value: v
        };
    });

    this.loadConf = (function (nuevaConf){
        var element = document.getElementById("tune_"+conf.name);
        element.value = nuevaConf.value;
        element.dispatchEvent(new Event('input'));
    });
}