function TD_slider(conf) {
    this.getHTML = function (){
        var name = conf.name;
        var value = conf.value;
        var res = "<div id='tune_"+name+"_div' class='tune_div'>";
        res+="<a onclick='deleteVariableFromTunedesk(\""+name+"\")'>x</a><label class='tune_label' id='tune_"+name+"_label' ondblclick='activateEditVariableFromTunedesk(\""+name+"\")'>" +name + "</label>";
        res+='<input value="'+conf.min+'" id="tune_'+name+'_min" class="inputNumber" oninput="document.getElementById(\'tune_'+name+'\').min=this.value">';
        res+="<div class='tuneSliderWrapper'>";
        res+="<input class='tune_slider' type='range' min='"+conf.min+"' max='"+conf.max+"' step='any' id='tune_"+name+"' value='"+value+"' oninput='updateVariableValue(\""+name+"\",this.value)' oncontextmenu='startMovingSliderSmoothly(\""+name+"\",event)' onmousemove='dragSliderSmoothly(this,event)' onmouseup='finishMovingSliderSmoothly()' onmouseout='finishMovingSliderSmoothly()'>";
        res+="<p class='valueSlider' id='actualValue"+name+"'>0.2000</p>";
        res+="</div>";
        res+='<input value="'+conf.max+'" id="tune_'+name+'_max" class="inputNumber" style="display: inline-block;" oninput="document.getElementById(\'tune_'+name+'\').max=this.value">';
        res+=' <img src="icons/midi.png" id="midi_button'+name+'" alt="preferences"  onclick="learnMidi(\''+name+'\')" oncontextmenu="unlearnMidi(\''+name+'\'); return false" >';      //  return false Para que no muestre el menu contextual
        res+="</div>";
        return res;
    };

    this.getConf = (function (){
        console.log(conf);
        var v = document.getElementById("tune_"+conf.name).value;
        var min = document.getElementById("tune_"+conf.name+"_min").value;
        var max = document.getElementById("tune_"+conf.name+"_max").value;
        return {
            name: conf.name,
            type: "slider",
            min: min,
            max: max,
            value: v
        };
    });

    this.loadConf = (function (nuevaConf){
        var element = document.getElementById("tune_"+conf.name);
        var elementmin = document.getElementById("tune_"+conf.name+"_min");
        var elementmax = document.getElementById("tune_"+conf.name+"_max");
        element.value = nuevaConf.value;
        elementmin.value = nuevaConf.min;
        elementmax.value = nuevaConf.max;
        element.dispatchEvent(new Event('input'));
        elementmin.dispatchEvent(new Event('input'));
        elementmax.dispatchEvent(new Event('input'));
    });
}