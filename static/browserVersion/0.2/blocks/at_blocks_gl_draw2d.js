// http://www.iquilezles.org/www/articles/distfunctions2d/distfunctions2d.htm

Blockly.JavaScript['sdsegment2d'] = function(block) {
    var value_p = Blockly.JavaScript.valueToCode(block, 'p', Blockly.JavaScript.ORDER_ATOMIC);
    var value_a = Blockly.JavaScript.valueToCode(block, 'a', Blockly.JavaScript.ORDER_ATOMIC);
    var value_b = Blockly.JavaScript.valueToCode(block, 'b', Blockly.JavaScript.ORDER_ATOMIC);

    return[Blockly.JavaScript.provideFunction_("sdSegment2D",["float sdSegment2D(vec2 p, vec2 a, vec2 b) {","  vec2 pa = p-a, ba = b-a;","  float h = clamp( dot(pa,ba)/dot(ba,ba), 0.0, 1.0 );","  return length( pa - ba*h );","  }"])+"("+value_p+","+value_a+","+value_b+")",Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.Blocks['sdsegment2d'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"sdsegment2d\",\n  \"message0\": \"sdSegment p: %1 a: %2 b: %3\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"p\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"a\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"b\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['sdcircle2d'] = function(block) {
    var value_p = Blockly.JavaScript.valueToCode(block, 'p', Blockly.JavaScript.ORDER_ATOMIC);
    var value_r = Blockly.JavaScript.valueToCode(block, 'r', Blockly.JavaScript.ORDER_ATOMIC);

    return[Blockly.JavaScript.provideFunction_("sdCircle2D",["float sdCircle2D(vec2 p, float r) {"," return length(p) - r;","  }"])+"("+value_p+","+value_r+")",Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.Blocks['sdcircle2d'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"sdcircle2d\",\n  \"message0\": \"sdCircle2D p: %1 r: %2\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"p\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"r\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['sdbox2d'] = function(block) {
    var value_p = Blockly.JavaScript.valueToCode(block, 'p', Blockly.JavaScript.ORDER_ATOMIC);
    var value_b = Blockly.JavaScript.valueToCode(block, 'b', Blockly.JavaScript.ORDER_ATOMIC);

    return[Blockly.JavaScript.provideFunction_("sdBox2D",["float sdBox2D(vec2 p, vec2 b) {","  vec2 d = abs(p)-b;","  return length(max(d,0.0)) + min(max(d.x,d.y),0.0);","  }"])+"("+value_p+","+value_b+")",Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.Blocks['sdbox2d'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"sdbox2d\",\n  \"message0\": \"sdBox2D p: %1 b: %2\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"p\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"b\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['sdpentagon2d'] = function(block) {
    var value_p = Blockly.JavaScript.valueToCode(block, 'p', Blockly.JavaScript.ORDER_ATOMIC);
    var value_r = Blockly.JavaScript.valueToCode(block, 'r', Blockly.JavaScript.ORDER_ATOMIC);

    return[Blockly.JavaScript.provideFunction_("sdPentagon2D",["float sdPentagon2D(vec2 p, float r) {","    const vec3 k = vec3(0.809016994,0.587785252,0.726542528);    p.x = abs(p.x);    p -= 2.0*min(dot(vec2(-k.x,k.y),p),0.0)*vec2(-k.x,k.y);    p -= 2.0*min(dot(vec2( k.x,k.y),p),0.0)*vec2( k.x,k.y);    p -= vec2(clamp(p.x,-r*k.z,r*k.z),r);        return length(p)*sign(p.y);","  }"])+"("+value_p+","+value_r+")",Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.Blocks['sdpentagon2d'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"sdpentagon2d\",\n  \"message0\": \"sdPentagon2D p: %1 r: %2\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"p\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"r\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['sdhexagon2d'] = function(block) {
    var value_p = Blockly.JavaScript.valueToCode(block, 'p', Blockly.JavaScript.ORDER_ATOMIC);
    var value_r = Blockly.JavaScript.valueToCode(block, 'r', Blockly.JavaScript.ORDER_ATOMIC);

    return[Blockly.JavaScript.provideFunction_("sdHexagon2D",["float sdHexagon2D(vec2 p, float r) {","     const vec3 k = vec3(-0.866025404,0.5,0.577350269);    p = abs(p);    p -= 2.0*min(dot(k.xy,p),0.0)*k.xy;    p -= vec2(clamp(p.x, -k.z*r, k.z*r), r);    return length(p)*sign(p.y);","  }"])+"("+value_p+","+value_r+")",Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.Blocks['sdhexagon2d'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"sdhexagon2d\",\n  \"message0\": \"sdHexagon2D p: %1 r: %2\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"p\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"r\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['sdoctogon2d'] = function(block) {
    var value_p = Blockly.JavaScript.valueToCode(block, 'p', Blockly.JavaScript.ORDER_ATOMIC);
    var value_r = Blockly.JavaScript.valueToCode(block, 'r', Blockly.JavaScript.ORDER_ATOMIC);

    return[Blockly.JavaScript.provideFunction_("sdOctogon2D",["float sdOctogon2D(vec2 p, float r) {","       const vec3 k = vec3(-0.9238795325, 0.3826834323, 0.4142135623 );    p = abs(p);    p -= 2.0*min(dot(vec2( k.x,k.y),p),0.0)*vec2( k.x,k.y);    p -= 2.0*min(dot(vec2(-k.x,k.y),p),0.0)*vec2(-k.x,k.y);    p -= vec2(clamp(p.x, -k.z*r, k.z*r), r);    return length(p)*sign(p.y);","  }"])+"("+value_p+","+value_r+")",Blockly.JavaScript.ORDER_FUNCTION_CALL];
    return[Blockly.JavaScript.provideFunction_("sdOctogon2D",["float sdOctogon2D(vec2 p, float r) {","       const vec3 k = vec3(-0.9238795325, 0.3826834323, 0.4142135623 );    p = abs(p);    p -= 2.0*min(dot(vec2( k.x,k.y),p),0.0)*vec2( k.x,k.y);    p -= 2.0*min(dot(vec2(-k.x,k.y),p),0.0)*vec2(-k.x,k.y);    p -= vec2(clamp(p.x, -k.z*r, k.z*r), r);    return length(p)*sign(p.y);javasc","  }"])+"("+value_p+","+value_r+")",Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.Blocks['sdoctogon2d'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"sdoctogon2d\",\n  \"message0\": \"sdOctogon2D p: %1 r: %2\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"p\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"r\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['sdrhombus2d'] = function(block) {
    var value_p = Blockly.JavaScript.valueToCode(block, 'p', Blockly.JavaScript.ORDER_ATOMIC);
    var value_b = Blockly.JavaScript.valueToCode(block, 'b', Blockly.JavaScript.ORDER_ATOMIC);

    return[Blockly.JavaScript.provideFunction_("sdRhombus2D",["float sdRhombus2D(vec2 p, vec2 b) {","  vec2 q = abs(p);","  float h = clamp((-2.0*ndot(q,b)+ndot(b,b))/dot(b,b),-1.0,1.0);","  float d = length( q - 0.5*b*vec2(1.0-h,1.0+h) );","  return d * sign( q.x*b.y + q.y*b.x - b.x*b.y );","  }"])+"("+value_p+","+value_b+")",Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.Blocks['sdrhombus2d'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"sdrhombus2d\",\n  \"message0\": \"sdRhombus2D p: %1 b: %2\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"p\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"b\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['sdequitriangle2d'] = function(block) {
    var value_p = Blockly.JavaScript.valueToCode(block, 'p', Blockly.JavaScript.ORDER_ATOMIC);

    return[Blockly.JavaScript.provideFunction_("sdEquiTriangle2D",["float sdEquiTriangle2D(vec2 p) {","  const float k = sqrt(3.0);    p.x = abs(p.x) - 1.0;    p.y = p.y + 1.0/k;   if( p.x+k*p.y>0.0 ) p = vec2(p.x-k*p.y,-k*p.x-p.y)/2.0;   p.x -= clamp( p.x, -2.0, 0.0 );    return -length(p)*sign(p.y);","  }"])+"("+value_p+")",Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.Blocks['sdequitriangle2d'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"sdequitriangle2d\",\n  \"message0\": \"sdEquilateralTriangle2D p: %1\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"p\"\n    }  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['sdisotriangle2d'] = function(block) {
    var value_p = Blockly.JavaScript.valueToCode(block, 'p', Blockly.JavaScript.ORDER_ATOMIC);
    var value_b = Blockly.JavaScript.valueToCode(block, 'b', Blockly.JavaScript.ORDER_ATOMIC);

    return[Blockly.JavaScript.provideFunction_("sdIsoTriangle2D",["float sdIsoTriangle2D(vec2 p, vec2 q) {","  p.x = abs(p.x);    vec2 a = p - q*clamp( dot(p,q)/dot(q,q), 0.0, 1.0 );    vec2 b = p - q*vec2( clamp( p.x/q.x, 0.0, 1.0 ), 1.0 );    float s = -sign( q.y );    vec2 d = min( vec2( dot(a,a), s*(p.x*q.y-p.y*q.x) ),                  vec2( dot(b,b), s*(p.y-q.y)  ));    return -sqrt(d.x)*sign(d.y);","  }"])+"("+value_p+","+value_b+")",Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.Blocks['sdisotriangle2d'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"sdisotriangle2d\",\n  \"message0\": \"sdIsoscelesTriangle2D p: %1 b: %2\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"p\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"b\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

/*
Blockly.Blocks['sdsegment'] = {

    init: function() {
        this.jsonInit(JSON.parse());
    }
};

 */