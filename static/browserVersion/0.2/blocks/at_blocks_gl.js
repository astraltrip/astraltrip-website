Blockly.JavaScript['glmain'] = function(block) {
    var statements_main = Blockly.JavaScript.statementToCode(block, 'main');
    //Blockly.JavaScript.provideFunction_("headerCosas",["precision mediump float;const float PI = 3.141592654;uniform float time;uniform vec2 mouse;uniform vec2 resolution;"]);
    var code = 'void main(){\n'+statements_main+'\n} \n';     return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.Blocks['glmain'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"glmain\",\n  \"message0\": \"main %1\",\n  \"args0\": [\n    {\n      \"type\": \"input_statement\",\n      \"name\": \"main\"\n    }\n  ],\n  \"colour\": 195,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['vec4'] = function(block) {
    var value_r = Blockly.JavaScript.valueToCode(block, 'r', Blockly.JavaScript.ORDER_ATOMIC);
    var value_g = Blockly.JavaScript.valueToCode(block, 'g', Blockly.JavaScript.ORDER_ATOMIC);
    var value_b = Blockly.JavaScript.valueToCode(block, 'b', Blockly.JavaScript.ORDER_ATOMIC);
    var value_a = Blockly.JavaScript.valueToCode(block, 'a', Blockly.JavaScript.ORDER_ATOMIC);

    var code = 'vec4('+value_r;
    if(value_g) {
        code +=',' + value_g;
    }
    if(value_b) {
        code +=',' + value_b;
    }
    if(value_a) {
        code +=',' + value_a;
    }
    code+=')';


    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['vec4'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"vec4\",\n  \"message0\": \"%1 %2 %3 %4\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"r\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"g\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"b\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"a\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 60,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};


Blockly.JavaScript['vec3'] = function(block) {
    var value_r = Blockly.JavaScript.valueToCode(block, 'r', Blockly.JavaScript.ORDER_ATOMIC);
    var value_g = Blockly.JavaScript.valueToCode(block, 'g', Blockly.JavaScript.ORDER_ATOMIC);
    var value_b = Blockly.JavaScript.valueToCode(block, 'b', Blockly.JavaScript.ORDER_ATOMIC);

    var code = 'vec3('+value_r;
    if(value_g) {
        code +=',' + value_g;
    }
    if(value_b) {
        code +=',' + value_b;
    }
    code+=')';

    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['vec3'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"vec3\",\n  \"message0\": \"%1 %2 %3\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"r\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"g\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"b\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 60,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['vec2'] = function(block) {
    var value_x = Blockly.JavaScript.valueToCode(block, 'x', Blockly.JavaScript.ORDER_ATOMIC);
    var value_y = Blockly.JavaScript.valueToCode(block, 'y', Blockly.JavaScript.ORDER_ATOMIC);

    var code = 'vec2('+value_x;
    if(value_y) {
        code +=',' + value_y;
    }
    code+=')';

    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['vec2'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"vec2\",\n  \"message0\": \"%1 %2\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"x\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"y\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 60,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};


Blockly.JavaScript['glfloat'] = function(block) {

    code = parseFloat(block.getFieldValue('num'));
    if(code%1===0) code +=".";


    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['glfloat'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"glfloat\",\n  \"message0\": \"%1\",\n  \"args0\": [\n    {\n      \"type\": \"field_input\",\n      \"name\": \"num\",\n      \"text\": \"0.0\"\n    }\n  ],\n  \"output\": \"Number\",\n  \"colour\": 60,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};



Blockly.JavaScript['glint'] = function(block) {
    code = parseInt(block.getFieldValue('num'));

    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['glint'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"glint\",\n  \"message0\": \"%1\",\n  \"args0\": [\n    {\n      \"type\": \"field_input\",\n      \"name\": \"num\",\n      \"text\": \"0\"\n    }\n  ],\n  \"output\": \"Number\",\n  \"colour\": 60,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};




Blockly.JavaScript['checkbox'] = function(block) {
    code = block.getFieldValue('NAME').toLowerCase();

    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['checkbox'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"checkbox\",\n  \"message0\": \"%1\",\n  \"args0\": [\n    {\n      \"type\": \"field_checkbox\",\n      \"name\": \"NAME\",\n      \"checked\": true\n    }\n  ],\n  \"output\": null,\n  \"colour\": 60,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};



Blockly.JavaScript['mat4'] = function(block) {
    var value_r = Blockly.JavaScript.valueToCode(block, 'vec1', Blockly.JavaScript.ORDER_ATOMIC);
    var value_g = Blockly.JavaScript.valueToCode(block, 'vec2', Blockly.JavaScript.ORDER_ATOMIC);
    var value_b = Blockly.JavaScript.valueToCode(block, 'vec3', Blockly.JavaScript.ORDER_ATOMIC);
    var value_a = Blockly.JavaScript.valueToCode(block, 'vec4', Blockly.JavaScript.ORDER_ATOMIC);

    var code = 'mat4('+value_r+'.x,'+value_r+'.y,'+value_r+'.z,'+value_r+'.w';
    if(value_g) {
        code +=',' +value_g+'.x,'+value_g+'.y,'+value_g+'.z,'+value_g+'.w';
    }
    if(value_b) {
        code +=',' + value_b+'.x,'+value_b+'.y,'+value_b+'.z,'+value_b+'.w';
    }
    if(value_a) {
        code +=',' + value_a+'.x,'+value_a+'.y,'+value_a+'.z,'+value_a+'.w';
    }
    code+=')';


    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['mat4'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"mat4\",\n  \"message0\": \"mat4 %1 %2 %3 %4\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"vec1\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"vec2\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"vec3\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"vec4\"\n    }\n  ],\n  \"inputsInline\": false,\n  \"output\": null,\n  \"colour\": 60,\n  \"tooltip\": \"Converts 4 vec4 into one mat4\",\n  \"helpUrl\": \"\"\n}"));
    }
};



Blockly.JavaScript['mat3'] = function(block) {
    var value_r = Blockly.JavaScript.valueToCode(block, 'vec1', Blockly.JavaScript.ORDER_ATOMIC);
    var value_g = Blockly.JavaScript.valueToCode(block, 'vec2', Blockly.JavaScript.ORDER_ATOMIC);
    var value_b = Blockly.JavaScript.valueToCode(block, 'vec3', Blockly.JavaScript.ORDER_ATOMIC);

    var code = 'mat3('+value_r+'.x,'+value_r+'.y,'+value_r+'.z';
    if(value_g) {
        code +=',' +value_g+'.x,'+value_g+'.y,'+value_g+'.z';
    }
    if(value_b) {
        code +=',' + value_b+'.x,'+value_b+'.y,'+value_b+'.z';
    }
    code+=')';


    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['mat3'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"mat3\",\n  \"message0\": \"mat3 %1 %2 %3\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"vec1\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"vec2\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"vec3\"\n    }\n  ],\n  \"inputsInline\": false,\n  \"output\": null,\n  \"colour\": 60,\n  \"tooltip\": \"Converts 3 vec3 into one mat3\",\n  \"helpUrl\": \"\"\n}"));
    }
};



Blockly.JavaScript['mat2'] = function(block) {
    var value_r = Blockly.JavaScript.valueToCode(block, 'vec1', Blockly.JavaScript.ORDER_ATOMIC);
    var value_g = Blockly.JavaScript.valueToCode(block, 'vec2', Blockly.JavaScript.ORDER_ATOMIC);

    var code = 'mat2('+value_r+'.x,'+value_r+'.y';
    if(value_g) {
        code += ',' + value_g + '.x,' + value_g + '.y';
    }
    code+=')';

    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['mat2'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"mat2\",\n \"message0\": \"mat2 %1 %2\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"vec1\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"vec2\"\n    }\n  ],\n  \"inputsInline\": false,\n  \"output\": null,\n  \"colour\": 60,\n  \"tooltip\": \"Converts 2 vec2 into one mat2\",\n  \"helpUrl\": \"\"\n}"));
    }
};




Blockly.JavaScript['gl_matrix_access'] = function(block) {
    var value_matrix = Blockly.JavaScript.valueToCode(block, 'matrix', Blockly.JavaScript.ORDER_ATOMIC);
    var value_x = Blockly.JavaScript.valueToCode(block, 'x', Blockly.JavaScript.ORDER_ATOMIC);
    var value_y = Blockly.JavaScript.valueToCode(block, 'y', Blockly.JavaScript.ORDER_ATOMIC);

    var code = value_matrix + '['+value_x+']';
    if(value_y) {
        code += '[' + value_y + ']';
    }

    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['gl_matrix_access'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"gl_matrix_access\",\n  \"message0\": \"matix %1 [ %2 %3  ][ %4 %5 ]\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"matrix\"\n    },\n    {\n      \"type\": \"input_dummy\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"x\"\n    },\n    {\n      \"type\": \"input_dummy\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"y\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 60,\n  \"tooltip\": \"Get one element from a matrix. Third parameter is optional\",\n  \"helpUrl\": \"\"\n}"));
    }
};



Blockly.JavaScript['gl_vector_component'] = function(block) {
    var value_vector = Blockly.JavaScript.valueToCode(block, 'vector', Blockly.JavaScript.ORDER_ATOMIC);
    var dropdown_comp = block.getFieldValue('comp');

    var code = value_vector;
    switch (dropdown_comp) {
        case "x": code+='.x';break;
        case "y": code+='.y';break;
        case "z": code+='.z'; break;
        case "w": code+='.w'; break;
        case "xy": code+='.xy'; break;
        case "xyz": code+='.xyz'; break;
    }

    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['gl_vector_component'] = {

    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"gl_vector_component\",\n  \"message0\": \"%1 %2\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"vector\",\n      \"check\": \"vec\"\n    },\n    {\n      \"type\": \"field_dropdown\",\n      \"name\": \"comp\",\n      \"options\": [\n        [\n          \".x\",\n          \"x\"\n        ],\n        [\n          \".y\",\n          \"y\"\n        ],\n        [\n          \".z\",\n          \"z\"\n        ],\n        [\n          \".w\",\n          \"w\"\n        ],\n        [\n          \".xy\",\n          \"xy\"\n        ],\n        [\n          \".xyz\",\n          \"xyz\"\n        ]\n      ]\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 60,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['gl_vardeclare'] = function(block) {
    var variable_var = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('var'), Blockly.Variables.NAME_TYPE);
    var dropdown_type = block.getFieldValue('type');

    var code = dropdown_type+' '+variable_var+';\n';

    return code;
};

Blockly.Blocks['gl_vardeclare'] = {

    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"gl_vardeclare\",\n  \"message0\": \"Declare %1 as %2\",\n  \"args0\": [\n    {\n      \"type\": \"field_variable\",\n      \"name\": \"var\",\n      \"variable\": \"variable\"\n    },\n    {\n      \"type\": \"field_dropdown\",\n      \"name\": \"type\",\n      \"options\": [\n        [\n          \"float\",\n          \"float\"\n        ],\n        [\n          \"int\",\n          \"int\"\n        ],\n        [\n          \"bool\",\n          \"bool\"\n        ],\n        [\n          \"vec2\",\n          \"vec2\"\n        ],\n        [\n          \"vec3\",\n          \"vec3\"\n        ],\n        [\n          \"vec4\",\n          \"vec4\"\n        ]\n  ,\n        [\n          \"mat2\",\n          \"mat2\"\n        ],\n        [\n          \"mat3\",\n          \"mat3\"\n        ],\n        [\n          \"mat4\",\n          \"mat4\"\n        ]\n      ]\n    }\n  ],\n  \"previousStatement\": null,\n  \"nextStatement\": null,\n  \"colour\": 330,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};


Blockly.JavaScript['auto_variable_get'] = function(block) {
    var text_name = block.getFieldValue('NAME');

    var code = text_name;

    // TODO: Change ORDER_NONE to the correct strength.
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['auto_variable_get'] = {

    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"auto_variable_get\",\n  \"message0\": \"%1\",\n  \"args0\": [\n    {\n      \"type\": \"field_input\",\n      \"name\": \"NAME\",\n      \"text\": \"default\"\n    }\n  ],\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['shadercode'] = function(block) {
    var code = block.getCommentText();

    return code;
};

Blockly.Blocks['shadercode'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"shadercode\",\n  \"message0\": \"code %1\",\n  \"args0\": [\n    {\n      \"type\": \"field_input\",\n      \"name\": \"NAME\",\n      \"text\": \"Write code at comments\"\n    }\n  ],\n  \"previousStatement\": null,\n  \"nextStatement\": null,\n  \"colour\": 0,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};


Blockly.JavaScript['shadercode_inline'] = function(block) {
    var code = block.getFieldValue('code');
    return code;
};

Blockly.Blocks['shadercode_inline'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"shadercode_inline\",\n  \"message0\": \"code:  %1\",\n  \"args0\": [\n    {\n      \"type\": \"field_input\",\n      \"name\": \"code\",\n      \"text\": \"\"\n    }\n  ],\n  \"previousStatement\": null,\n  \"nextStatement\": null,\n  \"colour\": 0,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['shadercode_returning'] = function(block) {
    var code = block.getFieldValue('code');
    return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.Blocks['shadercode_returning'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"shadercode_returning\",\n  \"message0\": \"code: %1\",\n  \"args0\": [\n    {\n      \"type\": \"field_input\",\n      \"name\": \"code\",\n      \"text\": \"\"\n    }\n  ],\n  \"output\": null,\n  \"colour\": 0,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['shadercode_multi'] = function(block) {
    const code1 = Blockly.JavaScript.valueToCode(block, 'code1', Blockly.JavaScript.ORDER_ATOMIC);
    const code2 = Blockly.JavaScript.valueToCode(block, 'code2', Blockly.JavaScript.ORDER_ATOMIC);
    const code3 = Blockly.JavaScript.valueToCode(block, 'code3', Blockly.JavaScript.ORDER_ATOMIC);
    let code = "";
    if(code1) code += code1;
    if(code2) code += code2;
    if(code3) code += code3;

    return code;
};

Blockly.Blocks['shadercode_multi'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"shadercode_multi\",\n  \"message0\": \"code: %1 %2 %3\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"code1\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"code2\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"code3\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"previousStatement\": null,\n  \"nextStatement\": null,\n  \"colour\": 0,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['radians_degrees'] = function(block) {
    var name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    var value_type = block.getFieldValue('type');

    var code;
    switch (value_type) {
        case "radians": code='radians('+name+')';break;
        case "degrees": code='degrees('+name+')';break;
    }

    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['radians_degrees'] = {
    init: function() {
        this.jsonInit(JSON.parse( "{\n  \"type\": \"radians_degrees\",\n  \"message0\": \"%1 %2\",\n  \"args0\": [\n    {\n      \"type\": \"field_dropdown\",\n      \"name\": \"type\",\n      \"options\": [\n        [\n          \"to radians\",\n          \"radians\"\n        ],\n        [\n          \"to degrees\",\n          \"degrees\"\n        ]\n      ]\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME\"\n    }\n  ],\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"Converts from degrees to radians or from radians to degrees\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['number_functions_gl'] = function(block) {
    var name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    var value_type = block.getFieldValue('type');

    let code;
    switch (value_type) {
        case "sign": code='sign('+name+')';break;
        case "floor": code='floor('+name+')';break;
        case "ceil": code='ceil('+name+')';break;
        case "fract": code='fract('+name+')';break;
        case "normalize": code='normalize('+name+')';break;
    }

    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['number_functions_gl'] = {
    init: function() {
        this.jsonInit(JSON.parse( "{\n  \"type\": \"number_functions_gl\",\n  \"message0\": \"%1 %2\",\n  \"args0\": [\n    {\n      \"type\": \"field_dropdown\",\n      \"name\": \"type\",\n      \"options\": [\n        [\n          \"floor\",\n          \"floor\"\n        ],\n        [\n          \"ceil\",\n          \"ceil\"\n        ],\n        [\n          \"fract\",\n          \"fract\"\n        ],\n        [\n          \"sign\",\n          \"sign\"\n        ]\n  ,\n        [\n          \"normalize\",\n          \"normalize\"\n        ]\n      ]\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME\"\n    }\n  ],\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"Converts from degrees to radians or from radians to degrees\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['numbers_functions'] = function(block) {
    var name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    var name2 = Blockly.JavaScript.valueToCode(block, 'NAME2', Blockly.JavaScript.ORDER_ATOMIC);
    var value_type = block.getFieldValue('type');

    var code;
    switch (value_type) {
        case "mod": code='mod('+name+','+name2+')';break;
        case "min": code='min('+name+','+name2+')';break;
        case "max": code='max('+name+','+name2+')';break;
        case "step": code='step('+name+','+name2+')';break;
    }

    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['numbers_functions'] = {
    init: function() {
        this.jsonInit(JSON.parse( "{\n  \"type\": \"numbers_functions\",\n  \"message0\": \"%1 %2 %3\",\n  \"args0\": [\n    {\n      \"type\": \"field_dropdown\",\n      \"name\": \"type\",\n      \"options\": [\n        [\n          \"mod\",\n          \"mod\"\n        ],\n        [\n          \"min\",\n          \"min\"\n        ],\n        [\n          \"max\",\n          \"max\"\n        ],\n        [\n          \"step\",\n          \"step\"\n        ]\n      ]\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME2\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"Converts from degrees to radians or from radians to degrees\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['multi_number_functions'] = function(block) {
    var name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    var name2 = Blockly.JavaScript.valueToCode(block, 'NAME2', Blockly.JavaScript.ORDER_ATOMIC);
    var name3 = Blockly.JavaScript.valueToCode(block, 'NAME3', Blockly.JavaScript.ORDER_ATOMIC);
    var value_type = block.getFieldValue('type');


    var code;
    switch (value_type) {
        case "clamp": code='clamp('+name+','+name2+','+name3+')';break;
        case "mix": code='mix('+name+','+name2+','+name3+')';break;
        case "smoothstep": code='smoothstep('+name+','+name2+','+name3+')';break;
    }

    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['multi_number_functions'] = {
    init: function() {
        this.jsonInit(JSON.parse( "{\n  \"type\": \"multi_number_functions\",\n  \"message0\": \"%1 %2 %3 %4\",\n  \"args0\": [\n    {\n      \"type\": \"field_dropdown\",\n      \"name\": \"type\",\n      \"options\": [\n        [\n          \"clamp\",\n          \"clamp\"\n        ],\n        [\n          \"mix\",\n          \"mix\"\n        ],\n        [\n          \"smoothstep\",\n          \"smoothstep\"\n        ]\n      ]\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME2\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME3\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"Converts from degrees to radians or from radians to degrees\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['geometric_multifunctions'] = function(block) {
    var name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    var name2 = Blockly.JavaScript.valueToCode(block, 'NAME2', Blockly.JavaScript.ORDER_ATOMIC);
    var name3 = Blockly.JavaScript.valueToCode(block, 'NAME3', Blockly.JavaScript.ORDER_ATOMIC);
    var value_type = block.getFieldValue('type');


    var code;
    switch (value_type) {
        case "refract": code='refract('+name+','+name2+','+name3+')';break;
        case "faceforward": code='faceforward('+name+','+name2+','+name3+')';break;
    }

    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['geometric_multifunctions'] = {
    init: function() {
        this.jsonInit(JSON.parse( "{\n  \"type\": \"geometric_multifunctions\",\n  \"message0\": \"%1 %2 %3 %4\",\n  \"args0\": [\n    {\n      \"type\": \"field_dropdown\",\n      \"name\": \"type\",\n      \"options\": [\n        [\n          \"reflect\",\n          \"reflect\"\n        ],\n        [\n          \"faceforward\",\n          \"faceforward\"\n        ],\n          ]\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME2\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME3\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"Converts from degrees to radians or from radians to degrees\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['geometric_2functions'] = function(block) {
    var name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    var name2 = Blockly.JavaScript.valueToCode(block, 'NAME2', Blockly.JavaScript.ORDER_ATOMIC);
    var value_type = block.getFieldValue('type');

    var code;
    switch (value_type) {
        case "distance": code='distance('+name+','+name2+')';break;
        case "dot": code='dot('+name+','+name2+')';break;
        case "cross": code='cross('+name+','+name2+')';break;
        case "reflect": code='reflect('+name+','+name2+')';break;
    }
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['geometric_2functions'] = {
    init: function() {
        this.jsonInit(JSON.parse( "{\n  \"type\": \"geometric_2functions\",\n  \"message0\": \"%1 %2 %3 %4\",\n  \"args0\": [\n    {\n      \"type\": \"field_dropdown\",\n      \"name\": \"type\",\n      \"options\": [\n        [\n          \"distance\",\n          \"distance\"\n        ],\n    [\n          \"reflect\",\n          \"reflect\"\n        ],\n    [\n          \"cross\",\n          \"cross\"\n        ],\n        [\n          \"dot\",\n          \"dot\"\n        ],\n          ]\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME2\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME3\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"Converts from degrees to radians or from radians to degrees\",\n  \"helpUrl\": \"\"\n}"));
    }
};


Blockly.JavaScript['geometric_functions'] = function(block) {
    var name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    var value_type = block.getFieldValue('type');

    var code;
    switch (value_type) {
        case "length": code='length('+name+')';break;
        case "normalize": code='length('+name+')';break;
    }
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['geometric_functions'] = {
    init: function() {
        this.jsonInit(JSON.parse( "{\n  \"type\": \"geometric_functions\",\n  \"message0\": \"%1 %2 %3 %4\",\n  \"args0\": [\n    {\n      \"type\": \"field_dropdown\",\n      \"name\": \"type\",\n      \"options\": [\n        [\n          \"length\",\n          \"length\"\n        ],\n    [\n          \"normalize\",\n          \"normalize\"\n        ],\n         ]\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME2\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME3\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"Converts from degrees to radians or from radians to degrees\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['matrix_multiplication'] = function(block) {
    var name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    var name2 = Blockly.JavaScript.valueToCode(block, 'NAME2', Blockly.JavaScript.ORDER_ATOMIC);

    var code;
    code='matrixCompMult('+name+','+name2+')';

    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['matrix_multiplication'] = {
    init: function() {
        this.jsonInit(JSON.parse(  "{\n  \"type\": \"matrix_multiplication\",\n  \"message0\": \"matrixCompMult %1 %2\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME2\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"Converts from degrees to radians or from radians to degrees\",\n  \"helpUrl\": \"\"\n}"));
    }
};


Blockly.JavaScript['texture2d'] = function(block) {
    var name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    var name2 = Blockly.JavaScript.valueToCode(block, 'NAME2', Blockly.JavaScript.ORDER_ATOMIC);
    var bias = Blockly.JavaScript.valueToCode(block, 'bias', Blockly.JavaScript.ORDER_ATOMIC);

    var code='texture2D('+name+','+name2+','+bias+')';

    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['texture2d'] = {
    init: function() {
        this.jsonInit(JSON.parse(  "{\n  \"type\": \"texture2d\",\n  \"message0\": \"texture2D %1 %2 %3\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"NAME2\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"bias\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"output\": null,\n  \"colour\": 230,\n  \"tooltip\": \"Get the texture value at a given point. Optional bias parameter is added before the texture lookup\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript.logic_compare_vectors=function(a){
    var b={EQ:"==",NEQ:"!=",LT:"<",LTE:"<=",GT:">",GTE:">="}[a.getFieldValue("OP")],c="=="==b||"!="==b?Blockly.JavaScript.ORDER_EQUALITY:Blockly.JavaScript.ORDER_RELATIONAL,d=Blockly.JavaScript.valueToCode(a,"A",c)||"0";a=Blockly.JavaScript.valueToCode(a,"B",c)||"0";
    switch (b) {
        case "==" : return["equal("+a+","+d+")",c];
        case "!=" : return["notEqual("+a+","+d+")",c];
        case ">=" : return["greaterThanEqual("+a+","+d+")",c];
        case "<=" : return["lessThanEqual("+a+","+d+")",c];
        case "<" : return["lessThan("+a+","+d+")",c];
        case ">" : return["greaterThan("+a+","+d+")",c];
    }
};


Blockly.JavaScript['gl_for'] = function(block) {
    var aaa = Blockly.JavaScript.valueToCode(block, 'a', Blockly.JavaScript.ORDER_ATOMIC);
    var bbb = Blockly.JavaScript.valueToCode(block, 'b', Blockly.JavaScript.ORDER_ATOMIC);
    var ccc = Blockly.JavaScript.valueToCode(block, 'c', Blockly.JavaScript.ORDER_ATOMIC);
    var statements = Blockly.JavaScript.statementToCode(block, 'statements');

    let code = 'for('+aaa+';'+bbb+';'+ccc+'){'+statements+'}';

    return code;
};

Blockly.Blocks['gl_for'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"for\",\n  \"message0\": \"for( %1 ; %2 ; %3 ) %4 %5\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"a\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"b\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"c\"\n    },\n    {\n      \"type\": \"input_dummy\"\n    },\n    {\n      \"type\": \"input_statement\",\n      \"name\": \"statements\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"previousStatement\": null,\n  \"nextStatement\": null,\n  \"colour\": 120,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};


Blockly.JavaScript['gl_for_new2'] = function(block) {
    var vartype = block.getFieldValue('vartype');
    var varname = block.getFieldValue('var');
    var varinitial = block.getFieldValue('varinitial');
    var varfinal = block.getFieldValue('varfinal');
    var varincrement = block.getFieldValue('varincrement');
    var comparation = block.getFieldValue('comparation');
    var incrementsymbol = block.getFieldValue('incrementsymbol');
    var statements = Blockly.JavaScript.statementToCode(block, 'statements');

    let code = 'for('+vartype+' '+varname+'='+varinitial+';'+varname+comparation+varfinal+';'+varname+incrementsymbol+varincrement+'){'+statements+'}';

    return code;
};

Blockly.Blocks['gl_for_new2'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"gl_for_new2\",\n  \"message0\": \"for( %1 %2 = %3 ; var %4 %5 ; var %6 %7 ) %8 %9\",\n  \"args0\": [\n    {\n      \"type\": \"field_dropdown\",\n      \"name\": \"vartype\",\n      \"options\": [\n        [\n          \"int\",\n          \"int\"\n        ],\n        [\n          \"float\",\n          \"float\"\n        ]\n      ]\n    },\n    {\n      \"type\": \"field_input\",\n      \"name\": \"var\",\n      \"text\": \"var\"\n    },\n    {\n      \"type\": \"field_input\",\n      \"name\": \"varinitial\",\n      \"text\": \"0\"\n    },\n    {\n      \"type\": \"field_dropdown\",\n      \"name\": \"comparation\",\n      \"options\": [\n        [\n          \"<\",\n          \"<\"\n        ],\n        [\n          \"<=\",\n          \"<=\"\n        ],\n        [\n          \">\",\n          \">\"\n        ],\n        [\n          \">=\",\n          \">=\"\n        ],\n        [\n          \"!=\",\n          \"!=\"\n        ],\n        [\n          \"==\",\n          \"==\"\n        ]\n      ]\n    },\n    {\n      \"type\": \"field_input\",\n      \"name\": \"varfinal\",\n      \"text\": \"0\"\n    },\n    {\n      \"type\": \"field_dropdown\",\n      \"name\": \"incrementsymbol\",\n      \"options\": [\n        [\n          \"+=\",\n          \"+=\"\n        ],\n        [\n          \"-=\",\n          \"-=\"\n        ]\n      ]\n    },\n    {\n      \"type\": \"field_input\",\n      \"name\": \"varincrement\",\n      \"text\": \"1\"\n    },\n    {\n      \"type\": \"input_dummy\"\n    },\n    {\n      \"type\": \"input_statement\",\n      \"name\": \"statements\"\n    }\n  ],\n  \"colour\": 120,\n \"previousStatement\": null,\n  \"nextStatement\": null,\n \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};


Blockly.JavaScript['gl_while'] = function(block) {
    var aaa = Blockly.JavaScript.valueToCode(block, 'aaaa', Blockly.JavaScript.ORDER_ATOMIC);
    var statements = Blockly.JavaScript.statementToCode(block, 'statements');

    let code = 'while('+aaa+'){\n'+statements+'\n}';

    return code;
};

Blockly.Blocks['gl_while'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"gl_while\",\n  \"message0\": \"while( %1 ) %2 %3\",\n  \"args0\": [\n    {\n      \"type\": \"input_value\",\n      \"name\": \"aaaa\"\n    },\n    {\n      \"type\": \"input_dummy\"\n    },\n    {\n      \"type\": \"input_statement\",\n      \"name\": \"statements\"\n    }\n  ],\n  \"inputsInline\": true,\n  \"previousStatement\": null,\n  \"nextStatement\": null,\n  \"colour\": 120,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};


Blockly.JavaScript['gl_do_while'] = function(block) {
    var aaa = Blockly.JavaScript.valueToCode(block, 'aaaa', Blockly.JavaScript.ORDER_ATOMIC);
    var statements = Blockly.JavaScript.statementToCode(block, 'statements');

    let code = 'do{'+statements+'} while('+aaa+');';

    return code;
};

Blockly.Blocks['gl_do_while'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"gl_do_while\",\n  \"message0\": \"do %1 %2 while %3\",\n  \"args0\": [\n    {\n      \"type\": \"input_dummy\"\n    },\n    {\n      \"type\": \"input_statement\",\n      \"name\": \"statements\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"a\"\n    }\n  ],\n  \"inputsInline\": false,\n  \"previousStatement\": null,\n  \"nextStatement\": null,\n  \"colour\": 120,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['gl_break_continue'] = function(block) {
    var value_type = block.getFieldValue('type');

    var code;
    switch (value_type) {
        case "break": code='break;';break;
        case "continue": code='continue;';break;
    }

    return code;
};

Blockly.Blocks['gl_break_continue'] = {
    init: function() {
        this.jsonInit(JSON.parse( "{\n  \"type\": \"gl_break_continue\",\n  \"message0\": \"%1 %2\",\n  \"args0\": [\n    {\n      \"type\": \"field_dropdown\",\n      \"name\": \"type\",\n      \"options\": [\n        [\n          \"break\",\n          \"break\"\n        ],\n        [\n          \"continue\",\n          \"continue\"\n        ]\n      ]\n    },\n    {\n      \"type\": \"input_dummy\",\n      \"name\": \"NAME\"\n    }\n  ],\n   \"colour\": 120,\n  \"tooltip\": \"Converts from degrees to radians or from radians to degrees\",\n  \"previousStatement\": null,\n  \"nextStatement\": null,\n  \"helpUrl\": \"\"\n}"));
    }
};

Blockly.JavaScript['midi_knob2'] = function(block) {
    return ['k'+(String(Number(block.getFieldValue('numberaso')))), Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['midi_knob2'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"midi_knob\",\n  \"message0\": \"MIDI Knob %1\",\n  \"args0\": [\n    {\n      \"type\": \"field_number\",\n      \"name\": \"numberaso\",\n      \"value\": 1,\n      \"min\": 1,\n      \"max\": 8\n    }\n  ],\n  \"inputsInline\": false,\n  \"output\": null,\n  \"colour\": 285,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};


Blockly.JavaScript['midi_pad2'] = function(block) {
    return ['p'+(String(Number(block.getFieldValue('numberaso')))), Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['midi_pad2'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"midi_knob\",\n  \"message0\": \"MIDI Pad %1\",\n  \"args0\": [\n    {\n      \"type\": \"field_number\",\n      \"name\": \"numberaso\",\n      \"value\": 1,\n      \"min\": 1,\n      \"max\": 8\n    }\n  ],\n  \"inputsInline\": false,\n  \"output\": null,\n  \"colour\": 285,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};


Blockly.JavaScript['midi_program'] = function(block) {
    return ['MIDI_PROGRAM', Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['midi_program'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"midi_knob\",\n  \"message0\": \"MIDI Program\",\n   \"inputsInline\": false,\n  \"output\": null,\n  \"colour\": 285,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};


Blockly.JavaScript['imgatpixel'] = function (block) {
    var value_name = block.getFieldValue('NAME');
    var value_pixel = Blockly.JavaScript.valueToCode(block, 'pixel', Blockly.JavaScript.ORDER_ATOMIC);
    var code = 'IMG_PIXEL(' + value_name + ',' + value_pixel + ')';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['imgatpixel'] = {
    init: function() {
        //this.jsonInit(JSON.parse("{\n  \"type\": \"imgatpixel\",\n  \"message0\": \"%1 at pixel %2\",\n  \"args0\": [\n    {\n      \"type\": \"input_dummy\",\n      \"name\": \"NAME\"\n          },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"pixel\"\n    }\n  ] ,\"extensions\": [\"dynamic_texture_extension\"]\n ,\n  \"output\": null,\n  \"colour\": 120,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
        this.jsonInit(JSON.parse("{\n  \"type\": \"imgatpixel\",\n  \"message0\": \"%1 at pixel %2\",\n  \"args0\": [\n    {\n      \"type\": \"input_dummy\",\n      \"name\": \"NAME_PLACE\",\n      \"text\": \"texture\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"pixel\"\n    }\n  ] \n ,\"extensions\": [\"dynamic_texture_extension\"]\n ,\n  \"output\": null,\n  \"colour\": 120,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};


Blockly.JavaScript['imgatnormpixel'] = function (block) {
    var value_name = block.getFieldValue('NAME');
    var value_pixel = Blockly.JavaScript.valueToCode(block, 'pixel', Blockly.JavaScript.ORDER_ATOMIC);
    var code = 'IMG_NORM_PIXEL(' + value_name + ',' + value_pixel + ')';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['imgatnormpixel'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"imgatnormpixel\",\n  \"message0\": \"%1 at normalized pixel %2\",\n  \"args0\": [\n    {\n      \"type\": \"input_dummy\",\n      \"name\": \"NAME_PLACE\",\n      \"text\": \"texture\"\n    },\n    {\n      \"type\": \"input_value\",\n      \"name\": \"pixel\"\n    }\n  ]\n ,\"extensions\": [\"dynamic_texture_extension\"]\n ,\n  \"output\": null,\n  \"colour\": 120,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};


Blockly.JavaScript['imgatthispixel'] = function (block) {
    var value_name = block.getFieldValue('NAME');
    var code = 'IMG_THIS_PIXEL(' + value_name + ')';
    return [code, Blockly.JavaScript.ORDER_NONE];
};
Blockly.JavaScript['imgatthispixel'] = function (block) {
    var value_name = block.getFieldValue('NAME');
    var code = 'IMG_THIS_PIXEL(' + value_name + ')';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['imgatthispixel'] = {
    init: function() {
        this.jsonInit(JSON.parse("{\n  \"type\": \"imgatthispixel\",\n  \"message0\": \"%1 at this pixel\",\n  \"args0\": [\n    {\n      \"type\": \"input_dummy\",\n      \"name\": \"NAME_PLACE\",\n      \"text\": \"texture\"\n    }\n  ]\n ,\"extensions\": [\"dynamic_texture_extension\"]\n ,\n  \"output\": null,\n  \"colour\": 120,\n  \"tooltip\": \"\",\n  \"helpUrl\": \"\"\n}"));
    }
};



Blockly.JavaScript['colour_rgb'] = function(block) {
    // Compose a colour from RGB components expressed as percentages.
    var red = Blockly.JavaScript.valueToCode(block, 'RED',
        Blockly.JavaScript.ORDER_COMMA) || 0;
    var green = Blockly.JavaScript.valueToCode(block, 'GREEN',
        Blockly.JavaScript.ORDER_COMMA) || 0;
    var blue = Blockly.JavaScript.valueToCode(block, 'BLUE',
        Blockly.JavaScript.ORDER_COMMA) || 0;
    var code = 'vec4(' + red + ', ' + green + ', ' + blue + ',1.0)';
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.JavaScript['colour_picker'] = function(block) {
    // Colour picker.
    const parsed = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(block.getFieldValue('COLOUR'));
    let result = parsed ? {
        r: parseInt(parsed[1], 16)/255,
        g: parseInt(parsed[2], 16)/255,
        b: parseInt(parsed[3], 16)/255
    } : null;
    var code = 'vec4('+result.r+','+result.g+','+result.b+',1.0)';
    return [code, Blockly.JavaScript.ORDER_ATOMIC];
};



Blockly.Extensions.register('dynamic_texture_extension',
    function() {
        this.getInput('NAME_PLACE')
            .appendField(new Blockly.FieldDropdown(
                function() {
                    var options = [];
                    var now = Date.now();
                    Object.entries(variables_a_inyectar['textures'] || {}).forEach(([key, val]) => {
                        options.push([key,key]);
                    });
                    preferences.BACKBUFFER ? options.push(["backbuffer","backbuffer"]) : null;
                    return options;
                }), 'NAME');
    });


/*
Blockly.Blocks['sdsegment'] = {

    init: function() {
        this.jsonInit(JSON.parse());
    }
};

 */