const MAPPEDMIDI = [ 'MIDI_program', 'k1', 'k2' ,'k3','k4','k5','k6','k7','k8'
  , 'p1', 'p2' ,'p3','p4','p5','p6','p7','p8'];

var learningMidi = null;
var midiLinks = [];

navigator.requestMIDIAccess()
    .then(onMIDISuccess, onMIDIFailure);

function onMIDIFailure() {
  console.log('Could not access your MIDI devices.');
}

function onMIDISuccess(midiAccess) {
  for (var input of midiAccess.inputs.values())
    input.onmidimessage = getMIDIMessage;
}

function getMIDIMessage(message) {
  var command = message.data[0];
  var note = message.data[1];
  var velocity = (message.data.length > 2) ? message.data[2] : 0; // a velocity value might not be included with a noteOff command

  if(learningMidi){
    if(midiLinks[note]){
      unlearnMidi(midiLinks[note]);   // si ya estaba linkado, se deslinkea
    }
    midiLinks[note] = learningMidi;
    document.getElementById("midi_button"+learningMidi).src="icons/midi_green.png";
    learningMidi = null;
  }
  if(midiLinks[note]){
    slider_min = parseFloat(document.getElementById('tune_'+midiLinks[note]).min);
    slider_max = parseFloat(document.getElementById('tune_'+midiLinks[note]).max);
    diff = slider_max - slider_min;
    document.getElementById('tune_'+midiLinks[note]).value = slider_min+(diff*(velocity/127));
    updateVariableValue(midiLinks[note],slider_min+(diff*(velocity/127)));
    //variables_a_inyectar['tune'][midiLinks[note]] = slider_min+(diff*(velocity/127));
  }
}


function learnMidi(name){
  if (midiLinks.indexOf(name) > -1) {
    unlearnMidi(name);
  }
  if(learningMidi){
    document.getElementById("midi_button"+learningMidi).src="icons/midi.png";
  }
  learningMidi=name;
  document.getElementById("midi_button"+name).src="icons/midi_orange.png";
}

function unlearnMidi(name){
  const index = midiLinks.indexOf(name);
  if (index > -1) {
    midiLinks.splice(index, 1);
  }
  document.getElementById("midi_button"+name).src="icons/midi.png";
}