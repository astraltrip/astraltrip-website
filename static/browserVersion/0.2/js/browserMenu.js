

function loadMenuHandler() {

    let menuBar = new Menu({ type: 'menubar' });
    var fileMenu = new Menu();
    fileMenu.append(new MenuItem({
        label: 'New project',
        click: () => {
            new_project(true);
        }
    }));
    fileMenu.append(new MenuItem({
        label: 'Save project (soon)',
        click: () => {
            //setQuality(1);
        }
    }));
    fileMenu.append(new MenuItem({
        label: 'Open project (soon)',
        click: () => {
            $('#projectInput').click();
        }
    }));
    fileMenu.append(new MenuItem({type: 'separator'}));
    var downloadProgramMenu = new Menu();
    downloadProgramMenu.append(new MenuItem({
        label: 'Linux - AppImage',
        click: () => {
            setQuality(0.5);
        }
    }));
    downloadProgramMenu.append(new MenuItem({
        label: 'Windows (soon)',
        click: () => {
            setQuality(1);
        }
    }));
    downloadProgramMenu.append(new MenuItem({
        label: 'Mac (soon)',
        click: () => {
            setQuality(2);
        }
    }));
    var projectMenu = new Menu();
    var qualityMenu= new Menu();
    qualityMenu.append(new MenuItem({
        label: 'Highest',
        click: () => {
            setQuality(0.5);
        }
    }));
    qualityMenu.append(new MenuItem({
        label: 'High',
        click: () => {
            setQuality(1);
        }
    }));
    qualityMenu.append(new MenuItem({
        label: 'Normal',
        click: () => {
            setQuality(2);
        }
    }));
    qualityMenu.append(new MenuItem({
        label: 'Low',
        click: () => {
            setQuality(4);
        }
    }));
    qualityMenu.append(new MenuItem({
        label: 'Lowest',
        click: () => {
            setQuality(8);
        }
    }));
    projectMenu.append(new MenuItem({label: "Run", click: () => {
            run_project();
        }}));
    projectMenu.append(new MenuItem({type: 'separator'}));
    projectMenu.append(new MenuItem({label: "Enable live-coding",                 type: 'checkbox',
        checked: false,click: () => {
            des_activar_livecoding();
        }}));
    projectMenu.append(new MenuItem({label: "Enable preview",                 type: 'checkbox',
        checked: true,click: () => {
            tooglePreviewMode();
        }}));
    projectMenu.append(new MenuItem({label: "Enable end-time mark",                 type: 'checkbox',
        checked: false,click: () => {
            timeline_des_activate_end();
        }}));
    projectMenu.append(new MenuItem({type: 'separator'}));
    projectMenu.append(new MenuItem({label: "Preferences",              click: () => {
            openPreferences();
        }}));
    projectMenu.append(new MenuItem({label: "Set quality", submenu: qualityMenu}));
    projectMenu.append(new MenuItem({type: 'separator'}));

    var exportMenu= new Menu();
    exportMenu.append(new MenuItem({
        label: 'Video (soon)',
        disabled: true,
        click: () => {
            //setQuality(0.5);
        }
    }));
    exportMenu.append(new MenuItem({
        label: 'PNG Sequence (soon)',
        disabled: true,
        click: () => {
            //setQuality(1);
        }
    }));
    exportMenu.append(new MenuItem({
        label: 'ISF (soon)',
        disabled: true,
        click: () => {
            //setQuality(2);
        }
    }));
    exportMenu.append(new MenuItem({
        label: 'GLSL',
        click: () => {
            openGLSLExporter();
        }
    }));
    projectMenu.append(new MenuItem({label: "Export as", submenu: exportMenu}));
    var viewMenu = new Menu();
    viewMenu.append(new MenuItem({
        label: 'Virtual MIDI controller',
        click: () => {
            window.open("https://studiocode.dev/bs2-editor/", '_blank');
        }
    }));
    var helpMenu = new Menu();
    helpMenu.append(new MenuItem({
        label: 'Online help',
        click: () => {
            //require("electron").shell.openExternal("https://www.patreon.com/astraltrip");
            window.open("https://www.patreon.com/astraltrip", '_blank');
        }
    }));
    helpMenu.append(new MenuItem({
        label: 'Support',
        click: () => {
            window.open("https://www.patreon.com/astraltrip", '_blank');
        }
    }));
    helpMenu.append(new MenuItem({
        label: 'Report bug',
        click: () => {
            window.open("https://gitlab.com/astraltrip/astraltrip-website/-/issues", '_blank');
        }
    }));

    menuBar.append(new MenuItem({label: 'File', submenu: fileMenu}));
    menuBar.append(new MenuItem({label: 'Project', submenu: projectMenu}));
    menuBar.append(new MenuItem({label: 'View', submenu: viewMenu}));
    menuBar.append(new MenuItem({label: 'Help', submenu: helpMenu}));
    Menu.setApplicationMenu(menuBar);

    document.getElementById('sidebar').style.top = "20px";  // para que el sidebar baje un pokito
    console.log("MENUAADIDO")
}
window.addEventListener("load", loadMenuHandler, false);

