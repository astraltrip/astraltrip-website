
var nextIdItemTimeline=0;
var looping = false;


// Configuration for the Timeline
var optionsTimeline = {
    minHeight: 800,
    start: 0,
    end: 130000,
    //verticalScroll: true,
    horizontalScroll: true,
    multiselect: true,
    showMajorLabels: false, // pa que no se vean los dias
    zoomKey: 'ctrlKey',
    min: 0,                // lower limit of visible range
    max: new Date(2018, 0, 1),
    zoomMax: 1000 * 60 * 60 * 24 * 31 * 3,
    zoomMin: 1,
    zoomFriction:4,
    orientation: 'top',
    snap: null,     // todo: hacer que se esnapee al final de los otros items
    stack: true,
    editable:{
        add: false,
        remove: true,
        updateTime: true,
        updateGroup: true,
    },
    itemsAlwaysDraggable: {
        range: true,
        item: true
    },
    //timeAxis: {scale: 'millisecond', step: 5},
    onMove: (item, callback) => {
        console.log(item);
        item.start_ms = item.start.getTime();
        item.start = item.start.toISOString();
        item.end_ms = item.end.getTime();
        item.end = item.end.toISOString();

        item.min = itemsTimeline.get(item.id).min;
        item.max = itemsTimeline.get(item.id).max;

        callback(item);
        updateVideosPosition();
    },
    onRemove: function(item, callback) {
        console.log("GREGE",item)
        if(item.func=="audio"){
            document.getElementById("audio").pause(); document.getElementById("audio").src="";
        }
        callback(item);
    },
};

function makeItemId() {
    while(itemsTimeline.get(nextIdItemTimeline)){
        nextIdItemTimeline++;
    }
    return nextIdItemTimeline;
}


function updateItemTimelineCurve(idItem){
    const item = itemsTimeline.get(idItem);
    let event;
    if(parseFloat(item.min) > parseFloat(item.max)){
        event = new CustomEvent("curveHighToLow", { "detail": idItem });
    } else {
        event = new CustomEvent("curveLowToHigh", { "detail": idItem });
    }
    document.getElementById("canvasCurve"+idItem).dispatchEvent(event);
}

function itemTimelineSetMin(idItem, newMin) {
    itemsTimeline.get(idItem).min=newMin;
    updateItemTimelineCurve(idItem);
}

function itemTimelineSetMax(idItem, newMax) {
    itemsTimeline.get(idItem).max=newMax;
    updateItemTimelineCurve(idItem);
}

function itemTimelineSetLoop(idItem, newLoopTime) {
    itemsTimeline.get(idItem).loop=newLoopTime*1000;

}


function itemTimelineSetVolumen(idItem, newVolume) {
    const func =itemsTimeline.get(idItem).func;
    if(func=="texture"){
        variables_a_inyectar['textures'][itemsTimeline.get(idItem).group].volume = newVolume;
    }else{
        htmlAudioElement.volume = newVolume;
    }
}


function itemTimelineSetSpeed(idItem, newplaybackRate) {
    const func =itemsTimeline.get(idItem).func;
    if(func=="texture"){
        variables_a_inyectar['textures'][itemsTimeline.get(idItem).group].playbackRate = newplaybackRate;
    }else{
        htmlAudioElement.playbackRate = newplaybackRate;
    }
}


function createTemplateAutomationTimeline(idItem, variableName){
    return '<div id="itemContentWrapper'+idItem+'">' +
           //'<img src="icons/conf.png" alt="configure curve" id="exportprojectButton" onclick="configItemTimeline('+idItem+')" style="position:absolute;">' +
           '<img src="'+curveImages[idItem]+'" alt="Curve" id="curvepreview'+idItem+'" style="width: 100%;height: 32px" >' +
           '</div>'
}

function createTemplateVideoTimeline(idItem, filepath){
    return '<div id="itemContentWrapper'+idItem+'">' +
           //'<img src="icons/conf.png" alt="configure curve" id="exportprojectButton" onclick="configItemTimeline('+idItem+')" style="position:absolute;">' +
           '<video src="'+filepath+'" alt="Curve" data-name="'+idItem+'" id="videoElement'+idItem+'" preload="metadata" style="width: 100%;height: 32px" onloadedmetadata = "timelineSetVideoItemDuration('+idItem+',this.duration)"></video>' +
           '</div>'
}

function createTemplateImageTimeline(idItem, filepath){
    return '<div id="itemContentWrapper'+idItem+'">' +
           //'<img src="icons/conf.png" alt="configure curve" id="exportprojectButton" onclick="configItemTimeline('+idItem+')" style="position:absolute;">' +
           '<img src="'+filepath+'" alt="Curve" data-name="'+idItem+'" id="imgElement'+idItem+'" preload="metadata" style="width: 100%;height: 32px">' +
           '</div>'
}

function createTemplateSongTimeline(idItem, variableName){
    return '<div id="itemContentWrapper'+idItem+'">' +
           //'<img src="icons/conf.png" alt="Save As" id="exportprojectButton" onclick="configItemTimeline('+idItem+')" style="position:absolute;">' +
           '<img src="'+curveImages[idItem]+'" alt="Song" id="waveform'+idItem+'" style="width: 100%;height: 32px" >' +
            //'<audio controls crossorigin="anonymous" id="audio" src="" preload="metadata" style="display: none; "</audio>' +
           '</div>'
}

function createTemplateItemTimeline(type){
    switch (type) {
        case "automation": return createTemplateAutomationTimeline();
        case "song": break;
        case "video": break;
    }
}

function closeMenuConfigItemTimeline() {
    configurandoItem=null;
    document.getElementById('menuConfigItemTimeline').style.display = "none";
    document.getElementById('menuConfigItemVideoTimeline').style.display = "none";
}

function configItemTimeline(idItem) {
    if(configurandoItem == idItem) {
       this.closeMenuConfigItemTimeline();
    }else {
        configurandoItem = idItem;
        if(itemsTimeline.get(idItem).group == "AUDIO"){
            console.log('#itemContentWrapper'+idItem)
            document.getElementById('menuConfigItemVideoTimeline').style.display = "block";
            const position = document.getElementById('itemContentWrapper' + idItem).getBoundingClientRect();
            document.getElementById('menuConfigItemVideoTimeline').style.top = position.top - 230 + "px";
            document.getElementById('menuConfigItemVideoTimeline').style.left = position.left + "px";
            document.getElementById('volumeControler').value = htmlAudioElement.volume;
            document.getElementById('speedControler').value = htmlAudioElement.playbackRate;
        } else if(itemsTimeline.get(idItem).func == "texture"){
            document.getElementById('menuConfigItemVideoTimeline').style.display = "block";
            const position = document.getElementById('itemContentWrapper' + idItem).getBoundingClientRect();
            document.getElementById('menuConfigItemVideoTimeline').style.top = position.top - 230 + "px";
            document.getElementById('menuConfigItemVideoTimeline').style.left = position.left + "px";
            document.getElementById('volumeControler').value = variables_a_inyectar['textures'][itemsTimeline.get(idItem).group].volume;
            document.getElementById('speedControler').value = variables_a_inyectar['textures'][itemsTimeline.get(idItem).group].playbackRate;
        }else {
            document.getElementById('curveMenuConfigItemTimeline').innerText = itemsTimeline.get(idItem).curve;
            drawActualCurve();
            document.getElementById('menuConfigItemTimeline').style.display = "block";
            const position = document.getElementById('itemContentWrapper' + idItem).getBoundingClientRect();
            document.getElementById('menuConfigItemTimeline').style.top = position.top - 230 + "px";
            document.getElementById('menuConfigItemTimeline').style.left = position.left + "px";
        }
    }
}

function deleteItemFromTimeline(idItem){
    console.log(idItem);
    const item = itemsTimeline.get(idItem);
    if(item.curve) {
        delete curves[idItem];
    }else if(item.func=="audio"){
        htmlAudioElement.pause(); htmlAudioElement.src="";
    }

    itemsTimeline.remove(idItem);
}

function timelineGetItemValuesAtTime(time) {
    let result = [];
    itemsTimeline.forEach((item) =>{
        if(item.start_ms <= time && item.end_ms > time){
            if(item.group == "AUDIO") {
                now_playing=true;
                htmlAudioElement.play();
                //console.log("DANCE");
                // TODO cargar los valores precalculados de los features desde un array para el ms actual.
            }else if(item.func ==="texture"){
                result[item.group] = document.getElementById('videoElement'+item.id);
                //variables_a_inyectar['textures'][item.group]['playing']=true;
                result[item.group].play();
            }else if(item.func ==="texturesStatic"){
                result[item.group] = document.getElementById('imgElement'+item.id);
            }else{
                let actual =time - item.start_ms;
                let total = item.end_ms - item.start_ms;
                if(item.loop !== 0){
                    actual = actual % item.loop;
                    total = Math.min(total,item.loop);
                }
                const diff = Math.abs(item.max- item.min);
                if(item.func === "curve"){
                    if(curves[item.id].mode == "HTL") {
                        result[item.group] = parseFloat(item.min) - ((( (curves[item.id].compute(actual / total).y / 200)) * diff));
                    }else{
                        result[item.group] = parseFloat(item.min) + (((1 - (curves[item.id].compute(actual / total).y / 200)) * diff));
                    }

                }
                if(item.func === "beat"){
                    result[item.group]=parseFloat(item.min)+((actual/total)*diff);    // todo make beat and stepped
                }
            }
        }else{
            if ( item.func ==="texture"){
                document.getElementById('videoElement'+item.id).pause();        // todo sacar el play y el pause a una funcion "pararmultimediaquenoestaenrango" que pare videos y audio cuando se mueva el tiempo, no cuando se actualice.
                document.getElementById('videoElement'+item.id).currentTime= 0 ;
            }
        }
    });

    groupsTimeline.forEach((group) =>{
        if(!result[group.id] && group.type=="texture"){
            result[group.id] =imgblackpixel; // black pixel
        }
    });

    return result;
}

function updateTimeTimeline(){
    if(timelinePlaying) {
        timeline.setCurrentTime(timeTimeline);
        //console.log("dd",timeTimeline)
        var endTime;
        try{endTime = timeline.getCustomTime('finishLine')}catch (e) { }
        if(endTime) {
            if (timeTimeline >= endTime.getTime()) {
                //timelinePlaying = false;
                if (looping) {
                    timelineGoToStart();
                } else {
                    timeline.setCurrentTime(endTime);
                    timelinePlay();
                }
            }
        }
        if(ejecutando_sketch) window_sketch.time =timeTimeline;
        const itemValues = timelineGetItemValuesAtTime(timeTimeline);
        //console.log("CURVEE"+itemValues)
        for( (item) in itemValues) {
            if(!isNaN(parseFloat(itemValues[item]))) {
                // is float
                variables_a_inyectar['auto'][item]=itemValues[item];
            } else{
                variables_a_inyectar['textures'][item]=itemValues[item];
            }

            cambiosAInyectarAuto = true;
        }
    }else {
        timeline.setCurrentTime(timeTimeline);
    }
}
setInterval(updateTimeTimeline,10);


function timelineActivateEditGroupName(groupname) {
    if(groupname=="null") return;

    new_content =' <a alt="Delete" style="display: inline-block" onclick=\'timelineDeleteGroup("'+groupname+'")\' >x</a><input  style="display: inline-block"  type="text" id="auto_'+groupname+'_newname" value="'+groupname+'"  onblur="timelineEditGroup(\''+groupname+'\',this.value)">';
    document.getElementById("label_"+groupname).innerHTML = new_content;
    document.getElementById("auto_"+groupname+'_newname').focus();

//groupsTimeline.get(groupname).content=new_content;
}


function timelineEditGroup(groupname, newname) {
    if(groupname==newname) {
        document.getElementById("label_"+groupname).innerHTML = groupname;
        return;
    }
    timelineAddGroup(newname);

    itemsTimeline.forEach((item) => {
        if(item.group == groupname){
            itemsTimeline.update({id: item.id, group:newname});
        }
    });
    groupsTimeline.remove(groupname);
    //todo que no se desordene
    variables_a_inyectar['auto'][newname]=variables_a_inyectar['auto'][groupname];
    delete variables_a_inyectar['auto'][groupname];
    cambiosAInyectarAuto = true;
    setearProyectoComoGuardado(false);
}

function timelineDeleteGroup(groupname) {
    const group = groupsTimeline.get(groupname);
    itemsTimeline.forEach((item) => {
        if(item.group == groupname){
            timelineDeleteItem(item.id);
        }
    });
    if(group.type == "texture"){
        variables_a_inyectar['textures'].splice(groupname, 1);
    }else {
        delete variables_a_inyectar['auto'][groupname];
    }
    groupsTimeline.remove(groupname);
    cambiosAInyectarAuto = true;
    setearProyectoComoGuardado(false);
}

function timelineDeleteItem(itemId) {
    itemsTimeline.remove(itemId);
}

function timelineAddGroup(groupName = null, type = "float") {
    groupName = groupName || randomWords();
    const now = timeline.getCurrentTime().getTime();
    let newItem = {
        id: groupName,
        name: groupName,
        type: type,
        content: "<img style='display: inline-block; width: 20px; margin-right: 3px;' src='icons/group_" +type + ".png'><p style='display: inline-block' id='label_" +groupName + "'> "+groupName+"</p><a style='display: inline-block' onclick='timelineAddItemToGroup(\""+groupName+"\")' >+</a>",
        start:0,
        end: 10000,
    };
    groupsTimeline.add(newItem);
    //stash.set('loadtimelineGroups',groupsTimeline.get());
    if(type == "float") {
        variables_a_inyectar['auto'][groupName] = 0;
    }
    if(type == "texture"){
        // todo variables_a_inyectar['textures'][groupName] = document.getElementById("videoElement"+);
    }
    timeline.redraw();
    setearProyectoComoGuardado(false);
    return groupName;
}

function timelineAddItemToGroup(groupName, time = -1) {
    const itemName = makeItemId();
    let time_ms;
    if(time == -1) {
        const currentT = timeline.getCurrentTime();
        time = currentT.toISOString();
        time_ms = currentT.getTime();
    }else{
        time_ms = time.getTime();
        time = time.toISOString();
    }
    const groupType = groupsTimeline.get(groupName).type;
    if(groupType == "texture"){
        const textureInput = $("#textureInput")
        //textureInput['data-grupo'] = props.group;
        textureInput.attr('data-time',time);
        textureInput.attr('data-grupo',groupName);
        textureInput.trigger('click')
    }else {
        const defaultCurve = '(0,200 , 10,90 , 110,100 , 200,0)';
        let newItem = {
            id: itemName,
            name: itemName,
            start: time,
            start_ms: time_ms,
            end: dayjs(time).add(5, "second").toISOString(),
            end_ms: time_ms + 5000,
            min: 0,
            max: 1,
            loop: 0,
            group: groupName,
            func: 'curve',
            curve: defaultCurve,
            content: createTemplateAutomationTimeline(itemName, groupName),
        };
        curves[itemName] = eval('new Bezier' + defaultCurve);
        itemsTimeline.add(newItem);
        this.configItemTimeline(itemName);
    }
    //stash.set('loadtimelineItems',itemsTimeline.get());
    setearProyectoComoGuardado(false);
}

async function loadAudioOnTimeline() {
    const itemName = makeItemId();
    if(groupsTimeline.get('AUDIO') == null){
        timelineAddGroup('AUDIO', 'audio');
    }

    htmlAudioElement.src = audio_file;
    htmlAudioElement.onloadedmetadata = async () => {
        let newItem = {
            id: itemName,
            name: itemName,
            start: 1 + '',
            start_ms: 1,
            end: htmlAudioElement.duration * 1000,
            end_ms: htmlAudioElement.duration * 1000,
            group: 'AUDIO',
            func: "audio",
            audio_file: audio_file,
            editable: false,
            content: createTemplateSongTimeline(itemName),
            onRemove: (item, callback) => {
                console.log("GREGE", item)
                if (item.func == "audio") {
                    htmlAudioElement.pause();
                    htmlAudioElement.src = "";
                }
                callback(item);
            },
        };
        itemsTimeline.add(newItem);

        wavesurfer = WaveSurfer.create({
            container: '#itemContentWrapper' + itemName,
            waveColor: 'darkblue',
            progressColor: 'antiqueblue'
        });
        wavesurfer.load(audio_file);
        setTimeout(() => {
            curveImages[itemName]=wavesurfer.exportImage();
            const songpreview = document.getElementById('waveform'+itemName);
            console.log(curveImages[itemName]);
            if(songpreview){
                songpreview.src = curveImages[itemName];
            }
            wavesurfer.destroy();
        },1500);     //todo cambiar esto por un interval que se desactive cuando se haya cargado la imagen
        //stash.set('loadtimelineItems', itemsTimeline.get());
    }
}

async function timelineAddVideoItem(filePath, group=null, time=null) {
    const nameNuevoGrupo = group? group : timelineAddGroup(null,"texture");
    if(!time)time = timeTimeline;
    else time = time.getTime? time.getTime(): time;
    const itemName = makeItemId();
    let newItem = {
        id: itemName,
        name: itemName,
        start: time,
        start_ms: time,
        end: 5000,
        end_ms: 5000,
        min: 0,
        max: 1,
        loop: 0,
        group: nameNuevoGrupo,
        func: 'texture',
        content: createTemplateVideoTimeline(itemName,filePath),
    };
    const newItemCreated = await itemsTimeline.add(newItem);
    const videoElement = document.getElementById('videoElement'+itemName);
    //stash.set('loadtimelineItems',itemsTimeline.get());
}


function timelineSetVideoItemDuration(idItem, duration) {
    item = itemsTimeline.get(idItem);
    itemsTimeline.update({ id: idItem, end: new Date(item.start_ms+(duration*1000)).toISOString(), end_ms :item.start_ms+(duration*1000)});
    variables_a_inyectar['textures'][item.group] = document.getElementById('videoElement'+idItem);
}


async function timelineAddImageItem(filePath, group=null, time=null) {
    const nameNuevoGrupo =  group? group : timelineAddGroup(null,"texture");
    if(!time)time = timeTimeline;
    else time = time.getTime? time.getTime(): time;
    const itemName = makeItemId();
    let newItem = {
        id: itemName,
        name: itemName,
        start: time,
        start_ms: time,
        end: time+10000,
        end_ms: time+10000,
        min: 0,
        max: 1,
        loop: 0,
        group: nameNuevoGrupo,
        func: 'texturesStatic',
        content: createTemplateImageTimeline(itemName,filePath),
    };
    const newItemCreated = await itemsTimeline.add(newItem);
    //stash.set('loadtimelineItems',itemsTimeline.get());
}

function timelineAddFinishLine() {
    timeline.addCustomTime(120000, 'finishLine');
    timeline.setCustomTimeMarker("End <img src='icons/refresh-line.png' style='height: 20px' alt='Loop' id='loopButton' onclick='timeline_des_activate_loop()'>", 'finishLine', false);
}

function timeline_des_activate_loop() {
    looping = !looping;
    if(looping)
        document.getElementById('loopButton').src='icons/refresh-line-green.png';
    else
        document.getElementById('loopButton').src='icons/refresh-line.png';
}



function timeline_des_activate_end() {
    var endTime = null;
    try{endTime = timeline.getCustomTime('finishLine')}catch (e) { };
    if(endTime)
        timeline.removeCustomTime('finishLine');
    else
        timelineAddFinishLine();
}


// Create a DataSet (allows two way data-binding)
var itemsTimeline = new vis.DataSet([
    //{id: 1,  start: '20000', end: '290000', content: createTemplateAutomationTimeline(1)},
]);

var groupsTimeline = new vis.DataSet([
    //{id: 1,  start: '20000', end: '290000', content: createTemplateAutomationTimeline(1)},
]);

var containerTimeline = document.getElementById('timelineDiv');

// Create a Timeline
var timeline = new vis.Timeline(containerTimeline, itemsTimeline, groupsTimeline, optionsTimeline);
timelineGoToStart();

function updateTime(){
    if(timelinePlaying){
        timeTimeline = ((Date.now()- ((timeWhenPlayClicked)  )) * (BPM/60.0))+timeTimelineSeek;
        if(audioFeatures){		// Todo mover esto al update del timeline cuando haya un item generado para el audio otra vez
            variables_a_inyectar['features'] = audioFeatures[parseInt(timeTimeline/10)];
        }
    }
}
setInterval(updateTime, 10);

function timelineGoToStart() {
    timelineSetCurrentTime(0);
    timeWhenPlayClicked = Date.now();
    timeline.moveTo(0,{duration: 0.5, easingFunction: 'easeInOutQuad'});
    document.getElementById('audio').currentTime = 0;
    itemsTimeline.get().forEach( (item) => {
        if(item.func == "texture"){
            video = variables_a_inyectar['textures'][item.group];
            video.currentTime = 0;
            if(video.currentTime < 2) video.pause();
        }
    })
}

function timelinePlay() {
    timelinePlaying = !timelinePlaying;
    const button = document.getElementById('timelinePlayButton');
    if(timelinePlaying){
        timeWhenPlayClicked  = Date.now()-timeTimeline;
        //timeline.setCurrentTime(timeWhenPlayClicked);
        button.src = 'icons/pause.png';
        htmlAudioElement.play();
        startAudioAnalyzer();
    }else{
        timeTimelineSeek = 0;
            //timeWhenPlayClicked  = timeWhenPlayClicked+timeTimeline;
        //timeline.setCurrentTime(timeWhenPlayClicked);
        button.src = 'icons/play.png';

        Object.entries(variables_a_inyectar['textures'] || {}).forEach(([key, val]) => {
            if(val.pause)val.pause();
        });
        htmlAudioElement.pause();
        stopAudioAnalyzer();
    }
}

function timelineSetCurrentTime(time) {
    timeTimelineSeek = time === 0 ? 0 : time.getTime();
    timeWhenPlayClicked  = Date.now();
    timeline.setCurrentTime(time);

    timeTimeline = ((Date.now()- ((timeWhenPlayClicked)  )) * (BPM/60.0))+timeTimelineSeek;
    if(!timelinePlaying){
        //timeWhenPlayClicked  = time;

        timeTimelineSeek = 0;
    }
    //timeline.setCurrentTime(time);
    //timeWhenPlayClicked = Date.now()-timeTimeline;
    //timeTimeline=timeTimelineSeek;
    //timeWhenPlayClicked -= timeTimeline;
    /*itemsTimeline.get().forEach( (item) => {
        if(item.func == "texture"){
            video = variables_a_inyectar['textures'][item.group];
            //console.log(time.getTime(), item.start_ms, timeTimeline)
            video.currentTime = (timeTimeline - item.start_ms)/1000;
            if(video.currentTime < 2) video.pause();
        }
    })*/
    updateVideosPosition();
    document.getElementById('audio').currentTime = timeTimeline/1000;

}

timeline.on('doubleClick', function (props) {
    if(props.what === "background") {
        if(props.group != null) {
            if(groupsTimeline.get(props.group).type == "texture") {     // todo else abrir selector de video para añadir otra extura al mismo grupo
                //openMultimedia(props.group, props.time);
                const textureInput = $("#textureInput")
                //textureInput['data-grupo'] = props.group;
                textureInput.attr('data-time',props.time);
                textureInput.attr('data-grupo',props.group);
                textureInput.trigger('click')
            } else if (groupsTimeline.get(props.group).type == "float") {
                timelineAddItemToGroup(props.group, props.time);
            }
        }
        return;
    }
    if(props.what === null) {
        timelineAddGroup();
        return;
    }
    if(props.what === "axis") {
        timelineSetCurrentTime(props.time);
        return;
    }
    if(props.what === "group-label") {
        timelineActivateEditGroupName(props.group);
        return;
    }
});

timeline.on('click', function (props) {
    if(props.what === "background") {
        closeMenuConfigItemTimeline();
    }
});

timeline.on('mouseDown', function (props) {
    if(props.event.button === 1 && props.item != null) {
        deleteItemFromTimeline(props.item);
    }
    if(props.event.button === 2 && props.item != null) {
        configItemTimeline(props.item);
    }
});

timeline.on('contextmenu', function (props) {
    if(props.what === "group-label") {
        timelineShowContextMenuOnGroup(props.group);
        return;
    }
});

//timeline.addCustomTime(120000, 'mainTime');
//timeline.setCustomTimeMarker("End <img src='icons/refresh-line.png' style='height: 20px' alt='Loop' id='loopButton' onclick='timeline_des_activate_loop()'>", 'finishLine', false);

timeline.on('timechanged', function (properties) {
    if (properties.id === 'finishLine'){
        stash.set('end',properties.time.getTime());
    }
});

timeline.on('select', function (properties) {
    if(Blockly.selected) Blockly.selected.unselect();   // unselect blocks in blockly
});

timeline.on('rangechange', function (properties) {console.log("AAAAAAAA")
    itemsTimeline.get().forEach( (item) => {
        if(item.start_ms<properties.start.getTime())
            console.log(properties.start.getTime()+"-"+item.start_ms)
    })
});
