function find(qs) {
    return Array.from(document.querySelectorAll(qs));
}


function drawCurve(idCurve){
    const numItemActual = idCurve;
    var curve = curves[numItemActual];
    curve.id= idCurve;
    var draw = function() {
        this.setRandomFill(0.2);
        this.drawCurve(curve);
        this.drawCurve(curve);
        this.saveBase64(numItemActual);
        this.drawSkeleton(curve);
    };
    var code = new CodeExample(idCurve);
    code.draw = draw.bind(code);
    handleInteraction(code.getCanvas(), curve).onupdate = evt => {
        code.reset();
        code.draw(evt);
    };
    code.draw();
}

function drawActualCurve(){
    drawCurve(configurandoItem);
}

function loadAllCurvesNow() {
    var list = find("figure script[type='text/beziercode']");

    list.forEach(function(e, idx) {
        var figure = e.parentNode;
        var code = e.textContent.substring(1).split("\n");
        e.parentNode.removeChild(e);
        var indent = "";
        code[0].replace(/^(\s+)/, function(a,b) { indent = b; });
        var len = code.length;

        code = code.map(l => l.replace(indent,'')).join("\n");

        var content = `
        function auxaaa(){
          var curve = new Bezier${code};
          var draw = function() {
              this.drawSkeleton(curve);
              this.drawCurve(curve);
          };
          var code = new CodeExample(${idx});
          code.draw = draw.bind(code);
          handleInteraction(code.getCanvas(), curve).onupdate = evt => {
            code.reset();
            code.draw(evt);
            //console.log(curve.points);      // TODO Onmouseup guardar la nueva curva en el item
          };
          code.draw();
          }
        `;

        /*var codearea = document.createElement("div");
        codearea.classList.add("textarea");
        codearea.textContent = code;
        codearea.setAttribute("style", "height: " + (16*(len-1)) + "px!important;");
        figure.appendChild(codearea);
        var button = document.createElement("button");
        button.textContent = "view source";
        figure.appendChild(button);

        button.onclick = function(evt) {
            if(open && open!==codearea) { open.classList.remove("showcode"); }
            if(codearea.classList.contains("showcode")) {
                codearea.classList.remove("showcode");
            } else {
                codearea.classList.add("showcode");
                open = codearea;
            }
            evt.stopPropagation();
        };

        document.addEventListener("click", function() {
            if(codearea.classList.contains("showcode")) {
                codearea.classList.remove("showcode");
            }
        });*/

        var ns = document.createElement("script");
        ns.type = "module";
        ns.textContent = content;
        eval(content);
        document.querySelector("head").appendChild(ns);
        return content;
    });
}

document.addEventListener("DOMContentLoaded", loadAllCurvesNow);
