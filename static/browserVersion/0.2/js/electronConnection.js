const electron = require('electron');

electron.ipcRenderer.on('new_project', function(event, message) {
    new_project(true);
});

electron.ipcRenderer.on('save_project', function(event, message) {
    save_project(false);
});

electron.ipcRenderer.on('save_project_as', function(event, message) {
    save_project(true);
});

electron.ipcRenderer.on('open_project', function(event, message) {
    open_project();
});

electron.ipcRenderer.on('run_project', function(event, message) {
    run_project();
});

electron.ipcRenderer.on('export_png', function(event, message) {
    openExporter();
});

electron.ipcRenderer.on('open_preferences', function(event, message) {
    openPreferences();
});

electron.ipcRenderer.on('export_glsl', function(event, message) {
    openGLSLExporter();
});

electron.ipcRenderer.on('set_quality', function(event, message) {
    quality = parseFloat(message);
    if(window_sketch) window_sketch.changeQuality(quality);
});

electron.ipcRenderer.on('des_activar_livecoding', function(event, message) {
    des_activar_livecoding();
});

electron.ipcRenderer.on('import_multimedia', function(event, message) {
    openMultimedia();
});

electron.ipcRenderer.on('des_activar_preview', function(event, message) {
    tooglePreviewMode();
});


electron.ipcRenderer.on('des_activar_endline', function(event, message) {
    timeline_des_activate_end();
});