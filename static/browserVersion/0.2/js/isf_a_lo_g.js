var coreMath = require('mathjs/core');

// Create a new, empty math.js instance
// It will only contain methods `import` and `config`
var mathJsEval = coreMath.create();

mathJsEval.import(require('mathjs/lib/expression/function/parse'));
mathJsEval.import(require('mathjs/lib/expression/function/compile'));
mathJsEval.import(require('mathjs/lib/expression/function/eval'));

mathJsEval.import(require('mathjs/lib/function/string/format'));

// create simple functions for all operators
mathJsEval.import({
    // arithmetic
    add:        function (a, b) { return a + b },
    subtract:   function (a, b) { return a - b },
    multiply:   function (a, b) { return a * b },
    divide:     function (a, b) { return a / b },
    mod:        function (a, b) { return a % b },
    unaryPlus:  function (a) { return  a },
    unaryMinus: function (a) { return -a },

    // bitwise
    bitOr:           function (a, b) { return a | b },
    bitXor:          function (a, b) { return a ^ b },
    bitAnd:          function (a, b) { return a & b },
    bitNot:          function (a) { return ~a },
    leftShift:       function (a, b) { return a << b },
    rightArithShift: function (a, b) { return a >> b },
    rightLogShift:   function (a, b) { return a >>> b },

    // logical
    or:  function (a, b) { return !!(a || b) },
    xor: function (a, b) { return !!a !== !!b },
    and: function (a, b) { return !!(a && b) },
    not: function (a) { return !a },

    // relational
    equal:     function (a, b) { return a == b },
    unequal:   function (a, b) { return a != b },
    smaller:   function (a, b) { return a < b },
    larger:    function (a, b) { return a > b },
    smallerEq: function (a, b) { return a <= b },
    largerEq:  function (a, b) { return a >= b },

    // matrix
    // matrix: function (a) { return a },
    matrix: function () {
        throw new Error('Matrices not supported')
    },
    index: function () {
        // TODO: create a simple index function
        throw new Error('Matrix indexes not supported')
    },

    // add pi and e as lowercase
    pi: Math.PI,
    e: Math.E,
    'true': true,
    'false': false,
    'null': null
})

// import everything from Math (like trigonometric functions)
var allFromMath = {};
Object.getOwnPropertyNames(Math).forEach(function (name) {
    // filter out stuff like Firefox's "toSource" method.
    if (!Object.prototype.hasOwnProperty(name)) {
        allFromMath[name] = Math[name];
    }
});
mathJsEval.import(allFromMath);

function jsonParse() {
    "use strict";

// This is a function that can parse a JSON text, producing a JavaScript
// data structure. It is a simple, recursive descent parser. It does not use
// eval or regular expressions, so it can be used as a model for implementing
// a JSON parser in other languages.

// We are defining the function inside of another function to avoid creating
// global variables.

    var at;     // The index of the current character
    var ch;     // The current character
    var escapee = {
        "\"": "\"",
        "\\": "\\",
        "/": "/",
        b: "\b",
        f: "\f",
        n: "\n",
        r: "\r",
        t: "\t"
    };
    var text;

    var error = function (m) {

// Call error when something is wrong.

        throw {
            name: "SyntaxError",
            message: m,
            at: at,
            text: text
        };
    };

    var next = function (c) {

// If a c parameter is provided, verify that it matches the current character.

        if (c && c !== ch) {
            error("Expected '" + c + "' instead of '" + ch + "'");
        }

// Get the next character. When there are no more characters,
// return the empty string.

        ch = text.charAt(at);
        at += 1;
        return ch;
    };

    var number = function () {

// Parse a number value.

        var value;
        var string = "";

        if (ch === "-") {
            string = "-";
            next("-");
        }
        while (ch >= "0" && ch <= "9") {
            string += ch;
            next();
        }
        if (ch === ".") {
            string += ".";
            while (next() && ch >= "0" && ch <= "9") {
                string += ch;
            }
        }
        if (ch === "e" || ch === "E") {
            string += ch;
            next();
            if (ch === "-" || ch === "+") {
                string += ch;
                next();
            }
            while (ch >= "0" && ch <= "9") {
                string += ch;
                next();
            }
        }
        value = +string;
        if (!isFinite(value)) {
            error("Bad number");
        } else {
            return value;
        }
    };

    var string = function () {

// Parse a string value.

        var hex;
        var i;
        var value = "";
        var uffff;

// When parsing for string values, we must look for " and \ characters.

        if (ch === "\"") {
            while (next()) {
                if (ch === "\"") {
                    next();
                    return value;
                }
                if (ch === "\\") {
                    next();
                    if (ch === "u") {
                        uffff = 0;
                        for (i = 0; i < 4; i += 1) {
                            hex = parseInt(next(), 16);
                            if (!isFinite(hex)) {
                                break;
                            }
                            uffff = uffff * 16 + hex;
                        }
                        value += String.fromCharCode(uffff);
                    } else if (typeof escapee[ch] === "string") {
                        value += escapee[ch];
                    } else {
                        break;
                    }
                } else {
                    value += ch;
                }
            }
        }
        error("Bad string");
    };

    var white = function () {

// Skip whitespace.

        while (ch && ch <= " ") {
            next();
        }
    };

    var word = function () {

// true, false, or null.

        switch (ch) {
            case "t":
                next("t");
                next("r");
                next("u");
                next("e");
                return true;
            case "f":
                next("f");
                next("a");
                next("l");
                next("s");
                next("e");
                return false;
            case "n":
                next("n");
                next("u");
                next("l");
                next("l");
                return null;
        }
        error("Unexpected '" + ch + "'");
    };

    var value;  // Place holder for the value function.

    var array = function () {

// Parse an array value.

        var arr = [];

        if (ch === "[") {
            next("[");
            white();
            if (ch === "]") {
                next("]");
                return arr;   // empty array
            }
            while (ch) {
                arr.push(value());
                white();
                if (ch === "]") {
                    next("]");
                    return arr;
                }
                next(",");
                white();
            }
        }
        error("Bad array");
    };

    var object = function () {

// Parse an object value.

        var key;
        var obj = {};

        if (ch === "{") {
            next("{");
            white();
            if (ch === "}") {
                next("}");
                return obj;   // empty object
            }
            while (ch) {
                key = string();
                white();
                next(":");
                if (Object.hasOwnProperty.call(obj, key)) {
                    error("Duplicate key '" + key + "'");
                }
                obj[key] = value();
                white();
                if (ch === "}") {
                    next("}");
                    return obj;
                }
                next(",");
                white();
            }
        }
        error("Bad object");
    };

    value = function () {

// Parse a JSON value. It could be an object, an array, a string, a number,
// or a word.

        white();
        switch (ch) {
            case "{":
                return object();
            case "[":
                return array();
            case "\"":
                return string();
            case "-":
                return number();
            default:
                return (ch >= "0" && ch <= "9")
                    ? number()
                    : word();
        }
    };

// Return the json_parse function. It will have access to all of the above
// functions and variables.

    return function (source, reviver) {
        var result;

        text = source;
        at = 0;
        ch = " ";
        result = value();
        white();
        if (ch) {
            error("Syntax error");
        }

// If there is a reviver function, we recursively walk the new structure,
// passing each name/value pair to the reviver function for possible
// transformation, starting with a temporary root object that holds the result
// in an empty key. If there is not a reviver function, we simply return the
// result.

        return (typeof reviver === "function")
            ? (function walk(holder, key) {
                var k;
                var v;
                var val = holder[key];
                if (val && typeof val === "object") {
                    for (k in val) {
                        if (Object.prototype.hasOwnProperty.call(val, k)) {
                            v = walk(val, k);
                            if (v !== undefined) {
                                val[k] = v;
                            } else {
                                delete val[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, val);
            }({"": result}, ""))
            : result;
    };
};

function MetadataExtractor(rawFragmentShader) {
    // First pull out the comment JSON to get the metadata.
    // This regex (should) match quotes in the form /* */.
    const regex = /\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/;
    const results = regex.exec(rawFragmentShader);

    if (!results) {
        throw new Error('There is no metadata here.');
    }

    const METADATA_ERROR_PREFIX = 'Something is wrong with your ISF metadata';
    let metadataString = results[0];
    metadataString = metadataString.substring(1, metadataString.length - 1);
    let metadata;
    try {
        metadata = jsonParse(metadataString);
    } catch (e) {
        const loc = e.at;
        const message = (e.message || 'Invalid JSON');
        if (loc) {
            const lines = (metadataString || '')
                .substring(0, loc)
                .split(/\r\n|\r|\n/);
            const lineNumber = lines.length;
            const position = lines[lineNumber - 1].length;
            const errorText = `${METADATA_ERROR_PREFIX}: ${message}\
        at line ${lineNumber} and position ${position}`;
            const enrichedError = new Error(errorText);
            enrichedError.lineNumber = lineNumber;
            enrichedError.position = position;
            throw enrichedError;
        }
        throw new Error(`${METADATA_ERROR_PREFIX}: ${message}`);
    }

    const startIndex = rawFragmentShader.indexOf('/*');
    const endIndex = rawFragmentShader.indexOf('*/');
    return {
        objectValue: metadata,
        stringValue: metadataString,
        startIndex,
        endIndex,
    };
};

function ISFTexture(params, contextState) {
    if (params == null) {
        params = {};
    }
    this.contextState = contextState;
    this.float = params.float;
    this.gl = this.contextState.gl;
    this.texture = this.gl.createTexture();
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
    this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, true);
    this.gl.bindTexture(this.gl.TEXTURE_2D, null);
}

ISFTexture.prototype.bind = function textureBind(location) {
    if (location === null || location === undefined) {
        location = -1;
    }
    const newTexUnit = this.contextState.newTextureIndex();
    this.gl.activeTexture(this.gl.TEXTURE0 + newTexUnit);
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
    if (location !== -1) {
        this.gl.uniform1i(location, newTexUnit);
    }
};

ISFTexture.prototype.setSize = function setSize(w, h) {
    if (this.width !== w || this.height !== h) {
        this.width = w;
        this.height = h;
        const pixelType = this.float ? this.gl.FLOAT : this.gl.UNSIGNED_BYTE;
        this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
        this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, w, h, 0, this.gl.RGBA, pixelType, null);
    }
};

ISFTexture.prototype.destroy = function destroy() {
    this.gl.deleteTexture(this.texture);
};


function ISFBuffer(pass, contextState) {
    this.contextState = contextState;
    this.gl = this.contextState.gl;
    this.persistent = pass.persistent;
    // Since float buffers have a lot of problems in webgl we dont actually use them.
    // This should be revisited.
    // this.float = pass.float;
    this.name = pass.target;
    this.textures = [];
    this.textures.push(new ISFTexture(pass, this.contextState));
    this.textures.push(new ISFTexture(pass, this.contextState));
    this.flipFlop = false;
    this.fbo = this.gl.createFramebuffer();
    this.flipFlop = false;
}

ISFBuffer.prototype.setSize = function setSize(w, h) {
    if (this.width !== w || this.height !== h) {
        this.width = w;
        this.height = h;
        for (let i = 0; i < this.textures.length; i++) {
            const texture = this.textures[i];
            texture.setSize(w, h);
        }
    }
};

ISFBuffer.prototype.readTexture = function readTexture() {
    if (this.flipFlop) {
        return this.textures[1];
    }
    return this.textures[0];
};

ISFBuffer.prototype.writeTexture = function writeTexture() {
    if (!this.flipFlop) {
        return this.textures[1];
    }
    return this.textures[0];
};

ISFBuffer.prototype.flip = function flip() {
    this.flipFlop = !this.flipFlop;
};

ISFBuffer.prototype.destroy = function destroy() {
    for (let i = 0; i < this.textures.length; i++) {
        const texture = this.textures[i];
        texture.destroy();
    }
    this.gl.deleteFramebuffer(this.fbo);
};

function ISFGLProgram(gl, vs, fs) {
    this.gl = gl;
    this.vShader = this.createShader(vs, this.gl.VERTEX_SHADER);
    this.fShader = this.createShader(fs, this.gl.FRAGMENT_SHADER);
    this.program = this.createProgram(this.vShader, this.fShader);
    this.locations = {};
}

ISFGLProgram.prototype.use = function glProgramUse() {
    this.gl.useProgram(this.program);
};

ISFGLProgram.prototype.getUniformLocation = function getUniformLocation(name) {
    return this.gl.getUniformLocation(this.program, name);
};

ISFGLProgram.prototype.bindVertices = function bindVertices() {
    this.use();
    const positionLocation = this.gl.getAttribLocation(this.program, 'isf_position');
    this.buffer = this.gl.createBuffer();
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.buffer);
    const vertexArray = new Float32Array(
        [-1.0, -1.0, 1.0,
            -1.0, -1.0, 1.0,
            -1.0, 1.0, 1.0,
            -1.0, 1.0, 1.0]);
    this.gl.bufferData(this.gl.ARRAY_BUFFER, vertexArray, this.gl.STATIC_DRAW);
    this.gl.enableVertexAttribArray(positionLocation);
    this.gl.vertexAttribPointer(positionLocation, 2, this.gl.FLOAT, false, 0, 0);
};

ISFGLProgram.prototype.cleanup = function cleanup() {
    this.gl.deleteShader(this.fShader);
    this.gl.deleteShader(this.vShader);
    this.gl.deleteProgram(this.program);
    this.gl.deleteBuffer(this.buffer);
};

ISFGLProgram.prototype.createShader = function createShader(src, type) {
    const shader = this.gl.createShader(type);
    this.gl.shaderSource(shader, src);
    this.gl.compileShader(shader);
    const compiled = this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS);
    if (!compiled) {
        const lastError = this.gl.getShaderInfoLog(shader);
        console.log('Error Compiling Shader ', lastError);
        throw new Error({
            message: lastError,
            type: 'shader',
        });
    }
    return shader;
};

ISFGLProgram.prototype.createProgram = function createProgram(vShader, fShader) {
    const program = this.gl.createProgram();
    this.gl.attachShader(program, vShader);
    this.gl.attachShader(program, fShader);
    this.gl.linkProgram(program);
    const linked = this.gl.getProgramParameter(program, this.gl.LINK_STATUS);
    if (!linked) {
        const lastError = this.gl.getProgramInfoLog(program);
        console.log('Error in program linking', lastError);
        throw new Error({
            message: lastError,
            type: 'program',
        });
    }
    return program;
};

const ISFGLState = function ISFGLState(gl) {
    this.gl = gl;
    this.textureIndex = 0;
};

ISFGLState.prototype.newTextureIndex = function newTextureIndex() {
    const i = this.textureIndex;
    this.textureIndex += 1;
    return i;
};

ISFGLState.prototype.reset = function reset() {
    this.textureIndex = 0;
};

function getMainLine(src) {
    const lines = src.split('\n');
    for (let i = 0; i < lines.length; i++) {
        console.log('line', lines[i]);
        if (lines[i].indexOf('main()') !== -1) return i;
    }
    return -1;
}

function mapGLErrorToISFLine(error, glsl, isf) {
    const glslMainLine = getMainLine(glsl);
    const isfMainLine = getMainLine(isf);
    const regex = /ERROR: (\d+):(\d+): (.*)/g;
    const matches = regex.exec(error.message);
    const glslErrorLine = matches[2];
    const isfErrorLine = parseInt(glslErrorLine, 10) + isfMainLine - glslMainLine;
    return isfErrorLine;
}


const typeUniformMap = {
    float: 'float',
    image: 'sampler2D',
    bool: 'bool',
    event: 'bool',
    long: 'int',
    color: 'vec4',
    point2D: 'vec2',
};

const ISFParser = function ISFParser() {};

ISFParser.prototype.parse = function parse(rawFragmentShader, rawVertexShader) {
    try {
        this.valid = true;
        this.rawFragmentShader = rawFragmentShader;
        this.rawVertexShader = rawVertexShader || ISFParser.vertexShaderDefault;
        this.error = null;
        const metadataInfo = MetadataExtractor(this.rawFragmentShader);
        const metadata = metadataInfo.objectValue;
        const metadataString = metadataInfo.stringValue;
        this.metadata = metadata;
        this.credit = metadata.CREDIT;
        this.categories = metadata.CATEGORIES;
        this.inputs = metadata.INPUTS;
        this.imports = (metadata.IMPORTED || {});
        this.description = metadata.DESCRIPTION;

        const passesArray = metadata.PASSES || [{}];
        this.passes = this.parsePasses(passesArray);
        const endOfMetadata =
            this.rawFragmentShader.indexOf(metadataString) + metadataString.length + 2;
        this.rawFragmentMain = this.rawFragmentShader.substring(endOfMetadata);
        this.generateShaders();
        this.inferFilterType();
        this.isfVersion = this.inferISFVersion();
    } catch (e) {
        this.valid = false;
        this.error = e;
        this.inputs = [];
        this.categories = [];
        this.credit = '';
        this.errorLine = e.lineNumber;
    }
};

ISFParser.prototype.parsePasses = function parsePasses(passesArray) {
    const passes = [];
    for (let i = 0; i < passesArray.length; ++i) {
        const passDefinition = passesArray[i];
        const pass = { };
        if (passDefinition.TARGET) pass.target = passDefinition.TARGET;
        pass.persistent = !!passDefinition.PERSISTENT;
        pass.width = passDefinition.WIDTH || '$WIDTH';
        pass.height = passDefinition.HEIGHT || '$HEIGHT';
        pass.float = !!passDefinition.FLOAT;
        passes.push(pass);
    }
    return passes;
};

ISFParser.prototype.generateShaders = function generateShaders() {
    this.uniformDefs = '';
    for (let i = 0; i < this.inputs.length; ++i) {
        this.addUniform(this.inputs[i]);
    }

    for (let i = 0; i < this.passes.length; ++i) {
        if (this.passes[i].target) {
            this.addUniform({ NAME: this.passes[i].target, TYPE: 'image' });
        }
    }

    for (const k in this.imports) {
        if ({}.hasOwnProperty.call(this.imports, k)) {
            this.addUniform({ NAME: k, TYPE: 'image' });
        }
    }

    this.fragmentShader = this.buildFragmentShader();
    this.vertexShader = this.buildVertexShader();
};

ISFParser.prototype.addUniform = function addUniform(input) {
    const type = this.inputToType(input.TYPE);
    this.addUniformLine(`uniform ${type} ${input.NAME};`);
    if (type === 'sampler2D') {
        this.addUniformLine(this.samplerUniforms(input));
    }
};

ISFParser.prototype.addUniformLine = function addUniformLine(line) {
    this.uniformDefs += `${line}\n`;
};

ISFParser.prototype.samplerUniforms = function samplerUniforms(input) {
    const name = input.NAME;
    let lines = '';
    lines += `uniform vec4 _${name}_imgRect;\n`;
    lines += `uniform vec2 _${name}_imgSize;\n`;
    lines += `uniform bool _${name}_flip;\n`;
    lines += `varying vec2 _${name}_normTexCoord;\n`;
    lines += `varying vec2 _${name}_texCoord;\n`;
    lines += '\n';
    return lines;
};

ISFParser.prototype.buildFragmentShader = function buildFragmentShader() {
    const main = this.replaceSpecialFunctions(this.rawFragmentMain);
    return ISFParser.fragmentShaderSkeleton.replace('[[uniforms]]', this.uniformDefs).replace('[[main]]', main);
};

ISFParser.prototype.replaceSpecialFunctions = function replaceSpecialFunctions(source) {
    let regex;

    // IMG_THIS_PIXEL
    regex = /IMG_THIS_PIXEL\((.+?)\)/g;
    source = source.replace(regex, (fullMatch, innerMatch) => `texture2D(${innerMatch}, isf_FragNormCoord)`);

    // IMG_THIS_NORM_PIXEL
    regex = /IMG_THIS_NORM_PIXEL\((.+?)\)/g;
    source = source.replace(regex, (fullMatch, innerMatch) => `texture2D(${innerMatch}, isf_FragNormCoord)`);

    // IMG_PIXEL
    regex = /IMG_PIXEL\((.+?)\)/g;
    source = source.replace(regex, (fullMatch, innerMatch) => {
        const results = innerMatch.split(',');
        const sampler = results[0];
        const coord = results[1];
        return `texture2D(${sampler}, (${coord}) / RENDERSIZE)`;
    });

    // IMG_NORM_PIXEL
    regex = /IMG_NORM_PIXEL\((.+?)\)/g;
    source = source.replace(regex, (fullMatch, innerMatch) => {
        const results = innerMatch.split(',');
        const sampler = results[0];
        const coord = results[1];
        return `VVSAMPLER_2DBYNORM(${sampler}, _${sampler}_imgRect, _${sampler}_imgSize, _${sampler}_flip, ${coord})`;
    });

    // IMG_SIZE
    regex = /IMG_SIZE\((.+?)\)/g;
    source = source.replace(regex, (fullMatch, imgName) => {
        return `_${imgName}_imgSize`;
    });
    return source;
};

ISFParser.prototype.buildVertexShader = function buildVertexShader() {
    let functionLines = '\n';
    for (let i = 0; i < this.inputs.length; ++i) {
        const input = this.inputs[i];
        if (input.TYPE === 'image') {
            functionLines += `${this.texCoordFunctions(input)}\n`;
        }
    }
    return ISFParser.vertexShaderSkeleton.replace('[[functions]]', functionLines).replace('[[uniforms]]', this.uniformDefs).replace('[[main]]', this.rawVertexShader);
};

ISFParser.prototype.texCoordFunctions = function texCoordFunctions(input) {
    const name = input.NAME;
    return [
        '_[[name]]_texCoord =',
        '    vec2(((isf_fragCoord.x / _[[name]]_imgSize.x * _[[name]]_imgRect.z) + _[[name]]_imgRect.x), ',
        '          (isf_fragCoord.y / _[[name]]_imgSize.y * _[[name]]_imgRect.w) + _[[name]]_imgRect.y);',
        '',
        '_[[name]]_normTexCoord =',
        '  vec2((((isf_FragNormCoord.x * _[[name]]_imgSize.x) / _[[name]]_imgSize.x * _[[name]]_imgRect.z) + _[[name]]_imgRect.x),',
        '          ((isf_FragNormCoord.y * _[[name]]_imgSize.y) / _[[name]]_imgSize.y * _[[name]]_imgRect.w) + _[[name]]_imgRect.y);',
    ].join('\n').replace(/\[\[name\]\]/g, name);
};

ISFParser.prototype.inferFilterType = function inferFilterType() {
    function any(arr, test) {
        return arr.filter(test).length > 0;
    }
    const isFilter = any(this.inputs, input => input.TYPE === 'image' && input.NAME === 'inputImage');
    const isTransition =
        any(this.inputs, input => input.TYPE === 'image' && input.NAME === 'startImage')
        &&
        any(this.inputs, input => input.TYPE === 'image' && input.NAME === 'endImage')
        &&
        any(this.inputs, input => input.TYPE === 'float' && input.NAME === 'progress');
    if (isFilter) {
        this.type = 'filter';
    } else if (isTransition) {
        this.type = 'transition';
    } else {
        this.type = 'generator';
    }
};

ISFParser.prototype.inferISFVersion = function inferISFVersion() {
    let v = 2;
    if (this.metadata.PERSISTENT_BUFFERS ||
        this.rawFragmentShader.indexOf('vv_FragNormCoord') !== -1 ||
        this.rawVertexShader.indexOf('vv_vertShaderInit') !== -1 ||
        this.rawVertexShader.indexOf('vv_FragNormCoord') !== -1) {
        v = 1;
    }
    return v;
};

ISFParser.prototype.inputToType = function inputToType(inputType) {
    const type = typeUniformMap[inputType];
    if (!type) throw new Error(`Unknown input type [${inputType}]`);
    return type;
};

ISFParser.fragmentShaderSkeleton = `
precision highp float;
precision highp int;

uniform int PASSINDEX;
uniform vec2 RENDERSIZE;
varying vec2 isf_FragNormCoord;
varying vec2 isf_FragCoord;
uniform float TIME;
uniform float TIMEDELTA;
uniform int FRAMEINDEX;
uniform vec4 DATE;

[[uniforms]]

// We don't need 2DRect functions since we control all inputs.  Don't need flip either, but leaving
// for consistency sake.
vec4 VVSAMPLER_2DBYPIXEL(sampler2D sampler, vec4 samplerImgRect, vec2 samplerImgSize, bool samplerFlip, vec2 loc) {
  return (samplerFlip)
    ? texture2D   (sampler,vec2(((loc.x/samplerImgSize.x*samplerImgRect.z)+samplerImgRect.x), (samplerImgRect.w-(loc.y/samplerImgSize.y*samplerImgRect.w)+samplerImgRect.y)))
    : texture2D   (sampler,vec2(((loc.x/samplerImgSize.x*samplerImgRect.z)+samplerImgRect.x), ((loc.y/samplerImgSize.y*samplerImgRect.w)+samplerImgRect.y)));
}
vec4 VVSAMPLER_2DBYNORM(sampler2D sampler, vec4 samplerImgRect, vec2 samplerImgSize, bool samplerFlip, vec2 normLoc)  {
  vec4    returnMe = VVSAMPLER_2DBYPIXEL(   sampler,samplerImgRect,samplerImgSize,samplerFlip,vec2(normLoc.x*samplerImgSize.x, normLoc.y*samplerImgSize.y));
  return returnMe;
}

[[main]]

`;

ISFParser.vertexShaderDefault = `
void main() {
  isf_vertShaderInit();
}
`;
ISFParser.vertexShaderSkeleton = `
precision highp float;
precision highp int;
void isf_vertShaderInit();

attribute vec2 isf_position; // -1..1

uniform int     PASSINDEX;
uniform vec2    RENDERSIZE;
varying vec2    isf_FragNormCoord; // 0..1
vec2    isf_fragCoord; // Pixel Space

[[uniforms]]

[[main]]
void isf_vertShaderInit(void)  {
gl_Position = vec4( isf_position, 0.0, 1.0 );
  isf_FragNormCoord = vec2((gl_Position.x+1.0)/2.0, (gl_Position.y+1.0)/2.0);
  isf_fragCoord = floor(isf_FragNormCoord * RENDERSIZE);
  [[functions]]
}
`;

const ConvertFragment = function ConvertFragment(fragShader) {
    const metadataInfo = MetadataExtractor(fragShader);
    const meta = metadataInfo.objectValue;
    const persistentBufferNames = meta.PERSISTENT_BUFFERS || [];
    if (meta.PASSES) {
        meta.PASSES.forEach((pass) => {
            if (persistentBufferNames.indexOf(pass.TARGET) !== -1) pass.persistent = true;
        });
    }
    delete meta.PERSISTENT_BUFFERS;
    fragShader = fragShader.replace(metadataInfo.stringValue, JSON.stringify(meta, null, 2));
    fragShader = fragShader.replace(/vv_FragNormCoord/g, 'isf_FragNormCoord');
    return fragShader;
};

const ConvertVertex = function ConvertVertex(vertShader) {
    vertShader = vertShader.replace(/vv_vertShaderInit/g, 'isf_vertShaderInit');
    vertShader = vertShader.replace(/vv_FragNormCoord/g, 'isf_FragNormCoord');
    return vertShader;
};


function ISFRenderer(gl) {
    this.gl = gl;
    this.uniforms = [];
    this.contextState = new ISFGLState(this.gl);
    this.setupPaintToScreen();
    this.startTime = Date.now();
    this.lastRenderTime = Date.now();
    this.frameIndex = 0;
    //this.loadSource(this.basicFragmentShader);
}

ISFRenderer.prototype.loadSource = function loadSource(fragmentISF, vertexISFOpt) {
    const parser = new ISFParser();
    parser.parse(fragmentISF, vertexISFOpt);
    this.sourceChanged(parser.fragmentShader, parser.vertexShader, parser);
};

ISFRenderer.prototype.sourceChanged = function sourceChanged(fragmentShader, vertexShader, model) {
    this.fragmentShader = fragmentShader;
    this.vertexShader = vertexShader;
    this.model = model;
    if (!this.model.valid) {
        this.valid = false;
        this.error = this.model.error;
        this.errorLine = this.model.errorLine;
        return;
    }
    try {
        this.valid = true;
        this.error = null;
        this.errorLine = null;
        this.setupGL();
        this.initUniforms();
        for (let i = 0; i < model.inputs.length; i++) {
            const input = model.inputs[i];
            if (input.DEFAULT !== undefined) {
                this.setValue(input.NAME, input.DEFAULT);
            }
        }
    } catch (e) {
        this.valid = false;
        this.error = e;
        this.errorLine = LineMapper(e, this.fragmentShader, this.model.rawFragmentShader);
    }
};

ISFRenderer.prototype.initUniforms = function initUniforms() {
    this.uniforms = this.findUniforms(this.fragmentShader);
    const inputs = this.model.inputs;
    for (let i = 0; i < inputs.length; ++i) {
        const input = inputs[i];
        const uniform = this.uniforms[input.NAME];
        if (!uniform) {
            continue;
        }
        uniform.value = this.model[input.NAME];
        if (uniform.type === 't') {
            uniform.texture = new ISFTexture({}, this.contextState);
        }
    }
    this.pushTextures();
};

ISFRenderer.prototype.setValue = function setValue(name, value) {
    this.program.use();

    const uniform = this.uniforms[name];
    if (!uniform) {
        console.error(`No uniform named ${name}`);
        return;
    }
    uniform.value = value;
    if (uniform.type === 't') {
        uniform.textureLoaded = false;
    }
    this.pushUniform(uniform);
};

ISFRenderer.prototype.setNormalizedValue = function setNormalizedValue(name, normalizedValue) {
    const inputs = this.model.inputs;
    let input = null;
    for (let i = 0; i < inputs.length; i++) {
        const thisInput = inputs[i];
        if (thisInput.NAME === name) {
            input = thisInput;
            break;
        }
    }
    if (input && input.MIN !== undefined && input.MAX !== undefined) {
        this.setValue(name, input.MIN + (input.MAX - input.MIN) * normalizedValue);
    } else {
        console.log('Trying to set normalized value without MIN and MAX input', name, input);
    }
};

ISFRenderer.prototype.setupPaintToScreen = function setupPaintToScreen() {
    this.paintProgram = new ISFGLProgram(this.gl, this.basicVertexShader, this.basicFragmentShader);
    return this.paintProgram.bindVertices();
};

ISFRenderer.prototype.setupGL = function setupGL() {
    this.cleanup();
    this.program = new ISFGLProgram(this.gl, this.vertexShader, this.fragmentShader);
    this.program.bindVertices();
    this.generatePersistentBuffers();
};

ISFRenderer.prototype.generatePersistentBuffers = function generatePersistentBuffers() {
    this.renderBuffers = [];
    const passes = this.model.passes;
    for (let i = 0; i < passes.length; ++i) {
        const pass = passes[i];
        const buffer = new ISFBuffer(pass, this.contextState);
        pass.buffer = buffer;
        this.renderBuffers.push(buffer);
    }
};

ISFRenderer.prototype.paintToScreen = function paintToScreen(destination, target) {
    this.paintProgram.use();
    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
    this.gl.viewport(0, 0, destination.width, destination.height);
    const loc = this.paintProgram.getUniformLocation('tex');
    target.readTexture().bind(loc);
    this.gl.drawArrays(this.gl.TRIANGLES, 0, 6);
    this.program.use();
};

ISFRenderer.prototype.pushTextures = function pushTextures() {
    Object.keys(this.uniforms).forEach((u) => {
        const uniform = this.uniforms[u];
        if (uniform.type === 't') this.pushTexture(uniform);
    });
};

ISFRenderer.prototype.pushTexture = function pushTexture(uniform) {
    if (!uniform.value) {
        return;
    }

    if (
        uniform.value.constructor.name !== 'OffscreenCanvas' &&
        (
            uniform.value.tagName !== 'CANVAS' &&
            !uniform.value.complete &&
            uniform.value.readyState !== 4)
    ) {
        return;
    }

    const loc = this.program.getUniformLocation(uniform.name);
    uniform.texture.bind(loc);
    this.gl.texImage2D(
        this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, uniform.value);
    if (!uniform.textureLoaded) {
        const img = uniform.value;
        uniform.textureLoaded = true;
        const w = img.naturalWidth || img.width || img.videoWidth;
        const h = img.naturalHeight || img.height || img.videoHeight;
        this.setValue(`_${uniform.name}_imgSize`, [w, h]);
        this.setValue(`_${uniform.name}_imgRect`, [0, 0, 1, 1]);
        this.setValue(`_${uniform.name}_flip`, false);
    }
};

ISFRenderer.prototype.pushUniforms = function pushUniforms() {
    for (const uniform of this.uniforms) {
        this.pushUniform(uniform);
    }
};

ISFRenderer.prototype.pushUniform = function pushUniform(uniform) {
    const loc = this.program.getUniformLocation(uniform.name);
    if (loc !== -1) {
        if (uniform.type === 't') {
            this.pushTexture(uniform);
            return;
        }
        const v = uniform.value;
        switch (uniform.type) {
            case 'f':
                this.gl.uniform1f(loc, v);
                break;
            case 'v2':
                this.gl.uniform2f(loc, v[0], v[1]);
                break;
            case 'v3':
                this.gl.uniform3f(loc, v[0], v[1], v[2]);
                break;
            case 'v4':
                this.gl.uniform4f(loc, v[0], v[1], v[2], v[3]);
                break;
            case 'i':
                this.gl.uniform1i(loc, v);
                break;
            case 'color':
                this.gl.uniform4f(loc, v[0], v[1], v[2], v[3]);
                break;
            default:
                console.log(`Unknown type for uniform setting ${uniform.type}`, uniform);
                break;
        }
    }
};

ISFRenderer.prototype.findUniforms = function findUniforms(shader) {
    const lines = shader.split('\n');
    const uniforms = {};
    const len = lines.length;
    for (let i = 0; i < len; ++i) {
        const line = lines[i].trim();
        if (line.indexOf('uniform') === 0) {
            const tokens = line.split(' ');
            const name = tokens[2].substring(0, tokens[2].length - 1);
            const uniform = this.typeToUniform(tokens[1]);
            uniform.name = name;
            uniforms[name] = uniform;
        }
    }
    return uniforms;
};

ISFRenderer.prototype.typeToUniform = function typeToUniform(type) {
    switch (type) {
        case 'float':
            return {
                type: 'f',
                value: 0,
            };
        case 'vec2':
            return {
                type: 'v2',
                value: [0, 0],
            };
        case 'vec3':
            return {
                type: 'v3',
                value: [0, 0, 0],
            };
        case 'vec4':
            return {
                type: 'v4',
                value: [0, 0, 0, 0],
            };
        case 'bool':
            return {
                type: 'i',
                value: 0,
            };
        case 'int':
            return {
                type: 'i',
                value: 0,
            };
        case 'color':
            return {
                type: 'v4',
                value: [0, 0, 0, 0],
            };
        case 'point2D':
            return {
                type: 'v2',
                value: [0, 0],
                isPoint: true,
            };
        case 'sampler2D':
            return {
                type: 't',
                value: {
                    complete: false,
                    readyState: 0,
                },
                texture: null,
                textureUnit: null,
            };
        default:
            throw new Error(`Unknown uniform type in ISFRenderer.typeToUniform: ${type}`);
    }
};

ISFRenderer.prototype.setDateUniforms = function setDateUniforms() {
    const now = Date.now();
    this.setValue('TIME', (now - this.startTime) / 1000);
    this.setValue('TIMEDELTA', (now - this.lastRenderTime) / 1000);
    this.setValue('FRAMEINDEX', this.frameIndex++);
    const date = new Date();
    this.setValue('DATE', [date.getFullYear(), date.getMonth() + 1, date.getDate(), date.getHours() * 3600 + date.getMinutes() * 60 + date.getSeconds()]);
    this.lastRenderTime = now;
};

ISFRenderer.prototype.draw = function draw(destination) {
    this.contextState.reset();
    this.program.use();
    this.setDateUniforms();

    const buffers = this.renderBuffers;
    for (let i = 0; i < buffers.length; ++i) {
        const buffer = buffers[i];
        const readTexture = buffer.readTexture();
        const loc = this.program.getUniformLocation(buffer.name);
        readTexture.bind(loc);
        if (buffer.name) {
            this.setValue(`_${buffer.name}_imgSize`, [buffer.width, buffer.height]);
            this.setValue(`_${buffer.name}_imgRect`, [0, 0, 1, 1]);
            this.setValue(`_${buffer.name}_flip`, false);
        }
    }
    let lastTarget = null;
    const passes = this.model.passes;
    for (let i = 0; i < passes.length; ++i) {
        const pass = passes[i];
        this.setValue('PASSINDEX', i);
        const buffer = pass.buffer;
        if (pass.target) {
            const w = this.evaluateSize(destination, pass.width);
            const h = this.evaluateSize(destination, pass.height);
            buffer.setSize(w, h);
            const writeTexture = buffer.writeTexture();
            this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, buffer.fbo);
            this.gl.framebufferTexture2D(
                this.gl.FRAMEBUFFER,
                this.gl.COLOR_ATTACHMENT0,
                this.gl.TEXTURE_2D,
                writeTexture.texture,
                0);
            this.setValue('RENDERSIZE', [buffer.width, buffer.height]);
            lastTarget = buffer;
            this.gl.viewport(0, 0, w, h);
        } else {
            const renderWidth = destination.width;
            const renderHeight = destination.height;
            this.gl.bindTexture(this.gl.TEXTURE_2D, null);
            this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
            this.setValue('RENDERSIZE', [renderWidth, renderHeight]);
            lastTarget = null;
            this.gl.viewport(0, 0, renderWidth, renderHeight);
        }
        this.gl.drawArrays(this.gl.TRIANGLES, 0, 6);
        this.gl.bindTexture(this.gl.TEXTURE_2D, null);
        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
    }

    for (let i = 0; i < buffers.length; ++i) {
        buffers[i].flip();
    }
    if (lastTarget) {
        this.paintToScreen(destination, lastTarget);
    }
};

ISFRenderer.prototype.evaluateSize = function evaluateSize(destination, formula) {
    formula += '';
    let s = formula.replace('$WIDTH', destination.offsetWidth || destination.width).replace('$HEIGHT', destination.offsetHeight || destination.height);
    for (const name in this.uniforms) {
        if ({}.hasOwnProperty.call(this.uniforms, name)) {
            const uniform = this.uniforms[name];
            s = s.replace(`$${name}`, uniform.value);
        }
    }

    return mathJsEval(s);
};

ISFRenderer.prototype.cleanup = function cleanup() {
    this.contextState.reset();
    if (this.renderBuffers) {
        for (let i = 0; i < this.renderBuffers.length; ++i) {
            this.renderBuffers[i].destroy();
        }
    }
};

ISFRenderer.prototype.basicVertexShader = "precision mediump float;\nprecision mediump int;\nattribute vec2 isf_position; // -1..1\nvarying vec2 texCoord;\n\nvoid main(void) {\n  // Since webgl doesn't support ftransform, we do this by hand.\n  gl_Position = vec4(isf_position, 0, 1);\n  texCoord = isf_position;\n}\n";

ISFRenderer.prototype.basicFragmentShader = 'precision mediump float;\nuniform sampler2D tex;\nvarying vec2 texCoord;\nvoid main()\n{\n  gl_FragColor = texture2D(tex, texCoord * 0.5 + 0.5);\n  //gl_FragColor = vec4(texCoord.x);\n}';