function TD_slider01(conf) {
    this.getHTML = (function (){
        var name = conf.name;
        var value = conf.value;
        var res = "<div id='tune_"+name+"_div'>";
        res+="<label id='tune_"+name+"_label' for='tune_"+name+"'>"+name+" <a onclick='deleteVariableFromTunedesk(\""+name+"\",this.value)'>'</label>";
        res+="<input type='range' min='0' max='1' step='any' id='tune_"+name+"' value='"+value+"' oninput='updateVariableValue(\""+name+"\",this.value)'>";
        res+="</div>";
        return res;
    });

    this.getVariableValue = function (){
        stash.get("tune_"+conf.name);
    };

    this.getConf = (function (){
        var v = document.getElementById("tune_"+conf.name).value;
        return {
            name: conf.name,
            type: "slider01",
            value: v
        };
    });

    this.loadConf = (function (nuevaConf){
        var element = document.getElementById("tune_"+conf.name);
        element.value = nuevaConf.value;
        element.dispatchEvent(new Event('input'));
    });
}