
var globalErrorRenderer;

var timeAnterior = 1;
var time;
var quality = 1;
var variables_a_inyectar = [];
var liveAudioFeatures = {};
variables_a_inyectar['tune'] = {};
variables_a_inyectar['auto'] = {};
var codigo_antiguo, codigo_antiguo_preview;
var codigo_actual ="";
var codigo_preview ="";
var tiempoAnterior = 0;

const animatex = () => {
    if(!isRenderer && !previewMode) return;
    requestAnimationFrame(animatex);

    //renderer.setValue("TIME", time);
    renderer.setValue("time", time/1000);
    if(!previewMode) {
        opener.fps = (1000 / (time - tiempoAnterior));
        tiempoAnterior = time;
    }
    renderer.setValue("resolution", [canvas.width,canvas.height]);
    Object.entries(variables_a_inyectar['textures'] || {}).forEach(([key, val]) => {
        renderer.setValue(key,val);
    });
    //Object.entries(variables_a_inyectar['texturesStatic']).forEach(([key, val]) => {
    //    renderer.setValue(key,val);
    //});
    Object.entries(variables_a_inyectar['tune'] || {}).forEach(([key, val]) => {
        renderer.setValue(key, val);
    });
    Object.entries(variables_a_inyectar['auto'] || {}).forEach(([key, val]) => {
        renderer.setValue(key, val);
    });
    //if(variables_a_inyectar['features']) Object.entries(variables_a_inyectar['features']).forEach(([key, val]) => {
    //Object.entries(['rms','energy']).forEach(([key, val]) => {
    Object.entries(opener.liveAudioFeatures || {}).forEach(([key, val]) => {
        if(key=="chroma"){
            for(x=0; x<11; x++){
                renderer.setValue("audio_chroma_" + x, val[x]);
            }
        }
        if(key=="mfcc"){
            for(x=0; x<11; x++){
                renderer.setValue("audio_mfcc_" + x, val[x]);
            }
        }else {
            renderer.setValue("audio_" + key, val);
        }
        //console.log("valuefeature: ", key, val);
    });

    //Object.entries(variables_a_inyectar['midi']).forEach(([key, val]) => {
    //    renderer.setValue(key, val);
    //});
    renderer.draw(canvas);
    //if(isRenderer){
     //   opener.fps = 1000/(parseInt(time)-timeAnterior);
    //    timeAnterior = parseInt(time);
   // }
};

const canvas = document.querySelector('#isf-canvas');
const gl = canvas.getContext("webgl");
const renderer = new ISFRendereer(gl);



function reloadSource(newCode) {
    renderer.loadSource(newCode);
    renderer.initUniforms();
    renderer.generatePersistentBuffers();
}

function handleResize() {
    if(isRenderer) {
        canvas.width = window.innerWidth / quality;
        canvas.height = window.innerHeight / quality;
        canvas.style.width = window.innerWidth + 'px';
        canvas.style.height = window.innerHeight + 'px';
    }
}

function changeQuality(newQuality) {
    quality = newQuality;
    handleResize();
}



window.addEventListener('resize', handleResize, false);

animatex();

