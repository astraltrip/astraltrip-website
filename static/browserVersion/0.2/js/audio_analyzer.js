var archivo_actual = opener.audio_file;
var htmlAudioElement = opener.document.getElementById('audio');
htmlAudioElement.src=archivo_actual;
var Meyda = require('meyda');
const guess = require( 'web-audio-beat-detector').guess;

var audioContext;
var audioAnalyzer;

async function loadOnTimeline() {
    const itemName = nextItemId;
    if(opener.groupsTimeline.get('AUDIO') == null){
        opener.timelineAddGroup('AUDIO', 'audio');
    }
    opener.curveImages[itemName]=wavesurfer.exportImage();
    //opener.htmlAudioElement.src = archivo_actual;
    const songpreview = opener.document.getElementById('curvepreview'+itemName);
    if(songpreview){
        songpreview.src = wavesurfer.exportImage();
    }
    let newItem = {
        id: itemName,
        name: itemName,
        start: 1+'',
        start_ms: 1,
        end: htmlAudioElement.duration*1000,
        end_ms: htmlAudioElement.duration*1000,
        group: 'AUDIO',
        func: "audio",
        audio_file: htmlAudioElement.src,
        editable: false,
        content: opener.createTemplateSongTimeline(itemName),
        onRemove: (item, callback) => {
            console.log("GREGE",item)
            if(item.func=="audio"){
                htmlAudioElement.pause(); htmlAudioElement.src="";
            }
            callback(item);
        },
    };
    opener.itemsTimeline.add(newItem);
    //stash.set('loadtimelineItems',itemsTimeline.get());

}
async function analyzeBPM() {
    fetch(archivo_actual)
    // when we get the asynchronous response, convert to an ArrayBuffer
        .then(response => response.arrayBuffer())
        .then(buffer => {
            // decode the ArrayBuffer as an AudioBuffer
            audioContext.decodeAudioData(buffer, decoded => {
                guess(decoded)
                    .then(({ bpm, offset }) => {
                        console.error(bpm,offset);
                    })
            });
        });
}

function fixAudioFeatures(){
    var ms_actual = 0;
    var result= [];
    opener.audioFeatures.forEach((entry) =>{
        var ms_entry = parseInt(entry.time*100);
        for(let x = ms_actual; x<=ms_entry; x++){
            result[x] = entry;
        }
        ms_actual = ms_entry+1;
    });

    opener.audioFeatures = result;
    console.log("ended", result);
}

function analyzeAudio() {
    if (typeof Meyda === "undefined") {
        console.log("Meyda could not be found! Have you included it?");
    }
    audioContext = new AudioContext();
    const source = audioContext.createMediaElementSource(htmlAudioElement);
    source.connect(audioContext.destination);
    audioAnalyzer = Meyda.createMeydaAnalyzer({
        "audioContext": audioContext,
        "source": source,
        "bufferSize": 512,
        "featureExtractors": ["rms", "energy"], //, "spectralSlope", "spectralCentroid", "spectralRolloff"],
        "callback": features => {
            //variables_a_inyectar['midi']['k1']=(features.energy);
            features.time=htmlAudioElement.currentTime;
            opener.audioFeatures.push(features);
            console.log(features,htmlAudioElement.currentTime, htmlAudioElement.duration)
        }
    });
    opener.audioFeatures = [];
    audioAnalyzer.start();
    htmlAudioElement.play();
    htmlAudioElement.addEventListener("ended", () =>{
        audioAnalyzer.stop();
        htmlAudioElement.currentTime = 0;
        fixAudioFeatures();
    });
}

const wavesurfer = WaveSurfer.create({
    container: '#waveform',
    waveColor: 'darkblue',
    progressColor: 'antiqueblue' });
wavesurfer.load(archivo_actual);
/*
setTimeout(() => {
    curveImages[itemName]=wavesurfer.exportImage();
    const songpreview = document.getElementById('curvepreview'+itemName);
    console.log(curveImages[itemName]);
    if(songpreview){
        songpreview.src = curveImages[itemName];
    }
    wavesurfer.destroy();
},700);
 */
htmlAudioElement.onloadedmetadata = async () => {
    await loadOnTimeline();
    window.close();

};
